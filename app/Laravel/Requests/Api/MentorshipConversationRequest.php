<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class MentorshipConversationRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'content' => "required",
        ];

        return $rules;
    }

    public function messages() {

        return [
            'required' => "Unable to send blank message.",

        ];
    }
}
