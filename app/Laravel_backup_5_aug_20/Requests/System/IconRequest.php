<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class IconRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;
		$type = $this->request->get('group')?:"";
		$media_type = $this->request->get('media_type')?:"";
		
	
		$rules = [
			'name'	=> "required",
			'group' => "required",
			
			'file' =>"required",
		];
		
		if($id){
			$rules['file'] = "nullable|image";
		}


		return $rules;
	}

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
		];
	}
}