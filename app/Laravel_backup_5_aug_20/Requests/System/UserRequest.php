<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class UserRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;
		$type = $this->request->get('user_type')?:"";
		
		$rules = [
			'username'		=> "required|unique:user,username,{$id},id,deleted_at,NULL",
			'email'	=> "required|email|unique:user,email,{$id},id,deleted_at,NULL",
			'file'	=> "nullable|mimes:png,jpeg,jpg",
			// 'birthdate'	=> "required|date",
			//'name'		=> "required",
			
			//'type'	=> "required",
			'street' => "required",
			'city' => "required",
			'state' => "required",
			'post_code' => "required",
			//'alternate_email' => "required",
			'business_group' => "required",
			'contact_number' =>  "required",
			'firstname' => "required",
			'lastname' => "required",
			// 'user_type' => "required",
			'business_name' => "required_if:user_type,area_head",
			'business_number' => "required_if:user_type,area_head",
			'tax_number' => "required_if:user_type,area_head",
			'business_street' => "required_if:user_type,area_head",
			'business_city' => "required_if:user_type,area_head",
			'business_state' => "required_if:user_type,area_head",
			'business_post_code' => "required_if:user_type,area_head",
			'user_head' => "required_if:user_type,area_head",
			'franchisee_head' => "required_if:user_type,franchisee",

		

			

		];

		if(Auth::user()->type == 'super_user'){
			$rules['user_type'] = "required";
		}

		//dd($type);

		if($type == "area_head"){
			$rules['business_avatar'] = "nullable|mimes:png,jpeg,jpg";
			$rules['business_name'] = "required";
		
		}

		

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'required_if'	=> "Field is required.",
			'contact.phone'	=> "Please provide a valid PH mobile number.",
			'birthdate.date'	=> "Birthdate must be a valid date.",
			'website.url'	=> "Invalid URL format. Adding http:// or https:// is also needed.",
		];
	}
}