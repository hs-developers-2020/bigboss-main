<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class ChatConversationFileRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'file' => "required|file|max:25000",
        ];

        return $rules;
    }

    public function messages() {

        return [
            'required' => "Attachment not found.",
            'file'      => "Invalid file attachment",
            'max'       => "Attachement should not exceed to 25mb in size.",
        ];
    }
}
