<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Marker Labels</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARqca1-aq9-JEWAyB177FepK2QyLRBDzs"></script>
    
    <script>
      // In the following example, markers appear when the user clicks on the map.
      // Each marker is labeled with a single alphabetical character.
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var labelIndex = 0;

      var totalLatLong =[]

      function initialize() {


        var bangalore = { lat: 28.5355, lng: 77.3910 };
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: bangalore
        });

        // This event listener calls addMarker() when the map is clicked.
        google.maps.event.addListener(map, 'click', function(event) {
          addMarker(event.latLng, map);
        });

        // Add a marker at the center of the map.
       // addMarker(bangalore, map);
      }

      // Adds a marker to the map.
      function addMarker(location, map) {
        // Add the marker at the clicked location, and add the next-available label
        // from the array of alphabetical characters.
        if(labelIndex != 0){
           totalLatLong.push({lat:location.lat(),lng:location.lat()});
         }else{

         }
       
        var marker = new google.maps.Marker({
          position: location,
          label: labels[labelIndex++ % labels.length],
          map: map
        });
      }

      google.maps.event.addDomListener(window, 'load', initialize);




    </script>
  </head>
  <body>

      <button type="button" class="btn btn-primary mr-3 text-xsmall" id="add_user" >ADD TERRITORY</button>

    <div id="map"></div>

    <script type="text/javascript">
      
      $(document).ready(function(){

         $('#add_user').on('click',function(){
          alert(JSON.stringify(totalLatLong));
        
    });

      });
    </script>



  </body>
</html>
    