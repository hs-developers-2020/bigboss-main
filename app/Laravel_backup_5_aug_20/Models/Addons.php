<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addons extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "add_ons";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','price','file','path', 'directory', 'filename'
    ];

    public $timestamps = true;

}

