@extends('system._layouts.main')
<style type="text/css">
	footer {
		margin-top: 25px !important;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1030;
	}
    body {
    	padding-top: 9% !important;
    }

    .text-color--yellow {
      color:#ffc700;
    }

    .text-color--gray {
      color: #8b9aab;
    }
</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
		<!-- header starts -->
	
			<!-- Begin page content -->
			<div class="container ">
                <form action="" id="search_filter" >
                    <div class="row">
                        <div class="col-lg-2 pt-2">
                            <h6 class="text-xsmall spacing-1 text-color-gray">Current Jobs</h6>
                        </div>
                        <div class="col-lg-3">     
                            {!!Form::select('group',$service_group,old('group'),['class' => "form-control text-color-blue font-semibold pl-3 pr-3 ", 'id' => "select_group"])!!}
                        </div>
                        <div class="col-lg-7 mb-2 d-flex flex-row ">   
                            <input type="text" class="searchTerm" name="keyword" value="{{$keyword}}" placeholder="Search keyword such as name">
                            <a href="#"  class="btn bg-color-blue text-color-white searchButton" id="button_search">
                            <i class="fa fa-search"></i>
                            </a>
                        </div>       
                    </div>
                </form>        
                <table class="table bg-color-white mb-0 footable">
                    <thead>
                        <tr>
                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">#</th>
                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Username</th>
                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Feedback</th>
                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">RATING</th>                                        
                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">Date Submitted</th>

                             <th  class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        @forelse($feedbacks as $feedback)
                            <tr>
                                <td>
                                    <h5 class="text-color-gray p-0">#{{ str_pad($feedback->id, 5, "0", STR_PAD_LEFT) }}</h5>
                                </td>
                                <td class="userlist p-4">
                                    <div class="media">
                                        <div class="figure avatar40 border rounded-circle align-self-start">
                                            <label class="checkbox-user-check">
                                                <input type="checkbox">
                                                <i class="fa fa-check"></i>
                                            </label>
                                            <img src="{{ asset('frontend/images/user.jpg') }}" alt="Generic placeholder image" class="">
                                        </div>
                                        <div class="media-body pt-2">
                                            <h5 class="text-color-gray">{{ $feedback->user ? $feedback->user->username : '' }}
                                                <span class="float-right text-muted"></span>
                                            </h5>
                                            {{-- <p class="mb-0">New York, U</p> --}}
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    {{-- <a class=" text-color-white" style="text-decoration: none;" href="{{ route('system.customer-feedback.feedback-details', $feedback->id) }}"><h5 class="text-color-gray p-0">{{ $feedback->feedback }}</h5></a> --}}
                                    <h5 class="text-color-gray p-0">{{ $feedback->feedback ?: "No Feedback" }}</h5>
                                </td>
                                <td>
                                    <h5 class="text-color-gray p-0">{{ $feedback->rate }} Stars</h5>
                                    <p class="mb-0" style="font-size: 10px;">
                                        @for($i = 0; $i < $feedback->rate; $i++)
                                            <i class="fas fa-star text-color--yellow"></i>
                                        @endfor
                                        @for($i = 0; $i < (5 - $feedback->rate); $i++)
                                            <i class="fas fa-star text-color--gray"></i>
                                        @endfor
                                    </p>
                                </td> 
                                <td>
                                    <h5 class="text-color-gray p-0">{{ date('d-m-Y',strtotime(Helper::date_db($feedback->created_at))) }}</h5>
                                </td>
                                {{-- <td>
                                    {!! Helper::ticket_status_badge($ticket->status) !!}
                                </td> --}}


                                <td>
                                    <?php
                                    if (Auth::user()->type == "super_user") {
                                        ?>
                       <a class="btn-destroy mb-2 text-color-light font-semibold" data-url="{{ route('system.customer-feedback.destroy',[$feedback->id]) }}" href="#"><i class="mdi mdi-delete"></i> <span>Delete Feedback</span></a>
                       <?php
                   }?>
                    </td> 


                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="float-right">
                                    {{ $feedbacks->appends(['keyword' => $keyword, 'group' => $group ])->render() }}
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>                    
			</div>
		</div>
		<!-- main container ends -->

		
		<!-- sidebar right ends -->

	</div>     
@stop

@section('page-scripts')
<script type="text/javascript">
    
    $('#select_group').on('change', function(){
        $('#search_filter').get(0).submit();
    });


    $(".table")
        .delegate('.btn-destroy','click', function(){
            var url = $(this).data('url');
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this record.",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#3085d6",   
                confirmButtonText: "Yes, delete it!"
            }).then(function(result) {
                window.location.href = url;
            });
        });

</script>

@stop