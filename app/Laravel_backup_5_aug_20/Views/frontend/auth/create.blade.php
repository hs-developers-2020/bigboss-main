@extends('frontend._layout.main')

@section('content')


@include('frontend._components.topnav-main')

<div class="container mt-4">
  <div class="d-flex justify-content-start">
    <p class="text-color-gray font-semibold text-xsmall spacing-1 text-uppercase">Create An <span class="text-color-blue">Account</span></p>

  </div>
</div>
<form action="" method="post" id="form">
  {{ csrf_field() }}

  <div class="container pb-5 pl-5 pr-5 pt-4" style="background-color: white; margin-top: 90px">       
    <div class="row">

      <div class="col-lg-12 mt-4 mb-2">
        <p class="text-color-gray text-xsmall font-weight-bold spacing-1 text-uppercase">USER DETAILS</p>
      </div>

    <div class="col-lg-6 {{ $errors->first('firstname') ? 'has_error': NULL  }}">
       <div class="text-color-gray mb-2 ">
        <i class="fas fa-angle-double-right text-color-blue"></i>
        <span class="ml-2 text-xxsmall font-semibold text-uppercase">First Name</span>
      </div>
      <input type="text" placeholder="Enter First Name" class="form-control text-xsmall" name="firstname"  value="{{ old('firstname') }}">
      @if($errors->first('firstname'))
      <span class="text-danger">{{$errors->first('firstname')}}</span>
      @endif
    </div>
    
    <div class="col-lg-6 mt-sm-3 mt-lg-0 mt-xs-3 {{ $errors->first('lastname') ? 'has_error': NULL  }}">
       <div class="text-color-gray mb-2 ">
        <i class="fas fa-angle-double-right text-color-blue"></i>
        <span class="ml-2 text-xxsmall font-semibold text-uppercase">Last Name</span>
      </div>
      <input type="text" placeholder="Enter Last Name" class="form-control text-xsmall" name="lastname"  value="{{ old('lastname') }}">
      @if($errors->first('lastname'))
      <span class="text-danger">{{$errors->first('lastname')}}</span>
      @endif
    </div>

    {{-- <div class="col-lg-4"></div> --}}

    <div class="col-lg-6 mt-3 {{ $errors->first('email') ? 'has_error': NULL  }}">
     <div class="text-color-gray mb-2">
      <i class="fas fa-angle-double-right text-color-blue"></i>
      <span class="ml-2 text-xxsmall font-semibold text-uppercase">Enter your email address</span></div>
      <input type="email" placeholder="Email address" class="form-control text-xsmall" name="email" value="{{ old('email') }}" >
      @if($errors->first('email'))
      <span class="text-danger">{{$errors->first('email')}}</span>
      @endif
    </div>


    <div class="col-lg-6 mt-3 offset-8">
     <div class="text-color-gray mb-2">
      <i class="fas fa-angle-double-right text-color-blue"></i>
      <span class="ml-2 text-xxsmall font-semibold text-uppercase">Contact number</span></div>
      <div class="input-icons"> 
        <i class="icon text-color-blue p-3" style="padding: 5px !important; padding-left: 10px !important;"><p class="text-color-blue font-weight-bold" style="font-style: normal; margin-top: 2px;">+61 |</p></i>
        <input type="number" placeholder="Contact number" class="input-field form-control text-xsmall" style="padding-left: 60px !important; padding-bottom: 8px !important; padding-top: 8px !important;" name="contact_number" value="{{ old('contact_number') }}">
      </div>
      @if($errors->first('contact_number'))
      <span class="text-danger">{{$errors->first('contact_number')}}</span>
      @endif
    </div>
    {{-- <div class="col-lg-4"></div> --}}



    <div class="col-lg-4 mt-3 {{ $errors->first('username') ? 'has_error': NULL  }}">
     <div class="text-color-gray mb-2">
      <i class="fas fa-angle-double-right text-color-blue"></i>
      <span class="ml-2 text-xxsmall font-semibold text-uppercase">Username</span></div>
      <input type="text" placeholder="username" class="form-control text-xsmall" name="username" value="{{ old('username') }}">
      @if($errors->first('username'))
      <span class="text-danger">{{$errors->first('username')}}</span>
      @endif
    </div>

    <div class="col-lg-4 mt-3 {{ $errors->first('password') ? 'has_error': NULL  }}">
     <div class="text-color-gray mb-2">
      <i class="fas fa-angle-double-right text-color-blue"></i>
      <span class="ml-2 text-xxsmall font-semibold text-uppercase">Password</span></div>
      <input type="password" placeholder="Password" class="form-control text-xsmall" name="password" value="{{ old('password') }}">
      @if($errors->first('password'))
      <span class="text-danger">{{$errors->first('password')}}</span>
      @endif
    </div>

    <div class="col-lg-4 mt-3 {{ $errors->first('password_confirmation') ? 'has_error': NULL  }}">
     <div class="text-color-gray mb-2">
      <i class="fas fa-angle-double-right text-color-blue"></i>
      <span class="ml-2 text-xxsmall font-semibold text-uppercase">Re-Enter Password</span></div>
      <input type="password" placeholder="Password" class="form-control text-xsmall" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
    </div>


  </div>
</div>

<div class="" style="background-color: #F4F4F4;">
  <div class="container p-4" style="background-color: white;" >
   <div class="row">
    <div class="col-lg-12">
      <hr> 
    </div>
    <div class="col-6">
      <div class="d-flex flex-row text-color-red pointer">
        <div class="d-flex flex-row justify-content-end text-color-blue pointer">
          <a id="cancel" class="btn text-color-red" style="text-decoration: none; background-color: transparent;" href="{{route('frontend.login')}}">CANCEL</a>
      
        </div>
      </div>
    </div>
    <div id="continue" class="col-6 text-right">
      <div class="d-flex flex-row justify-content-end text-color-blue pointer">
        <button type="submit" name="next"  class="btn bg-color-blue font-semibold text-color-blue" style="text-decoration: none; background-color: transparent;">CREATE AN ACCOUNT
        </button>
      </div>
    </div>

  </div>
</div>
</div>

</form>
@include('frontend._components.footer')


@stop
@section('scripts-content')
  <script>
  
     $(function(){  

      $('#cancel').on('click',function()
      {
         console.log("Dsadsa") 
         $("#form").trigger('reset');
      })
    });
  </script>
@stop


@section('style-script')

<style type="text/css">
  body {
    background-color: #F4F4F4 !important;
  }
</style>
@stop