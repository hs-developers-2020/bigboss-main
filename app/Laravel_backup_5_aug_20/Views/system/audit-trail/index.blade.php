@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
	<div class="container ">
		<div class="row">
			<div class="col-lg-3 pt-2">
                <h6 class="text-uppercase text-xsmall text-color-gray">Users ({{str_replace("_", " ",Auth::user()->type)}})</h6>
            </div>
		</div>
        <table class="table bg-color-white  mb-0 ">
            <thead>      
                <tr>
                    <th class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">#</th>
                    <th class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">IP</th>
                    <th class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm" >Description</th>
                                                                                     
                    <th  class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" width="15%">Process</th>
    
                    <th  class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="">Date</th>
                </tr>
            </thead>
            <tbody>
            @forelse($logs as $index)
            <tr style="text-decoration-color: #000;">
                <td><p class="mb-0">{{$index->id}}</p></td>
                <td><p class="mb-0"><i>{{$index->ip}}</i></p></td>
                <td><p class="mb-0"><i>{{$index->remarks}}</i></p></td>
                <td><p class="mb-0">{{str_replace("_", " ", $index->process)}}</p></td>
<?php 
$datebc= str_replace('/', '-',$index->created_at);
 $newdateb = date("d-m-Y", strtotime($datebc));
?>

                <td><p class="mb-0">{{$newdateb}}</p></td>
            </tr>
            @empty
                <tr>
                    <td>No Available Records</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">
                        <div class="float-right">
                            {{ $logs->render() }}
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>                 
	</div>
</div>



@stop

@section('page-styles')
<style type="text/css">
footer {
    margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
}
body {
    padding-top: 9% !important;
}
.table tbody tr td {
    
   color: #000!important;
}

</style>
@stop