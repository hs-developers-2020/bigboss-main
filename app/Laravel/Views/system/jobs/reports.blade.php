@extends('system._layouts.main')
<style type="text/css">
footer {
	margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
    <div class="container ">
        <form  method="post">
            {{ csrf_field() }}
      <div class="row">

             <div class="form-group col-md-3">
              <label>Start Date*</label>
              <input type="text" class="form-control"   name="sdate" id="sdate" placeholder="Start Date" required >
            </div>
             
            <div class="form-group col-md-3">
              <label>End Date*</label>
              <input type="text" class="form-control"   name="edate" id="edate" placeholder="End Date"  required >
            </div>
              
             
            <div class="form-group col-md-3">
              <label>Status</label>
              
              <select class="form-control" id="franchisee_selected_custom" name="franchisee_selected_custom">
                     <option value="">Select All</option>
                <?php
                foreach($franchisee_lists as $fval){?>
                    <option value="<?php echo $fval->id;?>"><?php echo $fval->firstname." ".$fval->lastname;?></option>
                <?php
                    }
                    ?>
            </select>

            </div>
             
            <div class="form-group col-md-2">
              <label>&nbsp;</label>

              <button type="submit" style="margin-top: 25px;" class="btn btn-info"> <i class="fa fa-download"></i> Download</button>
            </div>

      </div>  
      </form>              
    </div>
</div> 
@stop


@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">


<link rel="stylesheet" href="{{asset('system/js/datepicker/datepicker3.css')}}">
<script src="{{asset('system/js/datepicker/bootstrap-datepicker.js')}}"></script>
    <script>
$(document).ready(function(){
    
    $("#sdate").datepicker({
        format: 'yyyy-mm-dd',
        clickInput:true, 
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        //startDate: '+0d',
        autoclose: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#edate').datepicker('setStartDate', minDate);
    });
    
    
    $("#edate").datepicker({
        format: 'yyyy-mm-dd',
        clickInput:true, 
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        //startDate: '+0d',
        autoclose: true
    }) .on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            $('#sdate').datepicker('setEndDate', maxDate);
        });
});
    
    
    
</script>


@stop