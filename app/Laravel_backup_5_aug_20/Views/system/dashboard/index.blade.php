@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
	<!-- main container starts -->
	<div class="main-container" style="margin-top: 90px;">
		<div class="container-fluid bg-black position-relative pb-4" style="background-color: #3C3C3C !important;">
			<div class="row">
				<div class="container pt-2 pb-4">
					<div class="row page-title-row">
						<div class="col-lg-4 ">
							<h6 class="page-title text-white font-semibold text-uppercase" style="font-size: 17px;">Dashboard | Main</h6>
						</div>
						<!-- <div class="col-lg-8">
							<div class="d-flex flex-row d-flex justify-content-end">
								<p class="page-title text-white text-uppercase font-semibold mr-2" style="font-size: 16px;">Choose your dashboard</p>
								<select class="form-control w-25 text-color-blue font-semibold pl-3 pr-3">
									<option>Main dashboard</option>        
								</select>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>

		<!-- Begin page content -->
		<div class="container">
			<div class="row upside-summary">
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-4 pb-2">
					<a href="{{route('system.jobs.cancelled')}}">
						<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
							<img src="{{ asset('system/img/dashboard/1.jpg') }}" class="img-fluid" >
							<div class="card-body position-absolute w-100">						
								<div class="media">
									<div class="media-body">	
										<div class="row">
											<div class="col-lg-6 pt-2">
												<p class="text-color-white text-uppercase text-xsmall font-semibold">Call out jobs</p> 
												<!-- <p class="text-color-white font-semibold text-xsmall">1 Request</p> -->                                                  
											</div>
											<div class="col-6 text-right pt-2">
												<h2 class="text-color-white font-semibold round">{{$call_out}}</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
					<a href="{{route('system.jobs.show')}}?type=two_hours">
					<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
						<img src="{{ asset('system/img/dashboard/2.jpg') }}" class="img-fluid" >
						<div class="card-body position-absolute w-100">
							<div class="media">
								<div class="media-body">
									<div class="row">
										<div class="col-lg-6 pt-2">
											<p class="text-color-white text-uppercase text-xsmall font-semibold">Work received within 2 hours</p> 
											<!-- <p class="text-color-white font-semibold text-xsmall">10 Request</p> -->                                                  
										</div>
										<div class="col-6 text-right pt-2">
											<h2 class="text-color-white font-semibold round">{{$two_hours}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
					<a href="{{route('system.jobs.show')}}?type=one_day">
					<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
						<img src="{{ asset('system/img/dashboard/3.jpg') }}" class="img-fluid" >
						<div class="card-body position-absolute w-100">
							<div class="media">
								<div class="media-body">
									<div class="row">
										<div class="col-lg-6 pt-2">
											<p class="text-color-white text-uppercase text-xsmall font-semibold">Work received within 1 day</p> 
											<!-- <p class="text-color-white font-semibold text-xsmall">30 Request</p> -->                                                  
										</div>
										<div class="col-6 text-right pt-2">
											<h2 class="text-color-white font-semibold round">{{$last_day}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
					<a href="{{route('system.jobs.show')}}?progress=ongoing">
					<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
						<img src="{{ asset('system/img/dashboard/4.jpg') }}" class="img-fluid" >
						<div class="card-body position-absolute w-100">
							<div class="media">
								<div class="media-body">
									<div class="row">
										<div class="col-lg-6 pt-2">
											<p class="text-color-white text-uppercase text-xsmall font-semibold">Work on progress</p> 
											<!-- <p class="text-color-white font-semibold text-xsmall">12 Request</p> -->                                                  
										</div>
										<div class="col-6 text-right pt-2">
											<h2 class="text-color-white font-semibold round">{{$on_progress}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mt-3">
					<a href="{{route('system.jobs.show')}}?progress=pending">
					<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
						<img src="{{ asset('system/img/dashboard/5.jpg') }}" class="img-fluid" >
						<div class="card-body position-absolute w-100">
							<div class="media">
								<div class="media-body">
									<div class="row">
										<div class="col-lg-6 pt-2 ">
											<p class="text-color-white text-uppercase text-xsmall font-semibold">Pending <br>work</p> 
											<!-- <p class="text-color-white font-semibold text-xsmall">2 Request</p> -->                                             
										</div>
										<div class="col-6 pt-2 text-right w-100">
											<h2 class="text-color-white font-semibold round">{{$pending}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mt-3">
					<a href="{{route('system.jobs.show')}}?progress=completed">
					<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
						<img src="{{ asset('system/img/dashboard/6.jpg') }}" class="img-fluid" >
						<div class="card-body position-absolute w-100">
							<div class="media">
								<div class="media-body">
									<div class="row">
										<div class="col-lg-6 pt-2 ">
											<p class="text-color-white text-uppercase text-xsmall font-semibold">Completed work</p> 
											<!-- <p class="text-color-white font-semibold text-xsmall">7 Request</p> -->                                                  
										</div>
										<div class="col-6 pt-2 text-right w-100">
											<h2 class="text-color-white font-semibold round">{{$completed}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mt-3">
					<a href="{{route('system.users.index')}}?user_type=area_head">
					<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
						<img src="{{ asset('system/img/dashboard/7.jpg') }}" class="img-fluid" >
						<div class="card-body position-absolute w-100">
							<div class="media">
								<div class="media-body">
									<div class="row">
										<div class="col-lg-6 pt-2 ">
											<p class="text-color-white text-uppercase text-xsmall font-semibold">Number of Big Boss Area Head</p> 
											<!-- <p class="text-color-white font-semibold text-xsmall">{{$new_boss_regent}}</p>       -->                                            
										</div>
										<div class="col-6 pt-2 text-right w-100">
											<h2 class="text-color-white font-semibold round">{{$boss_regent}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</a>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mt-3">
					<a href="{{route('system.users.index')}}?user_type=franchisee">
					<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
						<img src="{{ asset('system/img/dashboard/8.jpg') }}" class="img-fluid" >
						<div class="card-body position-absolute w-100">
							<div class="media">
								<div class="media-body">
									<div class="row">
										<div class="col-lg-6 pt-2 ">
											<p class="text-color-white text-uppercase text-xsmall font-semibold">Number of Big Boss Franchisee</p> 
											<!-- <p class="text-color-white text-nowrap">{{$new_area_head}} Boss Intendant</p>   -->                                                
										</div>
										<div class="col-6 pt-2 text-right w-100">
											<h2 class="text-color-white font-semibold round">{{$franchisee}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</a>
				</div>   
			</div>          
			<div class="row mt-4">
				<div class="col-lg-4 mt-5">
					<p class="text-small text-color-gray font-semibold">Weekly Percentage</p>
					<div class="bg-color-white p-4 ">
						<div class="row">
<?php 
$date = str_replace('/', '-', $startDate);
 $newDates = date("d-m-Y", strtotime($date));
?>
    

<?php 
$date1 = str_replace('/', '-', $endDate);
 $newDatee = date("d-m-Y", strtotime($date1));
?>
							<div class="col-lg-8">
								<p class="text-xsmall text-color-gray text-uppercase font-semibold">Total Converted leads <br> from<span class="text-color-blue font-semibold"> {{$newDates}} - {{$newDatee}} </span></p>
							</div>



							<div class="col-lg-4 text-right">
								<h4>{{$converted}}/{{$total_job}}</h4>
							</div>
						</div>
					</div>
					<div class="bg-color-green p-1">
						<div class="container d-flex justify-content-between pt-2">
							<p class="text-color-white text-xsmall text-uppercase font-semibold">{{$converted}} Converted Leads</p>
							<p class="text-color-white text-xsmall text-uppercase">{{$percent}} %</p>
						</div>
					</div>
					<div class="bg-color-white p-4 mt-4">
						<div class="row">
							<div class="col-lg-8">
								<p class="text-xsmall text-color-gray text-uppercase font-semibold">Total Unconverted leads <br> from<span class="text-color-blue font-semibold"> {{$newDates}} - {{$newDatee}} </span></p>
							</div>
							<div class="col-lg-4 text-right">
								<h4>{{$unconverted}}</h4>
							</div>
						</div>
					</div>
					<div class="p-1" style="background-color: #931111;">
						<div class="container d-flex justify-content-between pt-2">
							<p class="text-color-white text-xsmall text-uppercase font-semibold">{{$unconverted}} Unconverted Leads</p>
							<p class="text-color-white text-xsmall ">{{$unconverted_percent}}%</p>
						</div>
					</div>

					<div class="bg-color-white p-4 mt-4">
						<div class="row">
							<div class="col-lg-8">
								<p class="text-xsmall text-color-gray text-uppercase font-semibold">Total response time <br> vs last week<span class="text-color-blue font-semibold"> 7-21 - 7-27 </span></p>
							</div>
							<div class="col-lg-4 text-right">
								<h4>8 mins</h4>
							</div>
						</div>
					</div>
					<div class="bg-color-green ">
						<div class="container d-flex justify-content-between pt-2">
							<p class="text-color-white text-xsmall text-uppercase font-semibold">12 Minutes</p>
							<p class="text-color-white text-xsmall text-uppercase ">99.14%</p>
						</div>
					</div>
				</div>

				<div class="col-lg-8 mt-5">
					<div class="d-flex justify-content-between">
						<p class="text-small text-color-gray font-semibold">Customer Feedback</p>
						<p class="text-xsmall font-semibold text-uppercase"><a class="text-color-red decoration-none"  href="{{route('system.customer-feedback.index')}}">View all</a></p>
					</div>
					<div class="bg-color-white p-4 ">
						<div class="row">
							<div class="col-lg-12">
								<table class="table bg-color-white mb-0">
									<thead>
										<tr>
											<th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">#</th>
											<th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Username</th>
											<th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Feedback</th>
											<th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">RATING</th>                                        
											<th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">Date Submitted</th>
										</tr>
									</thead>
									<tbody>
										@forelse($feedbacks as $feedback)
										<tr>
											<td>
												<h5 class="text-color-gray p-0">#{{ str_pad($feedback->id, 5, "0", STR_PAD_LEFT) }}</h5>
											</td>
											<td class="userlist p-4">
												<div class="media">
													<div class="figure avatar40 border rounded-circle align-self-start">
														<label class="checkbox-user-check">
															<input type="checkbox">
															<i class="fa fa-check"></i>
														</label>
														<img src="{{ asset('frontend/images/user.jpg') }}" alt="Generic placeholder image" class="">
													</div>
													<div class="media-body pt-2">
														<h5 class="text-color-gray">{{ $feedback->user ? $feedback->user->username : '' }}
															<span class="float-right text-muted"></span>
														</h5>
														{{-- <p class="mb-0">New York, U</p> --}}
													</div>
												</div>
											</td>
											<td>
												<h5 class="text-color-gray p-0">{{ Helper::get_excerpt($feedback->feedback) }}...</h5>
											</td>
											<td>
												<h5 class="text-color-gray p-0">{{ $feedback->rate }} Stars</h5>
												<p class="mb-0" style="font-size: 10px;">
													@for($i = 0; $i < $feedback->rate; $i++)
														<i class="fas fa-star text-color--yellow"></i>
													@endfor
													@for($i = 0; $i < (5 - $feedback->rate); $i++)
														<i class="fas fa-star text-color--gray"></i>
													@endfor
												</p>
											</td> 
<?php 
$datecc = str_replace('/', '-',$feedback->created_at);
 $newcreated = date("d-m-Y", strtotime($datecc));
?>
											<td>
												<h5 class="text-color-gray p-0">{{ $newcreated }}</h5>
											</td>
											{{-- <td>
												{!! Helper::ticket_status_badge($ticket->status) !!}
											</td> --}}
										</tr>
										@empty
										<tr>
											<td>No Record Available</td>
										</tr>
										@endforelse
									</tbody>
									<tfoot>
							            <tr>
							                <td colspan="5">
							                    <div class="float-right">
							                        {{ $feedbacks->render() }}
							                    </div>
							                </td>
							            </tr>
							        </tfoot>
								</table>        
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
@stop

@section('page-styles')
<style type="text/css">
footer {
	margin-top: 25px !important;
}


.text-color--yellow {
  color:#ffc700;
}

.text-color--gray {
  color: #8b9aab;
}
</style>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
@endif


});
</script>
@stop