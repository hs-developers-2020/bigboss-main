<?php

namespace App\Laravel\Controllers\System;

use App\Laravel\Services\ResponseManager;
use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Models\CustomerFeedback;
use App\Http\Requests\PageRequest;

use Curl,Str ,Auth,AuditRequest,Session,Event;

class CustomerFeedbackController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"Choose Service",'cleaning' => "Cleaning","information_technology" => "Information Technology","accounting" => "Accounting"];
	}	

 	public function index(Pagerequest $request){

  		$this->data['keyword'] = $request->get('keyword', NULL);
 		$this->data['group'] = $request->get('group',false);
 		
 		if (Auth::user()->type == "franchisee") {
			$this->data['feedbacks'] = CustomerFeedback::group($this->data['group'])->where("franchisee_id",Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(10);
		}elseif (Auth::user()->type == "area_head") {
			$this->data['feedbacks'] = CustomerFeedback::group($this->data['group'])->where("franchisee_id",Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(10);
		}
		else{
			$this->data['feedbacks'] = CustomerFeedback::group($this->data['group'])->orderBy('created_at', 'DESC')->paginate(10);
		}

 		

		return view ('system.customer-feedback.index', $this->data);
	}

	public function show($id = NULL){
		$this->data['conversations'] = [];
		// $response = Curl::to("https://ronniecastro.freshdesk.com/api/v2/tickets/{$id}/conversations")
  //    	->withHeader('Authorization: Bearer ' . base64_encode('IUAoQZEL4IBYm5SKLOUs'))
  //    	->returnResponseObject()
  //    	->get();

  //    	if($response->status == 200) {
  //    		$this->data['conversations'] = json_decode($response->content);
  //    	}

		return view ('system.customer-feedback.show', $this->data);
	}


	public function destroy($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$delete_feedback = CustomerFeedback::find($id);
			if (!$delete_feedback) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.customer-feedback.index');
			}

			if($delete_feedback->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FEEDBACK_DELETED", 'remarks' => "Fedback deleted",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session::flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.customer-feedback.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
