<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; margin: 0;">
<tbody>
<?php
/**************
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; margin: 0;">
<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align: center;" valign="top">
<a href="#" target="_new"><img src="{{asset('frontend/images/main-logo.png')}}" alt="" style="height: 70px; width: 140px;"></a>
</td>
</tr>
************/
?>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; margin: 0;">
<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
<p>Dear <?php echo $other_details_booking['name'];?>,</p>
<p>Thank you for your booking. Your local Big Boss expert will be in touch with you shortly.</p>
<h3>This is your booking number <b>{{$booking_id}}</b> for track <a href="http://booking.bigbossgroup.com.au/">click here</a></h3>



<?php
if(isset($other_details_booking) && count($other_details_booking)){
?>

<div style="width: 100%; box-shadow: 0px 0px 5px 0px;     height: 42em;">
    <div style="width: 40%; float: left; padding: 50px;">
      <table>
        <tr>
          <td style="font-size: 1.2rem; font-weight: 700; color: #3C3C3C !important; text-transform:uppercase; padding-bottom: 30px;"><i class="fas fa-receipt mr-2 text-color-blue"></i>QUOTE</td>
        </tr>
        <tr style="padding-top: 20px;">
          <td style="padding-bottom: 10px; font-family: 'Poppins', sans-serif !important;">CUSTOMER NAME</td>
        </tr>
        <tr style="padding-bottom: 10px;">
          <td style="color: #2047A4; font-family: 'Poppins', sans-serif !important; font-size: 15px; padding-bottom: 13px;"><?php echo $other_details_booking['name'];?></td>
        </tr>
        <tr style="padding-top: 20px;">
          <td style="padding-bottom: 10px;font-family: 'Poppins', sans-serif !important;">Contact Number</td>
        </tr>
        <tr style="padding-top: 20px;">
          <td style="color: #2047A4; font-family: 'Poppins', sans-serif !important; font-size: 15px; padding-bottom: 13px;"><?php echo $other_details_booking['contact'];?></td>
        </tr>
        <tr style="padding-top: 20px;">
          <td style="padding-bottom: 10px; font-family: 'Poppins', sans-serif !important;">EMAIL ADDRESS</td>
        </tr>
        <tr style="padding-top: 20px;">
          <td style="color: #2047A4; font-family: 'Poppins', sans-serif !important; font-size: 15px; padding-bottom: 13px;"><?php echo $other_details_booking['email'];?></td>
        </tr>
        <tr style="padding-top: 20px;">
          <td style="padding-bottom: 10px; font-family: 'Poppins', sans-serif !important;">ADDRESS</td>
        </tr>
         <tr style="padding-top: 20px;">
          <td style="color: #2047A4; font-family: 'Poppins', sans-serif !important; font-size: 15px; padding-bottom: 13px;"><?php echo $other_details_booking['location'];?></td>
        </tr>
         <tr style="padding-top: 20px;">
          <td style="padding-bottom: 10px; font-family: 'Poppins', sans-serif !important;">POST</td>
        </tr>
         <tr>
          <td style="color: #2047A4; font-size: 15px; font-family: 'Poppins', sans-serif !important; padding-bottom: 13px; "><?php echo $other_details_booking['postcode'];?></td>
        </tr>
        <tr>
          <td style="padding-bottom:10px; font-family: 'Poppins', sans-serif !important;">PAYMENT METHOD</td>
        </tr>
        <tr>
          <td style="color: #2047A4; font-size: 15px; font-family: 'Poppins', sans-serif !important; padding-bottom: 13px;"><?php echo $other_details_booking['payment'];?></td>
        </tr>


      </table>
    </div>

    <div class="edfs" style="width: 40%;float: right; background:#1758a8; padding: 50px 46px 58px;">
      <div style="font-size: 15px; color: white; font-family: 'Poppins', sans-serif !important; text-align: right; padding-bottom: 30px;">BOOKING ID : 5EF66A7B9159B</div>
      <div style="font-size: 15px; color: white; font-family: 'Poppins';padding-bottom: 20px;"><i class="fa fa-user" aria-hidden="true" style=" padding-right: 9px;"></i>PAYMENT DETAILS</div>

      <table class="table-responsive" style="width: 100%; font-family: 'Poppins', sans-serif !important; padding: 15px 0px;">
        <thead>
          <tr>
            <th style="text-align: left; color: white;    padding: 15px 0px;">SERVICE TYPE</th>
            <th style="text-align: left; color: white;    padding: 15px 0px;">PROPERTY TYPE</th>
            <th style="text-align: left; color: white;    padding: 15px 0px;">PRICE</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="color: white;padding: 14px 0px;font-size: 13px;"><?php echo $other_details_booking['service_type'];?></td>
            <td style="color: white;padding: 14px 0px;font-size: 13px;"><?php echo $other_details_booking['property_type'];?></td>
            <td style="color: white;padding: 14px 0px;font-size: 13px;">$<?php echo $other_details_booking['service_type_amount'];?></td>
          </tr>
          <tr class="tds">
            <th style="text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-bottom: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;">SERVICE PACKAGE</th>
            <th style="text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-bottom: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;">SERVICE PRICE</th>
            <th style="text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-bottom: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;"></th>
         
          </tr>
          <tr class="tds">
            <td style="color: white;padding: 14px 0px;font-size: 13px;"><?php echo $other_details_booking['services'];?></td>
            <td style="color: white;padding: 14px 0px;font-size: 13px;">$<?php echo $other_details_booking['amount'];?></td>
            <td style="color: white;padding: 14px 0px;font-size: 13px;">$<?php echo $other_details_booking['amount'];?></td>
          </tr>
           <!-- <tr class="tds">
            <th style="text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-bottom: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;">SUB SERVICE PACKAGE</th>
            <th style="text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-bottom: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;">SUB SERVICE PRICE</th>
            <th style="text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-bottom: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;"></th>
          </tr>
          <tr class="tds">
            <td style="color: white;padding: 14px 0px;font-size: 13px;"></td>
            <td style="color: white;padding: 14px 0px;font-size: 13px;">$</td>
            <td style="color: white;padding: 14px 0px;font-size: 13px;">$</td>
          </tr> -->
           <tr class="ladt_ta">
            <td style="font-size: 15px; color: white;padding: 14px 0px; text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-top: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;">BOOKING TOTAL</td>
            <td style="color: white;padding: 14px 0px;font-size: 13px;text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-top: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;"></td>
            <td style="color: white;padding: 14px 0px;font-size: 13px; text-align: left; color: white; padding: 15px 0px; vertical-align: bottom; border-top: 1px solid #e9ecef;padding: 6px 28px 15px 0px;font-size: 15px;">$<?php echo $other_details_booking['amount'] + $other_details_booking['service_type_amount'];?></td>
          </tr>
        </tbody>
      </table>




<?php
}?>




<p style="font-weight: bold;font-size: 25px;"></p>
<p style="font-size: 12px; font-style: italic;">Note: If you would like to track the status of your booking kindly click this link <a href="http://booking.bigbossgroup.com.au/">http://booking.bigbossgroup.com.au/</a> and provide your booking ID reference in the Track My Booking option."</p>

</td>
</tr>



<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; margin: 0;">
<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
*** This is an automatically generated email, please do not reply. ***<br>
</td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; margin: 0;">
<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;  font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
<p>Kind regards,</p><br>

Big Boss Group  <br>
Call 1800 131 599<br>
Email info@bigbossgroup.com.au<br>
Website www.bigbossgroup.com.au<br>




</td>
</tr>
</tbody>
</table>
