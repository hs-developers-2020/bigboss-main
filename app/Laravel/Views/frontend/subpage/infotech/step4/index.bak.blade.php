@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
{{ Session::put('percent',40) }}
<form action="" onsubmit="return validateForm()" method="post">
  {{ csrf_field() }}
   <div class="container" style="padding-top: 153px;padding-bottom:7%;">
     <div class="row">
       <div class="col-12 text-center text-xs-center text-md-right text-lg-right text-right">
          <p class="text-color-white font-semibold text-uppercase spacing-1">Service Quote for: {{ Session::get('infotech.postcode') }}, {{ Session::get('infotech.sub') }}</p>
        </div>
        <div class="col-lg-12 text-center">
         <p class="text-white font-semibold spacing-1 mt-3 ">Select service type</p>
        </div>
      </div>
    
          <div class="row">

            @forelse($sub_service as $index)
               <div class="col-lg-3 col-6 text-center mt-3 ">
                  <div class="styled-radio mt-2 mb-2 ">
                    <input type="radio" id="{{$index->id}}" name="sub_svc_type" value="{{$index->id}}" >
                      <label for="{{$index->id}}" class="text-xxsmall spacing-1 pt-3 pb-3" style="color: white;">
                         <img src="{{asset($index->directory.'/'.$index->filename)}}" style="width: auto;height: 80px"><br>
                        <span class="mobile-text">{{Str::limit($index->name)}}</span>
                      </label>
                    </div>
                </div>
            @empty
              <h1>No Service Available</h1>
            @endforelse
             
             
               
              </div>

             <div class="col-lg-12 mt-2 text-center"> 
                <button type="submit"  id="next" class="next btn card-shadow  font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 " href="#"style="color: white !important;">Continue</bjtton>
            </div>
      </div>
   </div>
   </form>
   @include('frontend._components.footer-main')
 @stop
@section('scripts-content')
<script>
function validateForm() {
    var radios = document.getElementsByName("sub_svc_type");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked) formValid = true;
        i++;        
    }

    if (!formValid) Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Please select Service Type!',
  // footer: '<a href>Why do I have this issue?</a>'
})
    return formValid;
}



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 

@media only screen and (max-width: 1500px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
 .footer-bottom{
margin-top: 100px;
  
}
}

@media only screen and (min-width: 1500px) {

   #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
  }
  .picture-size {
  height:5%; width: 7%;
  }
  .footer-bottom{
  position: absolute;
  bottom:0;
  top: 100%

}
  }
  a:hover{
    color: white !important;
  }

.hidden {
  display: none ;
}
.show {
  display: block ;
}



</style>
@stop