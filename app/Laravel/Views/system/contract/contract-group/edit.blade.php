@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper pb-5">
  <div class="container">
    <h6 class="text-xsmall text-color-gray text-uppercase pb-3">Add new contract: Da cleaners</h6>
  </div>
  <form action=""  method="POST" enctype="multipart/form-data">
    {!!csrf_field()!!}
    <div class="container pb-5 pl-5 pr-5 pt-4 mt-2" style="background-color: white; ">       
      <div class="row">
        <div class="col-lg-12 mt-2 mb-2">
          <p class="text-color-gray text-xsmall font-semibold text-uppercase">File Information</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6" >
          <div class="text-color-gray mb-2">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold ">Current File
              <span class="text-color-red">*</span>
            </span>
          </div>
          @if($business_file->file_type == "image_file")
          <img src="{{"{$business_file->directory}/resized/{$business_file->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:cover;height:200px">
          @endif
          @if($business_file->file_type == "pdf_file" or $business_file->file_type == "text_file")
          <input type="text"  placeholder="Enter email file name" class="form-control text-xsmall"  value="{{old('filename',$business_file->filename)}}" readonly>
          @endif

          <div class=" mb-3 mt-3">
            <input type="file" id="file" name="file"/> 
          </div>
           @if($errors->first('file'))
              <span class="help-block" style="color:red;">{{$errors->first('file')}}</span>
            @endif
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 mt-3 {{$errors->first('is_high_risk') ? 'has-error' : NULL}}">
          <div class="text-color-gray mb-2">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">High Risk Contract ?</span>
            {!!Form::select('is_high_risk',$risk_type,old('is_high_risk',$business_file->is_high_risk),['class' => "form-control text-color-blue font-semibold pl-3 pr-3", 'id' => "is_high_risk"])!!}
          </div>
         @if($errors->first('is_high_risk'))
            <span class="help-block" style="color:red;">{{$errors->first('is_high_risk')}}</span>
          @endif
        </div>
        <div class="col-lg-6 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('reference') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Reference Number</span>
            <input type="text"  placeholder="Enter email file name" class="form-control text-xsmall" name="reference" value="{{old('reference',$business_file->reference)}}">
          </div>     
          @if($errors->first('reference'))
            <span class="help-block" style="color:red;">{{$errors->first('reference')}}</span>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('name') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">File Name</span>
            <input type="text"  placeholder="Enter email file name" class="form-control text-xsmall" name="name" value="{{old('name',$business_file->name)}}">
          </div>     
          @if($errors->first('name'))
            <span class="help-block" style="color:red;">{{$errors->first('name')}}</span>
          @endif
        </div>
        <div class="col-lg-6 mt-3 {{$errors->first('label') ? 'has-error' : NULL}}">
          <div class="text-color-gray mb-2">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">File Label</span>
            <input type="text"  placeholder="Enter file label" class="form-control text-xsmall" name="label"  value="{{old('label',$business_file->label)}}">
          </div>
          @if($errors->first('label'))
            <span class="help-block" style="color:red;">{{$errors->first('label')}}</span>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 mt-3 {{$errors->first('date_issue') ? 'has-error' : NULL}}">
          <div class="text-color-gray mb-2">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Date Issue</span>
            <input type="date"  placeholder="Enter file label" class="form-control text-xsmall" name="date_issue" value="{{old('date_issue',$business_file->date_issue)}}">
          </div>
          @if($errors->first('date_issue'))
            <span class="help-block" style="color:red;">{{$errors->first('date_issue')}}</span>
          @endif
        </div>
        <div class="col-lg-6 mt-3 {{$errors->first('date_expiry') ? 'has-error' : NULL}}">
          <div class="text-color-gray mb-2">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Date Expiry</span>
            <input type="date"  placeholder="Enter file date_expiry" class="form-control text-xsmall" name="date_expiry" value="{{old('date_expiry',$business_file->date_expiry)}}">
          </div>
          @if($errors->first('date_expiry'))
            <span class="help-block" style="color:red;">{{$errors->first('date_expiry')}}</span>
          @endif
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 mt-3">
          <button type="submit" class="btn bg-color-blue text-xxsmall text-color-white p-2 pl-3 pr-3"> EDIT FILE<i class="ml-2 fas fa-chevron-right text-color-white" style="font-size: 10px;"></i><i class="fas fa-chevron-right text-color-white" style="font-size: 10px;"></i> </button>
        </div> 
    </div>
  </form>
</div>
  


@include('system._components.footer')     
@stop

@section('page-styles')
<style type="text/css">
  footer {
   position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
  }
body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@stop