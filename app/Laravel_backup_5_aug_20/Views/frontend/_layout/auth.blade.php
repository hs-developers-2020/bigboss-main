<!DOCTYPE html>
<html lang="en">
  
<head>
    @include('frontend._components.metas')
    @include('frontend._components.styles')
  </head>
  <body>
      
      @yield('content')

    @include('frontend._components.scripts')
  </body>
</html>