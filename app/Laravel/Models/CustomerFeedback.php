<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use Str;

class CustomerFeedback extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "customer_feedback";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'booking_id', 'rate', 'feedback','group'
    ];

    public $timestamps = true;

    public function scopeGroup($query,$group = NULL){
        $key = Str::lower($group);
        if ($key) {
            return $query->where(function($query) use ($key){
                    $query->where('group', $key);
            });
        }
       

    }
    public function user(){
        return $this->belongsTo('App\Laravel\Models\User', 'user_id', 'id');
    }



}
