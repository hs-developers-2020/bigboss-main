<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ProfileRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;
		$type = $this->request->get('user_type')?:"";

		$rules = [
			'username'		=> "required|unique:user,username,{$id},id,deleted_at,NULL",
			'email'	=> "required|unique:user,email,{$id}",
			'password'	=> "required|confirmed",
			'contact_number' => "required",
			'firstname' => "required",
			'lastname' => "required",
			
		];

		if($id != 0){
			$rules['password'] = "confirmed";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'required_if'	=> "Field is required.",
			'contact.phone'	=> "Please provide a valid PH mobile number.",
			'birthdate.date'	=> "Birthdate must be a valid date.",
			'website.url'	=> "Invalid URL format. Adding http:// or https:// is also needed.",
		];
	}
}