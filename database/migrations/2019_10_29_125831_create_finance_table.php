<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance', function($table){
            $table->increments('id');
            $table->string('group')->nullable();
            $table->string('booking_id')->nullable();
            $table->string('franchisee_id')->nullable();
            $table->string('details')->nullable();
            $table->string('property_type')->nullable();
            $table->string('type')->nullable();
            $table->string('category')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance');
    }
}
