@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
  <div class="container">
    <p class="text-color-gray font-semibold mb-3">Add New Expense</p>
  </div>
  <form action="" class="with-confirmation" method="POST">
     {!!csrf_field()!!}
 
    <div class="container pb-5 pl-5 pr-5 pt-4 mt-2" style="background-color: white; ">       
      <div class="row">
        <div class="col-lg-12 mt-4 mb-2">
          <p class="text-color-gray text-xsmall font-semibold">Service Group</p>
        </div>          
        <div class="col-lg-4">
          <div class="text-color-gray mb-2 {{$errors->first('group') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold ">Service Group
              <span class="text-color-red ml-1">*</span>
            </span>
            {!!Form::select('group',$service_group,old('group'),['class' => "form-control text-xsmall input-sm select2", 'id' => "group"])!!}
            @if($errors->first('group'))
              <span class="help-block" style="color:red;">{{$errors->first('group')}}</span>
            @endif
          </div>
        </div>
        <div class="col-lg-8 mt-2 pt-4">
          <p class="text-color-light text-xsmall">Choose from: Cleaning, Accounting, FX and infoTech</p>
        </div>
        <div class="col-lg-12 mt-4">
          <p class="text-color-gray text-xsmall font-semibold">Expense Details</p>
        </div>
        <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('details') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>  
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold ">Item Name
              <span class="text-color-red ml-1">*</span>
            </span>
            <input type="text" placeholder="Enter item name" class="form-control text-xxsmall" name="details" value="{{ old('details') }}">
          </div>
          @if($errors->first('details'))
              <span class="help-block" style="color:red;">{{$errors->first('details')}}</span>
          @endif
        
        </div>
        <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('amount') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold ">Price
              <span class="text-color-red ml-1">*</span>
            </span>
            <input type="text" placeholder="Enter price" class="form-control text-xxsmall" name="amount" value="{{ old('amount') }}">    
          </div>
          @if($errors->first('amount'))
              <span class="help-block" style="color:red;">{{$errors->first('amount')}}</span>
          @endif
        </div>

        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('category') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold ">Category
              <span class="text-color-red ml-1">*</span>
            </span>
            <input type="text" placeholder="Enter category" class="form-control text-xxsmall" name="category" value="{{ old('category') }}">    
            @if($errors->first('category'))
              <span class="help-block" style="color:red;">{{$errors->first('category')}}</span>
            @endif
          </div>
        </div>
        <div class="col-lg-12 mt-5">
          <button type="submit" class="btn bg-color-blue text-xsmall text-color-white p-2 pl-3 pr-3">Add Expense</button>
          <a href="{{route('system.finance.index')}}" class="btn bg-color-red text-xsmall text-color-white p-2 pl-3 pr-3 ml-2">Cancel</a>
        
        </div> 
      </div>
    </div>
  </form>
</div>
@stop
@section('page-styles')
<style type="text/css">
  footer {
position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
  }
body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@stop
