@extends('system._layouts.main')
<style type="text/css">
  footer {
    margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
  }
body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
  <div class="container pb-5 pl-5 pr-5 pt-4 mt-2" style="background-color: white; ">
    <form method="POST" action="" enctype="multipart/form-data">
      {!!csrf_field()!!}
      <div class="row">
        <div class="col-lg-12 mt-4 mb-2">
          <p class="text-color-gray text-xsmall font-weight-bold spacing-1 text-uppercase">Edit Customer Information</p>
          <p class="text-uppercase text-xsmall font-semibold">Position : {{Str_replace("_"," ",$edit_user->type)}}</p>
        </div>
        <input type="hidden" name="user_type" value="{{$edit_user->type}}">
        <div class="col-lg-12" align="center">
          <div class="text-color-gray mb-2 {{$errors->first('file') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">User Profile 
              <span class="text-color-red">*</span>
            </span>
          </div>
          @if($edit_user->directory)
          <img src="{{"{$edit_user->directory}/resized/{$edit_user->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:cover;height:200px">
          @else
          <img src="{{ asset('frontend/images/default.png') }}" alt="Generic placeholder image" class="">
          @endif
          <div class=" mb-3 mt-3">
            <input type="file" id="file" name="file"/> 
            @if($errors->first('file'))
            <span class="help-block" style="color: red;">{{$errors->first('file')}}</span>
          @endif 
          </div>
        </div>

        <div class="col-lg-6">
          <div class="text-color-gray mb-2 {{$errors->first('firstname') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">First Name</span>
            <input type="text" placeholder="Enter First name" class="form-control text-xsmall"  name="firstname" value="{{old('firstname',$edit_user->firstname)}}">
          </div>
          @if($errors->first('firstname'))
            <span class="help-block" style="color: red;">{{$errors->first('firstname')}}</span>
          @endif  
        </div>

        <div class="col-lg-6 mt-3 mt-lg-0 mt-md-0">
          <div class="text-color-gray mb-2 {{$errors->first('lastname') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Last Name</span>
            <input type="text"  placeholder="Enter Last name" class="form-control text-xsmall" name="lastname" value="{{old('lastname',$edit_user->lastname)}}">
          </div>
          @if($errors->first('lastname'))
            <span class="help-block" style="color: red;">{{$errors->first('lastname')}}</span>
          @endif  
        </div>

        <div class="col-lg-8 mt-3">

          <div class="row" style="margin-top: -15px;">
            <div class="col-lg-5 mt-3">

                <div class="form-group">
    <label for="country"><i class="fas fa-angle-double-right font-small text-color-blue"></i> Code </label>
    <select name="country_code" id="country_code" class="form-control">
    <option data-countryCode="AU" value="61" <?php if($edit_user->country_code == 61) echo 'selected';?> >Australia (+61)</option>
    <option data-countryCode="DZ" value="213"  <?php if($edit_user->country_code == 213) echo 'selected';?> >Algeria (+213)</option>
    <option data-countryCode="AD" value="376"  <?php if($edit_user->country_code == 376) echo 'selected';?> >Andorra (+376)</option>
    <option data-countryCode="AO" value="244"  <?php if($edit_user->country_code == 244) echo 'selected';?> >Angola (+244)</option>
    <option data-countryCode="AI" value="1264"  <?php if($edit_user->country_code == 1264) echo 'selected';?> >Anguilla (+1264)</option>
    <option data-countryCode="AG" value="1268"  <?php if($edit_user->country_code == 1268) echo 'selected';?> >Antigua &amp; Barbuda (+1268)</option>
    <option data-countryCode="AR" value="54"  <?php if($edit_user->country_code == 54) echo 'selected';?> >Argentina (+54)</option>
    <option data-countryCode="AM" value="374"  <?php if($edit_user->country_code == 374) echo 'selected';?> >Armenia (+374)</option>
    <option data-countryCode="AW" value="297"  <?php if($edit_user->country_code == 297) echo 'selected';?> >Aruba (+297)</option>    
    <option data-countryCode="AT" value="43"  <?php if($edit_user->country_code == 43) echo 'selected';?> >Austria (+43)</option>
    <option data-countryCode="AZ" value="994"  <?php if($edit_user->country_code == 994) echo 'selected';?> >Azerbaijan (+994)</option>
    <option data-countryCode="BS" value="1242" <?php if($edit_user->country_code == 1242) echo 'selected';?> >Bahamas (+1242)</option>
    <option data-countryCode="BH" value="973" <?php if($edit_user->country_code == 973) echo 'selected';?> >Bahrain (+973)</option>
    <option data-countryCode="BD" value="880" <?php if($edit_user->country_code == 880) echo 'selected';?> >Bangladesh (+880)</option>
    <option data-countryCode="BB" value="1246" <?php if($edit_user->country_code == 1246) echo 'selected';?> >Barbados (+1246)</option>
    <option data-countryCode="BY" value="375" <?php if($edit_user->country_code == 375) echo 'selected';?> >Belarus (+375)</option>
    <option data-countryCode="BE" value="32"  <?php if($edit_user->country_code == 32) echo 'selected';?> >Belgium (+32)</option>
    <option data-countryCode="BZ" value="501" <?php if($edit_user->country_code == 501) echo 'selected';?> >Belize (+501)</option>
    <option data-countryCode="BJ" value="229"  <?php if($edit_user->country_code == 229) echo 'selected';?> >Benin (+229)</option>
    <option data-countryCode="BM" value="1441" <?php if($edit_user->country_code == 1441) echo 'selected';?> >Bermuda (+1441)</option>
    <option data-countryCode="BT" value="975"  <?php if($edit_user->country_code == 975) echo 'selected';?> >Bhutan (+975)</option>
    <option data-countryCode="BO" value="591"  <?php if($edit_user->country_code == 591) echo 'selected';?> >Bolivia (+591)</option>
    <option data-countryCode="BA" value="387"  <?php if($edit_user->country_code == 387) echo 'selected';?> >Bosnia Herzegovina (+387)</option>
    <option data-countryCode="BW" value="267"  <?php if($edit_user->country_code == 267) echo 'selected';?> >Botswana (+267)</option>
    <option data-countryCode="BR" value="55"  <?php if($edit_user->country_code == 55) echo 'selected';?> >Brazil (+55)</option>
    <option data-countryCode="BN" value="673"  <?php if($edit_user->country_code == 673) echo 'selected';?> >Brunei (+673)</option>
    <option data-countryCode="BG" value="359"  <?php if($edit_user->country_code == 359) echo 'selected';?> >Bulgaria (+359)</option>
    <option data-countryCode="BF" value="226"  <?php if($edit_user->country_code == 226) echo 'selected';?> >Burkina Faso (+226)</option>
    <option data-countryCode="BI" value="257"  <?php if($edit_user->country_code == 257) echo 'selected';?> >Burundi (+257)</option>
    <option data-countryCode="KH" value="855"  <?php if($edit_user->country_code == 855) echo 'selected';?> >Cambodia (+855)</option>
    <option data-countryCode="CM" value="237"  <?php if($edit_user->country_code == 237) echo 'selected';?> >Cameroon (+237)</option>
    <option data-countryCode="CA" value="1"  <?php if($edit_user->country_code == 1) echo 'selected';?> >Canada (+1)</option>
    <option data-countryCode="CV" value="238"  <?php if($edit_user->country_code == 238) echo 'selected';?> >Cape Verde Islands (+238)</option>
    <option data-countryCode="KY" value="1345"  <?php if($edit_user->country_code == 1345) echo 'selected';?> >Cayman Islands (+1345)</option>
    <option data-countryCode="CF" value="236" <?php if($edit_user->country_code == 236) echo 'selected';?> >Central African Republic (+236)</option>
    <option data-countryCode="CL" value="56"  <?php if($edit_user->country_code == 56) echo 'selected';?> >Chile (+56)</option>
    <option data-countryCode="CN" value="86"  <?php if($edit_user->country_code == 86) echo 'selected';?> >China (+86)</option>
    <option data-countryCode="CO" value="57"  <?php if($edit_user->country_code == 57) echo 'selected';?> >Colombia (+57)</option>
    <option data-countryCode="KM" value="269"  <?php if($edit_user->country_code == 269) echo 'selected';?> >Comoros (+269)</option>
    <option data-countryCode="CG" value="242"  <?php if($edit_user->country_code == 242) echo 'selected';?> >Congo (+242)</option>
    <option data-countryCode="CK" value="682"  <?php if($edit_user->country_code == 682) echo 'selected';?> >Cook Islands (+682)</option>
    <option data-countryCode="CR" value="506"  <?php if($edit_user->country_code == 506) echo 'selected';?> >Costa Rica (+506)</option>
    <option data-countryCode="HR" value="385"  <?php if($edit_user->country_code == 385) echo 'selected';?> >Croatia (+385)</option>
    <option data-countryCode="CU" value="53"  <?php if($edit_user->country_code == 53) echo 'selected';?> >Cuba (+53)</option>
    <option data-countryCode="CY" value="90392"  <?php if($edit_user->country_code == 90392) echo 'selected';?> >Cyprus North (+90392)</option>
    <option data-countryCode="CY" value="357"  <?php if($edit_user->country_code == 357) echo 'selected';?> >Cyprus South (+357)</option>
    <option data-countryCode="CZ" value="42"  <?php if($edit_user->country_code == 42) echo 'selected';?> >Czech Republic (+42)</option>
    <option data-countryCode="DK" value="45"  <?php if($edit_user->country_code == 45) echo 'selected';?> >Denmark (+45)</option>
    <option data-countryCode="DJ" value="253"  <?php if($edit_user->country_code == 253) echo 'selected';?> >Djibouti (+253)</option>
    <option data-countryCode="DM" value="1809"  <?php if($edit_user->country_code == 1809) echo 'selected';?> >Dominica (+1809)</option>
    <option data-countryCode="DO" value="1809"  <?php if($edit_user->country_code == 1809) echo 'selected';?> >Dominican Republic (+1809)</option>
    <option data-countryCode="EC" value="593"  <?php if($edit_user->country_code == 593) echo 'selected';?> >Ecuador (+593)</option>
    <option data-countryCode="EG" value="20"  <?php if($edit_user->country_code == 20) echo 'selected';?> >Egypt (+20)</option>
    <option data-countryCode="SV" value="503"  <?php if($edit_user->country_code == 503) echo 'selected';?> >El Salvador (+503)</option>
    <option data-countryCode="GQ" value="240"  <?php if($edit_user->country_code == 240) echo 'selected';?> >Equatorial Guinea (+240)</option>
    <option data-countryCode="ER" value="291"  <?php if($edit_user->country_code == 291) echo 'selected';?> >Eritrea (+291)</option>
    <option data-countryCode="EE" value="372"  <?php if($edit_user->country_code == 372) echo 'selected';?> >Estonia (+372)</option>
    <option data-countryCode="ET" value="251"  <?php if($edit_user->country_code == 251) echo 'selected';?> >Ethiopia (+251)</option>
    <option data-countryCode="FK" value="500"  <?php if($edit_user->country_code == 500) echo 'selected';?> >Falkland Islands (+500)</option>
    <option data-countryCode="FO" value="298"  <?php if($edit_user->country_code == 298) echo 'selected';?> >Faroe Islands (+298)</option>
    <option data-countryCode="FJ" value="679"  <?php if($edit_user->country_code == 679) echo 'selected';?> >Fiji (+679)</option>
    <option data-countryCode="FI" value="358"  <?php if($edit_user->country_code == 358) echo 'selected';?> >Finland (+358)</option>
    <option data-countryCode="FR" value="33"  <?php if($edit_user->country_code == 33) echo 'selected';?> >France (+33)</option>
    <option data-countryCode="GF" value="594"  <?php if($edit_user->country_code == 594) echo 'selected';?> >French Guiana (+594)</option>
    <option data-countryCode="PF" value="689"  <?php if($edit_user->country_code == 689) echo 'selected';?> >French Polynesia (+689)</option>
    <option data-countryCode="GA" value="241" <?php if($edit_user->country_code == 241) echo 'selected';?> >Gabon (+241)</option>
    <option data-countryCode="GM" value="220" <?php if($edit_user->country_code == 220) echo 'selected';?> >Gambia (+220)</option>
    <option data-countryCode="GE" value="7880" <?php if($edit_user->country_code == 7880) echo 'selected';?> >Georgia (+7880)</option>
    <option data-countryCode="DE" value="49" <?php if($edit_user->country_code == 49) echo 'selected';?> >Germany (+49)</option>
    <option data-countryCode="GH" value="233" <?php if($edit_user->country_code == 233) echo 'selected';?> >Ghana (+233)</option>
    <option data-countryCode="GI" value="350" <?php if($edit_user->country_code == 350) echo 'selected';?> >Gibraltar (+350)</option>
    <option data-countryCode="GR" value="30" <?php if($edit_user->country_code == 30) echo 'selected';?> >Greece (+30)</option>
    <option data-countryCode="GL" value="299" <?php if($edit_user->country_code == 299) echo 'selected';?> >Greenland (+299)</option>
    <option data-countryCode="GD" value="1473" <?php if($edit_user->country_code == 1473) echo 'selected';?> >Grenada (+1473)</option>
    <option data-countryCode="GP" value="590" <?php if($edit_user->country_code == 590) echo 'selected';?> >Guadeloupe (+590)</option>
    <option data-countryCode="GU" value="671" <?php if($edit_user->country_code == 671) echo 'selected';?> >Guam (+671)</option>
    <option data-countryCode="GT" value="502" <?php if($edit_user->country_code == 502) echo 'selected';?> >Guatemala (+502)</option>
    <option data-countryCode="GN" value="224" <?php if($edit_user->country_code == 224) echo 'selected';?> >Guinea (+224)</option>
    <option data-countryCode="GW" value="245" <?php if($edit_user->country_code == 245) echo 'selected';?> >Guinea - Bissau (+245)</option>
    <option data-countryCode="GY" value="592" <?php if($edit_user->country_code == 592) echo 'selected';?> >Guyana (+592)</option>
    <option data-countryCode="HT" value="509" <?php if($edit_user->country_code == 509) echo 'selected';?> >Haiti (+509)</option>
    <option data-countryCode="HN" value="504" <?php if($edit_user->country_code == 504) echo 'selected';?> >Honduras (+504)</option>
    <option data-countryCode="HK" value="852" <?php if($edit_user->country_code == 852) echo 'selected';?> >Hong Kong (+852)</option>
    <option data-countryCode="HU" value="36" <?php if($edit_user->country_code == 36) echo 'selected';?> >Hungary (+36)</option>
    <option data-countryCode="IS" value="354" <?php if($edit_user->country_code == 354) echo 'selected';?> >Iceland (+354)</option>
    <option data-countryCode="IN" value="91"  <?php if($edit_user->country_code == 91) echo 'selected';?> >India (+91)</option>
    <option data-countryCode="ID" value="62"  <?php if($edit_user->country_code == 62) echo 'selected';?> >Indonesia (+62)</option>
    <option data-countryCode="IR" value="98"  <?php if($edit_user->country_code == 98) echo 'selected';?> >Iran (+98)</option>
    <option data-countryCode="IQ" value="964"  <?php if($edit_user->country_code == 964) echo 'selected';?> >Iraq (+964)</option>
    <option data-countryCode="IE" value="353"  <?php if($edit_user->country_code == 353) echo 'selected';?> >Ireland (+353)</option>
    <option data-countryCode="IL" value="972"  <?php if($edit_user->country_code == 972) echo 'selected';?> >Israel (+972)</option>
    <option data-countryCode="IT" value="39"  <?php if($edit_user->country_code == 39) echo 'selected';?> >Italy (+39)</option>
    <option data-countryCode="JM" value="1876"  <?php if($edit_user->country_code == 1876) echo 'selected';?> >Jamaica (+1876)</option>
    <option data-countryCode="JP" value="81"  <?php if($edit_user->country_code == 81) echo 'selected';?> >Japan (+81)</option>
    
    <option data-countryCode="KW" value="965"  <?php if($edit_user->country_code == 965) echo 'selected';?> >Kyrgyzstan (+996)</option>
    <option data-countryCode="LA" value="856" <?php if($edit_user->country_code == 856) echo 'selected';?>  >Laos (+856)</option>
    <option data-countryCode="LV" value="371" <?php if($edit_user->country_code == 371) echo 'selected';?> >Latvia (+371)</option>
    <option data-countryCode="LB" value="961" <?php if($edit_user->country_code == 961) echo 'selected';?> >Lebanon (+961)</option>
    <option data-countryCode="LS" value="266" <?php if($edit_user->country_code == 266) echo 'selected';?> >Lesotho (+266)</option>
    <option data-countryCode="LR" value="231"  <?php if($edit_user->country_code == 231) echo 'selected';?> >Liberia (+231)</option>
    <option data-countryCode="LY" value="218"  <?php if($edit_user->country_code == 218) echo 'selected';?> >Libya (+218)</option>
    <option data-countryCode="LI" value="417"  <?php if($edit_user->country_code == 417) echo 'selected';?> >Liechtenstein (+417)</option>
    <option data-countryCode="LT" value="370"  <?php if($edit_user->country_code == 370) echo 'selected';?> >Lithuania (+370)</option>
    <option data-countryCode="LU" value="352"  <?php if($edit_user->country_code == 352) echo 'selected';?> >Luxembourg (+352)</option>
    <option data-countryCode="MO" value="853"  <?php if($edit_user->country_code == 853) echo 'selected';?> >Macao (+853)</option>
    <option data-countryCode="MK" value="389"  <?php if($edit_user->country_code == 389) echo 'selected';?> >Macedonia (+389)</option>
    <option data-countryCode="MG" value="261"  <?php if($edit_user->country_code == 261) echo 'selected';?> >Madagascar (+261)</option>
    <option data-countryCode="MW" value="265"  <?php if($edit_user->country_code == 265) echo 'selected';?> >Malawi (+265)</option>
    <option data-countryCode="MY" value="60"  <?php if($edit_user->country_code == 60) echo 'selected';?> >Malaysia (+60)</option>
    <option data-countryCode="MV" value="960"  <?php if($edit_user->country_code == 960) echo 'selected';?> >Maldives (+960)</option>
    <option data-countryCode="ML" value="223"  <?php if($edit_user->country_code == 223) echo 'selected';?> >Mali (+223)</option>
    <option data-countryCode="MT" value="356"  <?php if($edit_user->country_code == 356) echo 'selected';?> >Malta (+356)</option>
    <option data-countryCode="MH" value="692"  <?php if($edit_user->country_code == 692) echo 'selected';?> >Marshall Islands (+692)</option>
    <option data-countryCode="MQ" value="596"  <?php if($edit_user->country_code == 596) echo 'selected';?> >Martinique (+596)</option>
    <option data-countryCode="MR" value="222"  <?php if($edit_user->country_code == 222) echo 'selected';?> >Mauritania (+222)</option>
    <option data-countryCode="YT" value="269"  <?php if($edit_user->country_code == 269) echo 'selected';?> >Mayotte (+269)</option>
    <option data-countryCode="MX" value="52"  <?php if($edit_user->country_code == 52) echo 'selected';?> >Mexico (+52)</option>
    <option data-countryCode="FM" value="691"  <?php if($edit_user->country_code == 691) echo 'selected';?> >Micronesia (+691)</option>
    <option data-countryCode="MD" value="373"  <?php if($edit_user->country_code == 373) echo 'selected';?> >Moldova (+373)</option>
    <option data-countryCode="MC" value="377"  <?php if($edit_user->country_code == 377) echo 'selected';?> >Monaco (+377)</option>
    <option data-countryCode="MN" value="976"  <?php if($edit_user->country_code == 976) echo 'selected';?> >Mongolia (+976)</option>
    <option data-countryCode="MS" value="1664"  <?php if($edit_user->country_code == 1664) echo 'selected';?> >Montserrat (+1664)</option>
    <option data-countryCode="MA" value="212"  <?php if($edit_user->country_code == 212) echo 'selected';?> >Morocco (+212)</option>
    <option data-countryCode="MZ" value="258"  <?php if($edit_user->country_code == 258) echo 'selected';?> >Mozambique (+258)</option>
    <option data-countryCode="MN" value="95"  <?php if($edit_user->country_code == 95) echo 'selected';?> >Myanmar (+95)</option>
    <option data-countryCode="NA" value="264"  <?php if($edit_user->country_code == 264) echo 'selected';?> >Namibia (+264)</option>
    <option data-countryCode="NR" value="674"  <?php if($edit_user->country_code == 674) echo 'selected';?> >Nauru (+674)</option>
    <option data-countryCode="NP" value="977"  <?php if($edit_user->country_code == 977) echo 'selected';?> >Nepal (+977)</option>
    <option data-countryCode="NL" value="31"  <?php if($edit_user->country_code == 31) echo 'selected';?> >Netherlands (+31)</option>
    <option data-countryCode="NC" value="687"  <?php if($edit_user->country_code == 687) echo 'selected';?> >New Caledonia (+687)</option>
    <option data-countryCode="NZ" value="64"  <?php if($edit_user->country_code == 64) echo 'selected';?> >New Zealand (+64)</option>
    <option data-countryCode="NI" value="505"  <?php if($edit_user->country_code == 505) echo 'selected';?> >Nicaragua (+505)</option>
    <option data-countryCode="NE" value="227"  <?php if($edit_user->country_code == 227) echo 'selected';?> >Niger (+227)</option>
    <option data-countryCode="NG" value="234"  <?php if($edit_user->country_code == 234) echo 'selected';?> >Nigeria (+234)</option>
    <option data-countryCode="NU" value="683"  <?php if($edit_user->country_code == 683) echo 'selected';?> >Niue (+683)</option>
    <option data-countryCode="NF" value="672"  <?php if($edit_user->country_code == 672) echo 'selected';?> >Norfolk Islands (+672)</option>
    <option data-countryCode="NP" value="670"  <?php if($edit_user->country_code == 670) echo 'selected';?> >Northern Marianas (+670)</option>
    <option data-countryCode="NO" value="47"  <?php if($edit_user->country_code == 47) echo 'selected';?> >Norway (+47)</option>
    <option data-countryCode="OM" value="968"  <?php if($edit_user->country_code == 968) echo 'selected';?> >Oman (+968)</option>
    <option data-countryCode="PW" value="680"  <?php if($edit_user->country_code == 680) echo 'selected';?> >Palau (+680)</option>
    <option data-countryCode="PA" value="507"  <?php if($edit_user->country_code == 507) echo 'selected';?> >Panama (+507)</option>
    <option data-countryCode="PG" value="675"  <?php if($edit_user->country_code == 675) echo 'selected';?> >Papua New Guinea (+675)</option>
    <option data-countryCode="PY" value="595"  <?php if($edit_user->country_code == 595) echo 'selected';?> >Paraguay (+595)</option>
    <option data-countryCode="PE" value="51"  <?php if($edit_user->country_code == 51) echo 'selected';?> >Peru (+51)</option>
    <option data-countryCode="PH" value="63"  <?php if($edit_user->country_code == 63) echo 'selected';?> >Philippines (+63)</option>
    <option data-countryCode="PL" value="48"  <?php if($edit_user->country_code == 48) echo 'selected';?> >Poland (+48)</option>
    <option data-countryCode="PT" value="351"  <?php if($edit_user->country_code == 351) echo 'selected';?> >Portugal (+351)</option>
    <option data-countryCode="PR" value="1787"  <?php if($edit_user->country_code == 1787) echo 'selected';?> >Puerto Rico (+1787)</option>
    <option data-countryCode="QA" value="974"  <?php if($edit_user->country_code == 974) echo 'selected';?> >Qatar (+974)</option>
    <option data-countryCode="RE" value="262"  <?php if($edit_user->country_code == 262) echo 'selected';?> >Reunion (+262)</option>
    <option data-countryCode="RO" value="40"  <?php if($edit_user->country_code == 40) echo 'selected';?> >Romania (+40)</option>
    <option data-countryCode="RW" value="250"  <?php if($edit_user->country_code == 250) echo 'selected';?> >Rwanda (+250)</option>
    <option data-countryCode="SM" value="378"  <?php if($edit_user->country_code == 378) echo 'selected';?> >San Marino (+378)</option>
    <option data-countryCode="ST" value="239"  <?php if($edit_user->country_code == 239) echo 'selected';?> >Sao Tome &amp; Principe (+239)</option>
    <option data-countryCode="SA" value="966"  <?php if($edit_user->country_code == 966) echo 'selected';?> >Saudi Arabia (+966)</option>
    <option data-countryCode="SN" value="221"  <?php if($edit_user->country_code == 221) echo 'selected';?> >Senegal (+221)</option>
    <option data-countryCode="CS" value="381"  <?php if($edit_user->country_code == 381) echo 'selected';?> >Serbia (+381)</option>
    <option data-countryCode="SC" value="248"  <?php if($edit_user->country_code == 248) echo 'selected';?> >Seychelles (+248)</option>
    <option data-countryCode="SL" value="232"  <?php if($edit_user->country_code == 232) echo 'selected';?> >Sierra Leone (+232)</option>
    <option data-countryCode="SG" value="65"  <?php if($edit_user->country_code == 65) echo 'selected';?> >Singapore (+65)</option>
    <option data-countryCode="SK" value="421"  <?php if($edit_user->country_code == 421) echo 'selected';?> >Slovak Republic (+421)</option>
    <option data-countryCode="SI" value="386"  <?php if($edit_user->country_code == 386) echo 'selected';?> >Slovenia (+386)</option>
    <option data-countryCode="SB" value="677"  <?php if($edit_user->country_code == 677) echo 'selected';?> >Solomon Islands (+677)</option>
    <option data-countryCode="SO" value="252"  <?php if($edit_user->country_code == 252) echo 'selected';?> >Somalia (+252)</option>
    <option data-countryCode="ZA" value="27"  <?php if($edit_user->country_code == 27) echo 'selected';?> >South Africa (+27)</option>
    <option data-countryCode="ES" value="34"  <?php if($edit_user->country_code == 34) echo 'selected';?> >Spain (+34)</option>
    <option data-countryCode="LK" value="94"  <?php if($edit_user->country_code == 94) echo 'selected';?> >Sri Lanka (+94)</option>
    <option data-countryCode="SH" value="290"  <?php if($edit_user->country_code == 290) echo 'selected';?> >St. Helena (+290)</option>
    <option data-countryCode="KN" value="1869"  <?php if($edit_user->country_code == 1869) echo 'selected';?> >St. Kitts (+1869)</option>
    <option data-countryCode="SC" value="1758"  <?php if($edit_user->country_code == 1758) echo 'selected';?> >St. Lucia (+1758)</option>
    <option data-countryCode="SD" value="249"  <?php if($edit_user->country_code == 249) echo 'selected';?> >Sudan (+249)</option>
    <option data-countryCode="SR" value="597"  <?php if($edit_user->country_code == 597) echo 'selected';?> >Suriname (+597)</option>
    <option data-countryCode="SZ" value="268"  <?php if($edit_user->country_code == 268) echo 'selected';?> >Swaziland (+268)</option>
    <option data-countryCode="SE" value="46"  <?php if($edit_user->country_code == 46) echo 'selected';?> >Sweden (+46)</option>
    <option data-countryCode="CH" value="41"  <?php if($edit_user->country_code == 41) echo 'selected';?> >Switzerland (+41)</option>
    <option data-countryCode="SI" value="963"  <?php if($edit_user->country_code == 963) echo 'selected';?> >Syria (+963)</option>
    <option data-countryCode="TW" value="886"  <?php if($edit_user->country_code == 886) echo 'selected';?> >Taiwan (+886)</option>
    <option data-countryCode="TH" value="66"  <?php if($edit_user->country_code == 66) echo 'selected';?> >Thailand (+66)</option>
    <option data-countryCode="TG" value="228"  <?php if($edit_user->country_code == 228) echo 'selected';?> >Togo (+228)</option>
    <option data-countryCode="TO" value="676"  <?php if($edit_user->country_code == 676) echo 'selected';?> >Tonga (+676)</option>
    <option data-countryCode="TT" value="1868"  <?php if($edit_user->country_code == 1868) echo 'selected';?> >Trinidad &amp; Tobago (+1868)</option>
    <option data-countryCode="TN" value="216"  <?php if($edit_user->country_code == 216) echo 'selected';?> >Tunisia (+216)</option>
    <option data-countryCode="TR" value="90"  <?php if($edit_user->country_code == 90) echo 'selected';?> >Turkey (+90)</option>
    <option data-countryCode="TM" value="7"  <?php if($edit_user->country_code == 7) echo 'selected';?> >Turkmenistan (+7)</option>
    <option data-countryCode="TM" value="993"  <?php if($edit_user->country_code == 993) echo 'selected';?> >Turkmenistan (+993)</option>
    <option data-countryCode="TC" value="1649"  <?php if($edit_user->country_code == 1649) echo 'selected';?> >Turks &amp; Caicos Islands (+1649)</option>
    <option data-countryCode="TV" value="688"  <?php if($edit_user->country_code == 688) echo 'selected';?> >Tuvalu (+688)</option>
    <option data-countryCode="UG" value="256"  <?php if($edit_user->country_code == 256) echo 'selected';?> >Uganda (+256)</option>
    <option data-countryCode="GB" value="44"  <?php if($edit_user->country_code == 44) echo 'selected';?> >UK (+44)</option>
    <option data-countryCode="UA" value="380"  <?php if($edit_user->country_code == 380) echo 'selected';?> >Ukraine (+380)</option>
    <option data-countryCode="AE" value="971"  <?php if($edit_user->country_code == 971) echo 'selected';?> >United Arab Emirates (+971)</option>
    <option data-countryCode="UY" value="598"  <?php if($edit_user->country_code == 598) echo 'selected';?> >Uruguay (+598)</option>
    <option data-countryCode="US" value="1"  <?php if($edit_user->country_code == 1) echo 'selected';?> >USA (+1)</option>
    <option data-countryCode="UZ" value="7"  <?php if($edit_user->country_code == 7) echo 'selected';?> >Uzbekistan (+7)</option>
    <option data-countryCode="VU" value="678">  <?php if($edit_user->country_code == 678) echo 'selected';?> Vanuatu (+678)</option>
    <option data-countryCode="VA" value="379"  <?php if($edit_user->country_code == 379) echo 'selected';?> >Vatican City (+379)</option>
    <option data-countryCode="VE" value="58"  <?php if($edit_user->country_code == 58) echo 'selected';?> >Venezuela (+58)</option>
    <option data-countryCode="VN" value="84"  <?php if($edit_user->country_code == 84) echo 'selected';?> >Vietnam (+84)</option>
    <option data-countryCode="VG" value="84"  <?php if($edit_user->country_code == 84) echo 'selected';?> >Virgin Islands - British (+1284)</option>
    <option data-countryCode="VI" value="84"  <?php if($edit_user->country_code == 84) echo 'selected';?> >Virgin Islands - US (+1340)</option>
    <option data-countryCode="WF" value="681"  <?php if($edit_user->country_code == 681) echo 'selected';?> >Wallis &amp; Futuna (+681)</option>
    <option data-countryCode="YE" value="969"  <?php if($edit_user->country_code == 969) echo 'selected';?> >Yemen (North)(+969)</option>
    <option data-countryCode="YE" value="967"  <?php if($edit_user->country_code == 967) echo 'selected';?> >Yemen (South)(+967)</option>
    <option data-countryCode="ZM" value="260"  <?php if($edit_user->country_code == 260) echo 'selected';?> >Zambia (+260)</option>
    <option data-countryCode="ZW" value="263"  <?php if($edit_user->country_code == 263) echo 'selected';?> >Zimbabwe (+263)</option>
    </select>
</div>

            </div>

             <div class="col-lg-7 mt-3">

                <div class="text-color-gray mb-2 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold">Contact number</span>
              
             <!--  <i class="icon text-color-blue p-3" style="padding: 5px !important; padding-left: 10px !important;">
                <p class="text-color-blue font-weight-bold text-xxsmall" style="font-style: normal; margin-top: 2px;">+61 |</p>
              </i> -->
              <input type="text" placeholder="Enter Contact number" class="form-control text-xsmall" s name="contact_number" value="{{ old('contact_number' ,$edit_user->contact_number) }}">
              @if($errors->first('contact_number'))
                <span class="help-block" style="color: red;" style="color: red;">{{$errors->first('contact_number')}}</span>
              @endif 
            
          </div>

              </div>
          </div>
          


          
        </div>

        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('email') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Email address</span>
            <input type="text"  placeholder="Enter email address" class="form-control text-xsmall" name="email" value="{{ old('email' ,$edit_user->email) }}">
            @if($errors->first('email'))
              <span class="help-block" style="color: red;">{{$errors->first('email')}}</span>
            @endif 
          </div>
        </div>

        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('alternate_email') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Alternate Email address</span>
            <input type="text"  placeholder="Enter alternate email address" class="form-control text-xsmall" name="alternate_email" value="{{ old('alternate_email',$edit_user->info->alternate_email) }}">
          </div>
          @if($errors->first('alternate_email'))
            <span class="help-block" style="color: red;">{{$errors->first('alternate_email')}}</span>
          @endif
        </div>

        <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('username') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Username</span>
            <input type="text"  placeholder="Enter username" class="form-control text-xsmall" name="username" value="{{ old('username',$edit_user->username) }}">
          </div>
          @if($errors->first('username'))
            <span class="help-block" style="color: red;">{{$errors->first('username')}}</span>
          @endif 
        </div>

         <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('sl_no') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">User No</span>
            <input type="text"  placeholder="Enter Sl no" class="form-control text-xsmall" name="sl_no" value="{{ old('username',$edit_user->sl_no) }}">
          </div>
          @if($errors->first('sl_no'))
            <span class="help-block" style="color: red;">{{$errors->first('sl_no')}}</span>
          @endif 
        </div>

        <div class="col-lg-12 mt-3">
          <p class="text-xsmall text-uppercase font-semibold">Address</p>
        </div>

        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('street') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Street</span>
            <input type="text"  placeholder="Unit No / Street" class="form-control text-xsmall" name="street" value="{{ old('street',$edit_user->info->street) }}">
          </div>
          @if($errors->first('street'))
            <span class="help-block" style="color: red;">{{$errors->first('street')}}</span>
          @endif
        </div>

        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('city') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">City</span>
            <input type="text"  placeholder="City" class="form-control text-xsmall" name="city" value="{{ old('city',$edit_user->info->city )}}">
          </div>
          @if($errors->first('city'))
            <span class="help-block" style="color: red;">{{$errors->first('city')}}</span>
          @endif
        </div>

        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('state') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">State</span>

             <input type="text"  placeholder="State" class="form-control text-xsmall" name="state" value="{{ old('state',$edit_user->info->state )}}">

            
          </div>
          @if($errors->first('state'))
            <span class="help-block" style="color: red;">{{$errors->first('state')}}</span>
          @endif
        </div>

        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('post_code') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Post Code</span>
            <input type="text"  placeholder="Post Code" class="form-control text-xsmall" name="post_code" value="{{ old('post_code',$edit_user->info->post_code) }}">
          </div>
          @if($errors->first('post_code'))
            <span class="help-block" style="color: red;">{{$errors->first('post_code')}}</span>
          @endif
        </div>


    <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('country') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Country</span>
            <input type="text"  placeholder="Country" class="form-control text-xsmall" name="country" value="{{ old('country',$edit_user->info->country) }}">
          </div>
          @if($errors->first('country'))
            <span class="help-block" style="color: red;">{{$errors->first('country')}}</span>
          @endif
        </div>




    <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('birthday') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Birthday</span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="birthday" value="{{ old('birthday',$edit_user->info->birthday) }}">
          </div>
          @if($errors->first('birthday'))
            <span class="help-block" style="color: red;">{{$errors->first('birthday')}}</span>
          @endif
        </div>


<div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('annervcy') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Anniversary</span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="annervcy" value="{{ old('birthday',$edit_user->info->annervcy) }}">
          </div>
          @if($errors->first('annervcy'))
            <span class="help-block" style="color: red;">{{$errors->first('annervcy')}}</span>
          @endif
        </div>










    

        <div class="col-lg-12 mt-3">
            <p class="text-xsmall font-semibold text-uppercase">Business Classification (Cleaning, Accounting, FX, Infotech)</p>
        </div>
       
        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('business_group') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Classification</span>
             {!!Form::select('business_group',$service_group,old('business_group',$edit_user->info->business_group),['class' => "form-control text-xsmall input-sm select2", 'id' => "business_group"])!!}
          </div>
          @if($errors->first('business_group'))
            <span class="help-block" style="color: red;">{{$errors->first('business_group')}}</span>
          @endif
        </div>


        @if($edit_user->type == "area_head")
        <div class="col-lg-6 mt-3" id="user_head">
          <div class="text-color-gray mb-2">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Select Big Boss Head</span>
            {!!Form::select('user_head',$business_name,old('user_head',$edit_user->head_id),['class' => "form-control text-xsmall input-sm select2", 'id' => "user_head"])!!}
          </div>
        </div>
        <div class="col-lg-12" id="area_head_input">
          <div class="row">
            <div class="col-lg-12 mt-4" id="business_label">
              <p class="text-color-gray font-semibold">Business Details</p>
            </div>
            <div class="col-lg-12 mt-2" id="avatar_container">
              <div class="text-color-gray mb-2 {{$errors->first('business_avatar') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Establishment Avatar</span>
              </div>
              @if($edit_user->business_avatar)
                <img src="{{"{$edit_user->business_avatar->directory}/resized/{$edit_user->business_avatar->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
              @endif
              <div class="d-flex flex-row mb-2 mt-1">
                <input type="file"  name="business_avatar[]"/> 
              </div>
              @if($errors->first('business_avatar'))
                <span class="help-block" style="color: red;">{{$errors->first('business_avatar')}}</span>
              @endif
            </div> 
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('business_name') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Business Name</span>
                <input type="text"  placeholder="Enter Establishment Name" class="form-control text-xsmall" name="business_name" value="{{old('business_name',$edit_user->info->business_name)}}">
              </div>
              @if($errors->first('business_name'))
                <span class="help-block" style="color: red;">{{$errors->first('business_name')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('business_number') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Australian Business Number</span>
                <input type="text"  placeholder="Enter ABN" class="form-control text-xsmall" name="business_number" value="{{old('business_number',$edit_user->info->business_number)}}">
              </div>
              @if($errors->first('business_number'))
                <span class="help-block" style="color: red;">{{$errors->first('business_number')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('tax_number') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Tax File Number</span>
                <input type="text"  placeholder="Enter TFN" class="form-control text-xsmall" name="tax_number" value="{{old('tax_number',$edit_user->info->tax_number)}}">
              </div>
              @if($errors->first('tax_number'))
                <span class="help-block" style="color: red;">{{$errors->first('tax_number')}}</span>
              @endif
            </div>
            <div class="col-lg-12 mt-3">
              <p class="text-xsmall font-semibold">Business Address</p>
            </div>
            <div class="col-lg-4 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('business_street') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Street</span>
                <input type="text"  placeholder="Unit No / Street" class="form-control text-xsmall" name="business_street" value="{{old('business_street',$edit_user->info->business_street)}}">
              </div>
              @if($errors->first('business_street'))
                <span class="help-block" style="color: red;">{{$errors->first('business_street')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('business_city') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">City</span>
                <input type="text"  placeholder="Unit No / Street" class="form-control text-xsmall" name="business_city" value="{{old('business_city',$edit_user->info->business_city)}}">
              </div>
              @if($errors->first('business_city'))
                <span class="help-block" style="color: red;">{{$errors->first('business_city')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('business_state') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">State</span>
                {!!Form::select('business_state',$reg,old('business_state',$edit_user->info->business_state),['class' => "form-control input-sm select2"])!!}
              </div>
              @if($errors->first('business_state'))
                <span class="help-block" style="color: red;">{{$errors->first('business_state')}}</span>
              @endif                             
            </div>
            <div class="col-lg-3 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('business_post_code') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Post Code</span>
                <input type="text"  placeholder="Post Code" class="form-control text-xsmall" name="business_post_code" value="{{old('business_post_code',$edit_user->info->business_post_code)}}">
              </div>
              @if($errors->first('business_post_code'))
                <span class="help-block" style="color: red;">{{$errors->first('business_post_code')}}</span>
              @endif              
            </div>
            <div class="col-lg-12 mt-4">
              <p class="text-color-gray font-semibold">Business Equipment Photos</p>
            </div>
            <div class="col-lg-6 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('file') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Utilitiy Vehicle</span>
              </div>
              @if($edit_user->utility_vehicle)
                <img src="{{"{$edit_user->utility_vehicle->directory}/resized/{$edit_user->utility_vehicle->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
              @endif
              <div class="d-flex flex-row mb-2 mt-1">
                <input type="file"  name="utility_vehicle[]"/> 
              </div>
              @if($errors->first('file'))
                <span class="help-block" style="color: red;">{{$errors->first('file')}}</span>
              @endif
            </div>
             <div class="col-lg-6 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('file') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Staff Uniform</span>
              </div>
              @if($edit_user->staff_uniform)
                <img src="{{"{$edit_user->staff_uniform->directory}/resized/{$edit_user->staff_uniform->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
              @endif
              <div class="d-flex flex-row mb-2 mt-1">
                <input type="file"  name="staff_uniform[]"/> 
              </div>
              @if($errors->first('file'))
                <span class="help-block" style="color: red;">{{$errors->first('file')}}</span>
              @endif
            </div>
          </div>
        </div>
        @endif

        @if($edit_user->type == "franchisee")
        <div class="col-lg-12" id="franchisee">
          <div class="row">
            <div class="col-lg-12 mt-3">
              <p class="text-xsmall font-semibold">Staff Classification</p>
            </div>
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('user_designation') ? 'has-error' : NULL}}">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">User Designation</span>
                <input type="text"  placeholder="Enter Designation" class="form-control text-xsmall" name="user_designation" value="{{old('user_designation' ,$edit_user->info->user_designation)}}">
              </div>
              @if($errors->first('user_designation'))
                <span class="help-block" style="color: red;">{{$errors->first('user_designation')}}</span>
              @endif
            </div> 
            <div class="col-lg-6 mt-3">
              <div class="text-color-gray mb-2">
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <i class="fas fa-chevron-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Choose Area Head</span>
                  @if(Auth::user()->type == "area_head")
                   {!!Form::select('franchisee_head',$business_area_head,old('franchisee_head',Auth::user()->id),['class' => "form-control text-xsmall input-sm select2", 'id' => "franchisee" , 'disabled' => "disabled"])!!}
                   <input type="hidden" name="franchisee_head" value="{{Auth::user()->id}}">
                  @else
                  {!!Form::select('franchisee_head',$business_area_head,old('franchisee_head',$edit_user->head_id),['class' => "form-control text-xsmall input-sm select2", 'id' => "franchisee_head"])!!}
                  @endif
              </div>
             
            </div> 
          </div>
        </div>
        @endif

        <div class="col-lg-12 mt-4">
          <p class="text-color-gray font-semibold text-uppercase text-xsmall">User Requirements</p>
        </div>
        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('police_check') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Police Check</span>
          </div>
          @if($edit_user->police_check)
            <img src="{{"{$edit_user->police_check->directory}/resized/{$edit_user->police_check->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
          @endif
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file"  name="police_check[]"/> 
          </div>
          @if($errors->first('police_check'))
            <span class="help-block" style="color: red;">{{$errors->first('police_check')}}</span>
          @endif
        </div> 


<div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('policy_ex') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">  policy_ex </span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="policy_ex" value="{{ old('policy_ex',$edit_user->info->policy_ex) }}">
          </div>
          @if($errors->first('policy_ex'))
            <span class="help-block" style="color: red;">{{$errors->first('policy_ex')}}</span>
          @endif
        </div>







 <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('awareness_certificate') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Asbestos Awareness Certificate</span>
          </div>
          @if($edit_user->awareness_certificate)
            <img src="{{"{$edit_user->police_check->directory}/resized/{$edit_user->awareness_certificate->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
          @endif
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file"  name="awareness_certificate[]"/> 
          </div>
          @if($errors->first('awareness_certificate'))
            <span class="help-block" style="color: red;">{{$errors->first('awareness_certificate')}}</span>
          @endif
        </div> 









       <!-- <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('awareness_certificate') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Asbestos Awareness Certificate</span>
          </div>
          @if($edit_user->awareness_certificate)
            <img src="{{"{$edit_user->awareness_certificate->directory}/resized/{$edit_user->awareness_certificate->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
          @endif
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="awareness_certificate[]" /> 
          </div>
          @if($errors->first('awareness_certificate'))
            <span class="help-block" style="color: red;">{{$errors->first('awareness_certificate')}}</span>
          @endif
        </div> -->



<div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('asbes_ex') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">  Asbestos Awareness Expire </span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="asbes_ex" value="{{ old('asbes_ex',$edit_user->info->asbes_ex) }}">
          </div>
          @if($errors->first('asbes_ex'))
            <span class="help-block" style="color: red;">{{$errors->first('asbes_ex')}}</span>
          @endif
        </div>
















        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('insurance') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Insurance</span>
          </div>
          @if($edit_user->insurance)
           <img src="{{"{$edit_user->insurance->directory}/resized/{$edit_user->insurance->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
          @endif
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="insurance[]"/> 
          </div>
          @if($errors->first('insurance'))
            <span class="help-block" style="color: red;">{{$errors->first('insurance')}}</span>
          @endif
        </div>


<div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('insurnce_ex') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">  Insurance Expire </span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="insurnce_ex" value="{{ old('insurnce_ex',$edit_user->info->insurnce_ex) }}">
          </div>
          @if($errors->first('insurnce_ex'))
            <span class="help-block" style="color: red;">{{$errors->first('insurnce_ex')}}</span>
          @endif
        </div>












        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('first_aid_license') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">First Aid License</span>
          </div>
          @if($edit_user->first_aid_license)
          <img src="{{"{$edit_user->first_aid_license->directory}/resized/{$edit_user->first_aid_license->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
          @endif
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file"  name="first_aid_license[]"/> 
          </div>
          @if($errors->first('first_aid_license'))
            <span class="help-block" style="color: red;">{{$errors->first('first_aid_license')}}</span>
          @endif
        </div>





<div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('first_ex') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">  First Aid  License Expire </span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="first_ex" value="{{ old('first_ex',$edit_user->info->first_ex) }}">
          </div>
          @if($errors->first('first_ex'))
            <span class="help-block" style="color: red;">{{$errors->first('first_ex')}}</span>
          @endif
        </div>








        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('whitecard') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Whitecard</span>
          </div>
          @if($edit_user->whitecard)
          <img src="{{"{$edit_user->whitecard->directory}/resized/{$edit_user->whitecard->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
          @endif
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="whitecard[]"/>
          </div>
          @if($errors->first('whitecard'))
            <span class="help-block" style="color: red;">{{$errors->first('whitecard')}}</span>
          @endif
        </div>




<div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('whitecard_ex') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold "> Whitecard Expire </span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="whitecard_ex" value="{{ old('whitecard_ex',$edit_user->info->whitecard_ex) }}">
          </div>
          @if($errors->first('whitecard_ex'))
            <span class="help-block" style="color: red;">{{$errors->first('whitecard_ex')}}</span>
          @endif
        </div>












        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('other_documents') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Other Qualifying Documents</span>
          </div>
          @if($edit_user->other_docs)
          <img src="{{"{$edit_user->other_docs->directory}/resized/{$edit_user->other_docs->filename}"}}" alt="" class="img-thumbnail" width="200" style="object-fit:fill;height:150px">
          @endif
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="other_documents[]"/>
          </div>
          @if($errors->first('other_documents'))
            <span class="help-block" style="color: red;">{{$errors->first('other_documents')}}</span>
          @endif
        </div> 




<div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('other_ex') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Other Qualifying Documents Expire </span>
            <input type="date"  placeholder="Post Code" class="form-control text-xsmall" name="other_ex" value="{{ old('other_ex',$edit_user->info->other_ex) }}">
          </div>
          @if($errors->first('other_ex'))
            <span class="help-block" style="color: red;">{{$errors->first('other_ex')}}</span>
          @endif
        </div>






          <div class="col-lg-12 mt-3">
            <br><br>
            <h3> 
              Add Dynamic Feilds 
                <button id="add_dynamic_button" style="float: right;"   type="button" class="btn bg-color-green text-xxsmall text-uppercase text-color-white p-2 pl-3 pr-3"> + Add Dynamic Text Fields </button>

                
            </h3>
            <br><br>

            <div id="add_dynamic_template">
              <?php
              if(isset($edit_user->dynamic_text_feilds) && $edit_user->dynamic_text_feilds !=''){
                  $dynamic_text_feilds =json_decode($edit_user->dynamic_text_feilds);

                 if(isset($dynamic_text_feilds->title_dynamic_feilds) && count($dynamic_text_feilds->title_dynamic_feilds)){
                  foreach($dynamic_text_feilds->title_dynamic_feilds as $dynamic_keys => $dynamic_val){
                    ?>


                      <div class="row"> 
                        <div class="col-lg-5">
                        <div class="text-color-gray mb-2">      
                            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Dynamic Title</span>
                            <input type="text" placeholder="Dynamic Title" class="form-control text-xsmall" name="title_dynamic_feilds[]" value="<?php echo $dynamic_val;?>">
                        </div>                        
                      </div>

                      <div class="col-lg-5">
                        <div class="text-color-gray mb-2">      
                            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Dynamic Value</span>
                            <input type="text" placeholder="Dynamic Value" class="form-control text-xsmall" name="value_dynamic_feilds[]" value="<?php echo $dynamic_text_feilds->value_dynamic_feilds[$dynamic_keys];?>">
                        </div>                        
                      </div>

                      <div class="col-lg-2">

                        @if(Auth::user()->type != "franchisee")
                         <button  style="margin-top: 24px;" onclick="$(this).parent().parent().remove();" type="button" class="btn bg-color-red text-xxsmall text-uppercase text-color-white p-2 pl-3 pr-3"> Remove </button>
                        @endif
                      </div>
                  </div>


                    <?php
                  }
                }
              }
              ?>

            </div>


            <br><br>
            <h3> 
              Upload Documents
                <button id="add_dynamic_button_upload" style="float: right;"   type="button" class="btn bg-color-green text-xxsmall text-uppercase text-color-white p-2 pl-3 pr-3"> + Upload Documents PNG/JPG/PDF/CSV </button>

            </h3>
            <br><br>


            <div id="add_dynamic_template_upload">
                
                <br><br>

                 <?php
              if(isset($edit_user->dynamic_upload_files) && $edit_user->dynamic_upload_files !=''){
                  $dynamic_upload_files =json_decode($edit_user->dynamic_upload_files);
                 if(count($dynamic_upload_files)){
                    ?>

                    <table class="table bg-color-white  mb-0 ">
                    <thead>      
                        <tr>
                            <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Documents Name</th>

                            <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold"> Expiry</th>

                            <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Download /View</th>

                             <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Action</th>

                        </tr>
                    </thead>  
                        <?php
                      foreach($dynamic_upload_files as $dynamic_keys => $dynamic_val){
                        ?>
                            <tr>
                                <input type="hidden" name="old_dynamic_file[]" value="<?php echo $dynamic_val->file ;?>">
                                <input type="hidden" name="old_dynamic_name[]" value="<?php echo $dynamic_val->name ;?>">
                                <input type="hidden" name="old_dynamic_expiry[]" value="<?php echo $dynamic_val->expiry ;?>">

                                <td><?php echo $dynamic_val->name ;?></td>
                                <td><?php echo $dynamic_val->expiry ;?></td>
                                <td><a href="http://booking.bigbossgroup.com.au/<?php echo $dynamic_val->file ;?>" target="_blank"> Download / View</a></td>
                                <td>  
                                    @if(Auth::user()->type != "franchisee")
                                    <button onclick="$(this).parent().parent().remove();" type="button" class="btn bg-color-red text-xxsmall text-uppercase text-color-white p-2 pl-3 pr-3"> Remove </button>
                                    @endif
                                </td>
                            </tr>

                           

                     <?php
                      }
                      ?>
                  </table>
                      <?php
                }
              }
              ?>    
            </div>

          </div>


           <template id="dynamic_feilds_add_template_upload">
           <div class="row"> 
              <div class="col-lg-3">
              <div class="text-color-gray mb-2">      
                  <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                  <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Documents Name</span>
                  <input type="text" placeholder="Documents Name" class="form-control text-xsmall" name="documents_name_dynamic[]" value="">
              </div>                        
            </div>

            <div class="col-lg-3">
              <div class="text-color-gray mb-2">      
                  <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                  <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Expiry</span>
                  <input type="date" placeholder="Expiry" class="form-control text-xsmall" name="documents_expiry_dynamic[]" value="">
              </div>                        
            </div>

            <div class="col-lg-3">
              <div class="text-color-gray mb-2">      
                  <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                  <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Upload Documents</span>
                  <input type="file"  class="form-control text-xsmall" name="upload_documents_dynamic[]" >
              </div>                        
            </div>

            <div class="col-lg-2">
               <button  style="margin-top: 24px;" onclick="$(this).parent().parent().remove();" type="button" class="btn bg-color-red text-xxsmall text-uppercase text-color-white p-2 pl-3 pr-3"> Remove </button>
            </div>
        </div>
          </template>

          <template id="dynamic_feilds_add_template">
           <div class="row"> 
              <div class="col-lg-5">
              <div class="text-color-gray mb-2">      
                  <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                  <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Dynamic Title</span>
                  <input type="text" placeholder="Dynamic Title" class="form-control text-xsmall" name="title_dynamic_feilds[]" value="">
              </div>                        
            </div>

            <div class="col-lg-5">
              <div class="text-color-gray mb-2">      
                  <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                  <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Dynamic Value</span>
                  <input type="text" placeholder="Dynamic Value" class="form-control text-xsmall" name="value_dynamic_feilds[]" value="">
              </div>                        
            </div>

            <div class="col-lg-2">
               <button  style="margin-top: 24px;" onclick="$(this).parent().parent().remove();" type="button" class="btn bg-color-red text-xxsmall text-uppercase text-color-white p-2 pl-3 pr-3"> Remove </button>
            </div>
        </div>
          </template>







        <div class="col-lg-12 mt-3">
            <button class="btn bg-color-blue text-xxsmall text-color-white p-2 pl-3 pr-3"> UPDATE USER<i class="ml-2 fas fa-chevron-right text-color-white" style="font-size: 12px;"></i><i class="fas fa-chevron-right text-color-white" style="font-size: 12px;"></i> </button>
        </div> 
      </div>
    </form>
  </div>
</div>
  @stop
@section('page-scripts')
 <script type="text/javascript">
  $(function(){
      $("#add_dynamic_button").click(function(){
           $("#add_dynamic_template").append($("#dynamic_feilds_add_template").html());
       });

      $("#add_dynamic_button_upload").click(function(){
           $("#add_dynamic_template_upload").append($("#dynamic_feilds_add_template_upload").html());
       });


  })  
</script>
@stop