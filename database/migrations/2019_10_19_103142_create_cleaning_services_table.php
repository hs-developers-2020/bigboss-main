<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCleaningServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleaning_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('postcode')->nullable();
            $table->string('location')->nullable();
            $table->string('property_type')->nullable();
            $table->string('service_type')->nullable();
            $table->string('services')->nullable();
            $table->string('booking_id')->nullable();
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->date('booking_date')->nullable();
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->date('booking_date_secondary')->nullable();
            $table->time('secondary_time_start')->nullable();
            $table->time('secondary_time_end')->nullable();
            $table->string('payment')->nullable();
            $table->string('status')->nullable()->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cleaning_services');
    }
}
