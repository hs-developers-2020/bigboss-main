   
<style type="text/css">
 .text-color-light {
  color: #ADADAD !important;
}
a {
  color: #ADADAD !important;
}
</style>
    <nav class="navbar card-shadow navbar-expand-md navbar-dark pb-4 pt-3 bg-color-white position fixed-top">
    <div class="container">
   <a href="{{route('system.dashboard')}}" class="navbar-brand" style="width:  100px !important; margin-right: 0px !important;"> 
        <img src="{{ asset('frontend/images/main-logo.png') }}"  class="img-fluid logo-size w-100">
      </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-list text-color-red"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item p-2 mr-5 pr-5">
       
      </li>
      <li class="nav-item p-2">
        <a class="decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.dashboard','system.customer-feedback')) ? 'active' : ''}}" href="{{ route('system.dashboard') }}"><i class="fas fa-home mr-2"></i>Home</a>
      </li>
      @if(in_array(Str::lower(Auth::user()->type), ['super_user','head','area_head']))
      <li class="nav-item p-2">
        <a class="decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.users.index','system.users.create-user','system.users.edit-user','system.users.user-profile','system.users.work-load')) ? 'active' : ''}}" href="{{ route('system.users.index') }}"><i class="fas fa-users mr-2"></i>Users</a>
      </li>
        @if(in_array(Str::lower(Auth::user()->type), ['super_user','head']))
      

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.services.index','system.services.create','system.services.create','system.media-library.index','system.media-library.create','system.media-library.edit')) ? 'active' : ''}}" href="#" id="navbarDropdown" data-toggle="dropdown" ><i class="fas fa-cog mr-2">
            </i>Services
          </a>
          <div class="dropdown-menu" >
            <a class="dropdown-item" href="{{ route('system.services.index') }}">All Records</a>
            <a class="dropdown-item" href="{{ route('system.media-library.index') }}">Media Library</a>
          </div>
        </li>
        @endif
      @endif
      <li class="nav-item p-2">
       
        <a class="decoration-none pr-3 {{ in_array(Route::currentRouteName(),  array('system.audit-trail.index','system.audit-trail.show')) ? 'active' : ''}}" href="{{ route('system.audit-trail.index') }}"><i class="fas fa-clipboard mr-2"></i>Audit Trail</a>
      </li>

    
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.finance.index','system.finance.finance-history','system.invoice.index','system.finance.create')) ? 'active' : ''}}" href="#" id="navbarDropdown" data-toggle="dropdown" ><i class="fas fa-calculator mr-2">
          </i>Finance
        </a>
        <div class="dropdown-menu" >
          <a class="dropdown-item" href="{{ route('system.finance.index') }}">Dashboard</a>
          <a class="dropdown-item" href="{{ route('system.finance.finance-history') }}">All Records</a>
          <a class="dropdown-item" href="{{ route('system.finance.create') }}">Add Expenses</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('system.invoice.index') }}">Invoice</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        {{-- <a class="decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.jobs.index','system.jobs.show','system.jobs.details')) ? 'active' : ''}}" href="{{ route('system.jobs.index') }}"><i class="fas fa-briefcase mr-2"></i>Jobs</a> --}}
        <a class="nav-link dropdown-toggle decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.jobs.index','system.jobs.show','system.jobs.details')) ? 'active' : ''}}" href="#" id="navbarDropdown" data-toggle="dropdown"><i class="fas fa-briefcase mr-2"></i>Jobs</a>
        <div class="dropdown-menu" >
          <a class="dropdown-item" href="{{ route('system.jobs.index') }}">Dashboard</a>
          <a class="dropdown-item" href="{{ route('system.jobs.cancelled') }}">Cancelled Jobs</a>
          <a class="dropdown-item" href="{{ route('system.jobs.unserviced') }}">Unserviced Jobs</a>
        </div>
      </li>
      @if(Auth::user()->type == "franchisee")
      <li class="nav-item p-2">
        <a class="decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.users.user-profile','system.users.work-load')) ? 'active' : ''}}" href="{{ route('system.users.user-profile',[Auth::user()->id]) }}"><i class="fas fas fa-address-book mr-2"></i>Profile</a>

        <a class="decoration-none pr-3" href="http://booking.bigbossgroup.com.au/admin/users/edit-user/{{Auth::user()->id}}"><i class="fas fas fa-address-book mr-2"></i>Update Profile Details</a>

      </li>
      @endif

      @if(Auth::user()->type == "area_head")
      <li class="nav-item p-2">
        <a class="decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.contract.index','system.contract-group.list','system.contract-group.index','system.contract-group.create','system.contract-group.show')) ? 'active' : ''}}" href="{{ route('system.contract-group.list',[Auth::user()->id]) }}"><i class="fas fa-file mr-2"></i>Files</a>
      </li>
      @else
      <li class="nav-item p-2">
        <a class="decoration-none pr-3 {{ in_array(Route::currentRouteName(), array('system.contract.index','system.contract-group.list','system.contract-group.index','system.contract-group.create','system.contract-group.show')) ? 'active' : ''}}" href="{{ route('system.contract.index') }}"><i class="fas fa-file mr-2"></i>Files</a>
      </li>
      @endif


       
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="dropdown">
          <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="{{$auth->avatar}}" alt="Avatar"><span class="user-name"  style="padding-left: 15px;">{{$auth->firstname}}</span></a>
          <ul role="menu" class="dropdown-menu account" style="padding-bottom: 10px;">
            <li>
              <div class="user-info">
                <div class="user-name">Hello! {{$auth->full_name}}</div>
               
              </div>
            </li>
            @if(in_array(Str::lower(Auth::user()->type), ['area_head','franchisee','head']))
            <li><a href="{{ route('system.users.profile',[$auth->id]) }}"><span class="icon mdi mdi-face"></span> My Account</a></li>
            @endif

            @if(in_array(Str::lower(Auth::user()->type), ['super_user']))
        <li >
        <a  href="{{ route('system.jobs.reports') }}"><span class="icon mdi mdi-face"></span> Report </a> <br>
      </li>
       @endif

            <li><a href="{{route('system.logout')}}"><span class="icon mdi mdi-power"></span> Logout</a></li>
          </ul>
        </li>
    
     
    </ul>

  </div>
</div>
</nav>