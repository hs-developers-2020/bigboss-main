@extends('system._layouts.main')
<style type="text/css">
	footer {
		margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 9% !important;
}


.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
		<!-- header starts -->
	
			<!-- Begin page content -->
			<div class="container ">
				<div class="row">
					<div class="col-lg-12  pb-3">
                        <h6 class="text-uppercase text-xsmall spacing-1 text-color-gray">Customer FeedBack</h6>
                    </div>

                        <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="card rounded-0 border-0 mb-3">
                            <div class="card-header rounded-0">
                              
                              
                            </div>
                            <div class="card-body  userlist_large text-center">
                                <div class="media mt-0">
                                    <figure class="avatar150 rounded-circle  border">
                                        <img src="{{ asset('frontend/images/user.jpg') }}" alt="user image">
                                    </figure>
                                    <div class="media-body">
                                        <h4 class="mt-0 text-color-blue font-semibold">Peter Cruiser</h4>
                                        <p class="mb-2">Sydney</p> 
                                         <p class="mb-2">+91 000 000 0000</p>                                
                                        <p class="mb-2">petercruiser@example.com</p>                                         
                                    </div>
                                </div>                                                                                    
                            </div>
                        </div>
                       {{--  <div class="card rounded-0 border-0 mb-3">
                            <div class="card-body">
                                <p class="mb-2">Ticket Details</p> 
                                <div class="form-group">
                                    <div class="text-color-gray mb-2"><i class="fas fa-chevron-right text-color-blue" style="font-size: 12px;"></i><i class="fas fa-chevron-right text-color-blue" style="font-size: 12px;"></i><span class="ml-2 text-xxsmall font-semibold ">Status</span></div>
                                    <select class="form-control bg-none ">
                                        <option>On going</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="text-color-gray mb-2"><i class="fas fa-chevron-right text-color-blue" style="font-size: 12px;"></i><i class="fas fa-chevron-right text-color-blue" style="font-size: 12px;"></i><span class="ml-2 text-xxsmall font-semibold ">Assign to</span></div>
                                    <select class="form-control bg-none ">
                                        <option>Franchisor</option>
                                    </select>
                                </div>
                                <button class="btn bg-color-blue text-xxsmall text-color-white p-2 pl-3 pr-3"> APPLY<i class="ml-2 fas fa-chevron-right text-color-white" style="font-size: 12px;"></i><i class="fas fa-chevron-right text-color-white" style="font-size: 12px;"></i> </button>
                            </div>
                        </div> --}}
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-9 ">
                        <div class="card rounded-0 mb-3 border-0">
                            <div class="card-body p-4">
                                <div class="d-flex justify-content-between">
                                        <h5 class="f-light mb-3 text-small text-color-blue font-semibold">Booking ID: 0258898895</h5>
                                        <div>
                                            <span class="text-color-blue font-weight-bold">5</span>
                                            <i class="fas fa-star text-color-blue"></i>
                                            <i class="fas fa-star text-color-blue"></i>
                                            <i class="fas fa-star text-color-blue"></i>
                                            <i class="fas fa-star text-color-blue"></i>
                                            <i class="fas fa-star text-color-blue"></i>
                                        </div>
                                </div>
                                <p class="text-color-light text-xsmall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                <div class="d-flex flex-row pb-3">
                                <p class="text-color-blue font-semibold mt-3">Service Done by: </p>  
                                <figure class="avatar150 rounded-circle border ml-3" style="    min-width: 0px !important;
                                    height: 50px !important;
                                    width: 50px !important;">
                                        <img src="{{ asset('frontend/images/user.jpg') }}" alt="user image">
                                    </figure>  
                                    <p class="mt-3 ml-3 text-color-light text-xsmall">Lorem ipsum | da cleaners</p>  
                                    </div>                            
                                    <div class="row mb-3">
                                    <div class="col-12 col-sm-6">
                                        <ul class="mb-1 list-unstyled">                                         
                                            <li class="text-color-blue font-semibold text-xxsmall">Sent <span class="text-color-light"> 26-12-2018 05:46 am</span></li>
                                            <li class="text-color-blue font-semibold mt-3 text-xxsmall">Assigned to: <span class="text-color-light"> Maxartkiller</span></li>
                                        </ul>                                
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <ul class="mb-1 list-unstyled">
                                            <li class="text-color-blue font-semibold text-xxsmall">Last Responded on:<span class="text-color-light"> 26-12-2018 05:46 am</span></li>
                                            <li class="text-color-blue font-semibold mt-3 text-xxsmall">Status <button class="btn bg-color-blue text-xxxsmall ml-2 rounded-3 text-color-white">Ongoing</button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <hr>
                                        <div class="d-flex justify-content-between">
                                                <h5 class="f-light pb-3 pt-2 text-small text-color-blue font-semibold">Discussion History</h5>
                                                <h6 class="text-color-light pt-2">Answered by: Peter lorem</h6>
                                         </div>
                                                {{-- no chat box --}}
                                        {{-- <div class="p-5 text-center">
                                            <i class="fas fa-file-alt fa-5x text-color-light"></i>
                                            <p class="text-xsmall text-color-light mt-3">There is no staff assigned to this ticket, <br> assign a staff to proceed.</p>
                                            <button class="btn bg-color-blue text-color-white text-xsmall">Answer Ticket</button>
                                        </div> --}}

                                        <div class="tab-content" id="myTabContent">
                                          <div class="tab-pane fade show active">
                                              <ul class="chat mt-2">
                                                    <li class="left clearfix">
                                                        <figure class="chat-img avatar40 rounded-circle" data-toggle="modal"  data-target="#profilemodal">
                                                            <img  src="{{ asset('frontend/images/user.jpg') }}" alt="User Avatar" class="img-circle mCS_img_loaded">
                                                        </figure>
                                                        <div class="chat-body rounded clearfix bg-color-blue text-color-white py-3">
                                                            <p class="mt-2 mb-2 text-xsmall"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales. </p>
                                                            <small class="d-flex justify-content-end"><i class="fa fa-clock-o fa-fw"></i> 12 mins ago </small>
                                                        </div>
                                                    </li>
                                                    <li class="right clearfix">
                                                        <figure class="chat-img avatar40 rounded-circle">
                                                            <img  src="{{ asset('frontend/images/user.jpg') }}" alt="User Avatar" class="img-circle mCS_img_loaded">
                                                        </figure>
                                                        <div class="chat-body rounded clearfix py-3 text-color-gray">
                                                            <p class="mt-2 mb-2 text-xsmall"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales. </p>
                                                            <small class=""><i class="fa fa-clock-o fa-fw"></i> 13 mins ago</small>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <br>
                                                <div class=" bg-white rounded-0">
                                                    <div class="card-body pb-0">
                                                        <div class="row">
                                                            <div class="col-lg-8" style="padding-right: 0px !important;">
                                                            <input type="text" class="form-control text-xsmall" name="" placeholder="Enter message">  
                                                        </div>
                                                            <div class="col-lg-2" style="padding-left: 0px !important;">                                               
                                                                <div class="upload-btn-wrapper">
                                                                    <button class="btn text-color-light text-xxsmall text-uppercase"><i class="fas fa-download mr-2 text-color-light "></i>Upload a file</button>
                                                                    <input type="file" name="myfile" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <button class="btn bg-color-blue text-color-white text-xxsmall w-100 text-uppercase"><i class="fas fa-paper-plane mr-2 text-color-white"></i>Send</button>
                                                            </div>
                                                        </div>
                                                    </div>                                            
                                                </div>
                                          </div>                                          
                                        </div>
                                    </div>                              
                                    </div>                                  
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <!-- main container ends -->       
</div>
@stop