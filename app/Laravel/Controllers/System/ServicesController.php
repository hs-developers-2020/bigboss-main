<?php


namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Service;
use App\Laravel\Models\MediaLibrary;
use App\Laravel\Models\Regions;



/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ServiceRequest;
use App\Laravel\Events\AuditTrailActivity;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, Auth, ImageUploader,Input,PDF,FileUploader,File,DB,AuditRequest,Event;
class ServicesController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"Choose Service",'cleaning' => "Cleaning","information_technology" => "Information Technology","accounting" => "Accounting"];
		$this->data['type_service'] = ['home' => "House","office" => "Office"];
		$this->data['reg'] = ['' => "Choose Regions"] + Regions::pluck('name', 'code')->toArray();
		$this->data['media_lib'] = ['' => "Choose Icons"] + MediaLibrary::pluck('name', 'id')->toArray();
		$this->data['media_type'] = ['1' => "Choose from your files","2" => "Choose from Media Library"];
		
	}

  	public function index(PageRequest $request){
  		$this->data['group'] = $request->get('group',false);
  		$this->data['keyword'] = $request->get('keyword', NULL);
  		$this->data['services'] = Service::where('id','<>','1')->group($this->data['group'])->orderBy('created_at' , "DESC")->paginate(10);

		return view ('system.services.index',$this->data);
	}

	 public function create(){
		return view ('system.services.create',$this->data);

	}

	public function store (ServiceRequest $request) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		$media_library = MediaLibrary::find($request->get('media_library'));

		try {
			$new_service = new Service;
			$new_service->fill($request->all());
			$new_service->icon_id = $request->get('media_library');
			$new_service->icon_type = $request->get('media_type');
			if ($request->get('service_type')) {
				$new_service->service_type = implode(',', $request->get('service_type'));
			}
			
			if ($media_library) {
			    $new_service->path = $media_library->path;
			    $new_service->directory =$media_library->directory;
			    $new_service->filename = $media_library->filename;
			}

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/user_profile");
			    $new_service->path = $image['path'];
			    $new_service->directory = $image['directory'];
			    $new_service->filename = $image['filename'];
			}

			if($new_service->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "SERVICE_CREATED", 'remarks' => Auth::user()->full_name." has successfully created {$new_service->name} service information. [{$new_service->group}]",'ip' => $ip , 'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.services.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}

	public function edit ($id = NULL) {
	
		$services = Service::find($id);
		$this->data['type'] = explode(',', $services->service_type);
		
		if (!$services) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.services.index');
		}

		$this->data['services'] = $services;
		return view('system.services.edit',$this->data);
	}

	public function update (ServiceRequest $request, $id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$services = Service::find($id);
			$media_library = MediaLibrary::find($request->get('media_library'));

			$services->fill($request->all());
			$services->icon_id = $request->get('media_library');
			$services->icon_type = $request->get('media_type');

			if ($request->get('service_type')) {
				$services->service_type = implode(',', $request->get('service_type'));
			}
			
			if ($media_library) {
			    $services->path = $media_library->path;
			    $services->directory =$media_library->directory;
			    $services->filename = $media_library->filename;
			}

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/user_profile");
			    $services->path = $image['path'];
			    $services->directory = $image['directory'];
			    $services->filename = $image['filename'];
			}
			if (!$services) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.services.index');
			}

			

			if($services->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "SERVICE_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$services->name} service information. [{$services->group}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.services.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$services = Service::find($id);

			if (!$services) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.services.index');
			}

			if($services->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "SERVICE_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$services->name} service information. [{$services->group}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.services.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


	public function update_day (PageRequest $request, $id = NULL) {
		try {

			$services = Service::find($id);

			if (!$services) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.services.index');
			}

			//dd($request->input('regions'));

			
			if($request->get('days_available')){
			$services->days_available = implode(',', $request->get('days_available'));
			}

			$services->regions = $request->input('regions');

			if($services->save()) {
				if($request->get('days_available')){
					session()->flash('notification-status','success');
					Session()->flash('alert-type','success');
					session()->flash('notification-msg',"Record has been modified successfully.");
				}else{
					Session()->flash('alert-type','failed');
					session()->flash('notification-msg',"Something went wrong.");
				}
				return redirect()->route('system.services.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	
	

}
