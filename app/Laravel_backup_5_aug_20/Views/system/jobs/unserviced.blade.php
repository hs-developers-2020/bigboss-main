@extends('system._layouts.main')
<style type="text/css">
footer {
	margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
    <div class="container ">
    
        <form action="" id="search_filter" >
            <div class="row">
                <div class="col-lg-4 pt-2">
                    <h6 class="text-xsmall spacing-1 text-color-gray">Unserviced Jobs</h6>
                </div>
               
                {{-- <div class="col-lg-2">     
                   {!!Form::select('progress',$job_progress,old('progress',$type),["class" => "form-control text-color-blue font-semibold", 'id' => 'progress'])!!}
                </div> --}}
                <div class="col-lg-8 mb-2 d-flex flex-row ">   
                    <input type="text" class="searchTerm" name="keyword" value="{{$keyword}}" placeholder="Search keyword such as name">
                    <a href="#"  class="btn bg-color-blue text-color-white searchButton" id="button_search">
                        <i class="fa fa-search"></i>
                    </a>
                   
                </div>       
            </div>
        </form>
        <table class="table bg-color-white  mb-0 jobs-show">
            <thead>
                <tr>
                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Username</th>
                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Contact Number</th>
                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">Service Type</th>
                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Service Fee</th>
                      <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Date Scheduled</th>
                    <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Time Submitted</th>                                                                   
                    <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Status</th>
                    <!-- <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="">Action</th> -->
                </tr>
            </thead>
            <tbody>
                @forelse($jobs as $index => $value)
                <tr>
                <td class="userlist p-4">
                    <div class="media">
                        <div class="figure avatar40 border rounded-circle align-self-start">
                            <label class="checkbox-user-check">
                                <input type="checkbox">
                                <i class="fa fa-check"></i>
                            </label>
                            <img src="{{ asset('frontend/images/user.jpg') }}" alt="Generic placeholder image" class="">
                        </div>
                        <div class="media-body">
                            <a class=" text-color-white" style="text-decoration: none;" href="{{ route('system.jobs.details',[$value->id]) }}">
                            <h5 class="text-color-gray">{{Str::title($value->full_name)}}
                                <span class="float-right text-muted"></span>
                            </h5>
                            <p class="mb-0">{{$value->location}}</p>
                        </a>
                        </div>
                    </div>
                </td>
                <td>
                    <h5 class="text-color-gray p-0">+61 {{$value->contact}}</h5>
                    
                </td>
                <td>
                    <h5 class="text-color-gray p-0">{{Str::title(str_replace("_", " ", $value->type))}}</h5>
                    <p class="mb-0">{{Str::title($value->property_type)}}</p>
                </td>
                <td>
                    <h5 class="text-color-gray p-0">${{Helper::amount($value->booking_total)}}</h5>
                    <p class="mb-0">{{Str::title($value->service_type)}}</p>
                </td>

<?php 
$datebooking11= str_replace('/', '-',$value->booking_date);
 $newdatebf = date("d-m-Y", strtotime($datebooking11));
?>
                <td>
                    <h5 class="text-color-gray p-0">{{$newdatebf}}</h5>
                    <p class="mb-0">{{Helper::day_only($value->booking_date)}}</p>
                </td>
                <td>
                    @if($value->time_start)
                        <h5 class="text-color-gray p-0">{{Helper::time_only($value->time_start)}}</h5>
                        <p class="mb-0">to {{Helper::time_only($value->time_end)}}</p>
                    @else
                        <h5 class="text-color-gray p-0">{{$value->time_start ?: "N/A"}}</h5>
                        <p class="mb-0">to {{$value->time_end ?: "N/A"}}</p>
                    @endif
                </td>
                <td>
                    <a href="{{route('system.jobs.details',$value->id)}}" class="btn btn-warning rounded-3 text-color-white text-xxsmall">{{Str::title(Str_replace("_" , " ",$value->status))}}</a>
                   
                </td>
               <!--  <td>
                   <div class="dropdown ml-3">
                     <span>
                       <i class="fas fa-circle f-10"></i>
                       <i class="fas fa-circle f-10"></i>
                       <i class="fas fa-circle f-10"></i></span>
                       <div class="dropdown-content">
                           <div class="d-flex flex-column mt-1">
                               <a style="font-size: 16px;" class="mb-2 text-color-light font-semibold" style="text-decoration: none;"  href="{{route('system.jobs.details',$value->id)}}">View</a>
                               <a style="font-size: 16px;" class="mb-2 text-color-light font-semibold">Delete</a>
                           </div>
                       </div>
                   </div>
               </td> -->
                </tr>
                @empty
                <tr>
                    <td>No Record Available</td>
                </tr>
                @endforelse                             
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6">
                        <div class="float-right">
                            {{ $jobs->appends(['keyword' => $keyword, 'type' => $type ])->render() }}
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>                    
    </div>
</div> 
@stop


@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
        console.log("dsad")
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
    @endif
    $("#button_search").on("click",function(e){
      $("#search_filter").submit();
    });
    $('#type').on('change', function(){
     $("#search_filter").submit();
    });
    $('#progress').on('change', function(){
        $("#search_filter").submit();
    });
});
</script>
@stop