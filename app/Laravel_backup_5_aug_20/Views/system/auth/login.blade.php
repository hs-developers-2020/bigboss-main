@extends('system._layouts.auth')

@section('content')

	<div class="background bg-dark">

		  <video loop muted autoplay style="background-color: rgba(0,0,0,.5) !important;">
    <source src="http://booking.bigbossgroup.com.au/frontend/Video/main4.mp4" type="video/mp4" >
  </video>


		<!-- <img src="{{ asset('system/img/login-bg.jpg') }}" alt="" class="full-opacity"> -->
	</div>
	<div class="wrapper">
		<!-- main container starts -->
		<div class="main-container">
  
			<!-- Begin page content -->
			<div class="container">
				<div class="card rounded-2 border-0 mb-3 pb-4 z3 signin-block">
					<div class="card-body">
						<div class="text-center">
						 	<img src="{{ asset('frontend/Logos/MainLogo.png') }}" style="width: 35%;" class="img-fluid" style="">
						</div>
						<p class="text-xsmall text-color-blue font-semibold text-center text-uppercase">Log-in to your account</p>
						@include('system._components.notifications')
						<div class="container ">
						 	<form action="" method="POST">
	              				{!!csrf_field()!!}
								<div class="form-group text-left float-label">
									<input id="username" name="username" type="text" autocomplete="off" class="form-control" value="{{old('username')}}">
									<label>Email address</label>
								</div>
								<div class="form-group text-left float-label">
									<input id="password" name="password" type="password" class="form-control">
									<label>Password</label>
								</div>
						{{-- 		<div class="text-center">
									<label class="text-color-blue text-xsmall mb-3 font-semibold pointer">Forgot your password? </label>
								</div> --}}
								<div>
									 <button data-dismiss="modal" type="submit" class="btn btn-success btn-xl text-uppercase w-100">Sign in</button>
									<br>
										<div class="text-center mt-3">
										<a class="text-xsmall font-semibold text-uppercase no-decoration" href="{{ route('frontend.main') }}">Go Back to Main Page</a>
									</div>

									<div class="row">
		       			<div class="col-lg-12 col-12 text-left mt-2 mb-1 text-center">		
		  					<a href="#" data-toggle="modal" data-target="#addNotes" class="text-color-blue pointer">Forgot Password ?</a>
						</div>
					</div>


								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<!-- main container ends -->

		<form method="POST" action="{{ route('frontend.forgot') }}">
	{{ csrf_field() }}
	<div class="modal" role="dialog" id="addNotes">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title">Reset Password</h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		              <span aria-hidden="true">&times;</span>
		            </button>
		        </div>
			    <div class="modal-body">
			        <div class="text-color-gray mb-2 {{$errors->first('reset_email') ? 'has-error' : NULL}}">
				        <i class="fas fa-angle-double-right font-small text-color-blue"></i>
				        <span class="ml-2 text-xxsmall font-semibold text-uppercase">Reset your password
				        	<span class="text-color-red ml-1">*</span>
				        </span><br><br>
			        	<span>Enter your username or email address and we will send you a link to reset your password.</span>
			        	@include('frontend._components.notifications')
			        	<input type="text" name="reset_email" id="reset_email"  class="form-control mt-3">
				        @if($errors->first('reset_email'))
				        	<span class="help-block" style="color: red;font-size: 12px;">*{{$errors->first('reset_email')}}</span>
				        @endif
			        </div>
			    </div>
	          	<div class="modal-footer">
	          		<button type="submit" class="btn btn-primary">Reset Password</button>
	            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	          	</div>
	        </div>
	    </div>
	</div>
</form>

@stop

@section('page-styles')
<style type="text/css">
	@media only screen and (min-width: 1000px) {
	body{
		overflow: hidden;

	}
}
</style>
@stop