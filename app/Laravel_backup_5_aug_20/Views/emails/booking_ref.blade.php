    <!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Bigboss</title>
</head>
<body style="padding-top: 0 !important;padding-bottom: 0 !important;padding-top: 0 !important;padding-bottom: 0 !important;margin:0 !important;width: 100% !important;-webkit-text-size-adjust: 100% !important;-ms-text-size-adjust: 100% !important;-webkit-font-smoothing: antialiased !important;font-family:Helvetica;color:#333333;padding:0 10px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style='font-family:Helvetica, Arial,serif;'>
    <tr>
      <td>
        <table width="960" border="0" cellspacing="0" cellpadding="0" align="center" style="height:auto !important;">
          <tr>
            <td>
              <div style="border: 0px; padding-top: 0px; position: relative;">
                <div style='display:inline-block;'>
                  <div>
                  <p style="font-size:15px;padding:10px;">Greetings <?php echo $other_details_booking['name'];?>,</p>
                  </div>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div>
                <p style="font-size:16px;padding: 5px 15px 5px 10px;">Thank you for your booking. Your local Big Boss  expert will be in touch with you shortly.</p>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div style="border: 0px; padding-top: 0px; position: relative;">
                <p style="font-size:16px;margin: 0 10px;;padding:10px 15px 15px 0;font-weight:bold;">This is your booking number 
                  <a href="https://booking.bigbossgroup.com.au/">
                    <span style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;">{{$booking_id}}</span> 
                  </a>
               <!--  <span style="color:#f80a37;">< (please link to booking></span> --></p>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div style="border: 5px solid #ccc;">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                  <tbody>
                    <tr>
                      <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                          <tbody>
                            <tr>
                              <td valign="top" style="width:45%;float:left !important;display:block !important;font-size:13px !important;line-height:17px !important;width:50%">
                                <div style="padding:20px;">
                                  <div>
                                    <img src="https://booking.bigbossgroup.com.au/payment.png" /> QUOTE
                                  </div>
                                  <div style="font-size:14px;line-height:20px;margin-top:15px;">
                                    CUSTOMER NAME
                                  </div>
                                  <div style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;">
                                    <?php echo $other_details_booking['name'];?>
                                  </div>
                                  <div style="font-size:14px;line-height:20px;margin-top:15px;">
                                    CONTACT NUMBER
                                  </div>
                                  <div style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;">
                                    <?php echo $other_details_booking['contact'];?>
                                  </div>
                                  <div style="font-size:14px;line-height:20px;margin-top:15px;">
                                    EMAIL ADDRESS
                                  </div>
                                  <div style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;">
                                    <a href="mailto:<?php echo $other_details_booking['email'];?>" style="color:#065288;"><?php echo $other_details_booking['email'];?></a>
                                  </div>
                                  <div style="font-size:14px;line-height:20px;margin-top:15px;">
                                    ADDRESS
                                  </div>
                                  <div style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;">
                                   <?php echo $other_details_booking['location'];?>
                                  </div>
                                  <div style="font-size:14px;line-height:20px;margin-top:15px;">
                                    POST
                                  </div>
                                  <div style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;">
                                    <?php echo $other_details_booking['postcode'];?>
                                  </div>
                                  <div style="font-size:14px;line-height:20px;margin-top:15px;">
                                    PAYMENT METHOD
                                  </div>
                                  <div style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;">
                                    <?php echo $other_details_booking['payment'];?>
                                  </div>
                                </div>
                              </td>
                              <td valign="top" style="float:left !important;display:block !important;font-size:13px !important;line-height:17px !important;width:50%">
                                <div style="background:url('images/booking.jpg');background-repeat:no-repeat;min-height:520px;color:#fff;padding:10px;font-size: 12px;background:#2967a6;">
                                  <div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="15">
                                      <tr>
                                        <td colspan="3" align="right">
                                          BOOKING ID {{$booking_id}}
                                        
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="3" align="left">
                                          <img src="https://booking.bigbossgroup.com.au/user.png" />  <strong>PAYMENT DETAILS</strong>
                                        </td>
                                        
                                      </tr>
                                      <tr>
                                        <td align="left">
                                          SELECT TYPE
                                        </td>
                                        <td align="left">
                                          PROPERTY TYPE
                                        </td>
                                        <td align="left">
                                          PRICE
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left">
                                          <?php echo $other_details_booking['service_type'];?>
                                        </td>
                                        <td align="left">
                                          <?php echo $other_details_booking['property_type'];?>
                                        </td>
                                        <td align="left">
                                          $ <?php echo $other_details_booking['service_type_amount'];?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left" style="border-bottom:1px solid white;">
                                          SERVICE PACKAGE
                                        </td>
                                        <td align="left" style="border-bottom:1px solid white;">
                                          SERVICE PRICE
                                        </td>
                                        <td align="left" style="border-bottom:1px solid white;">
                                          &nbsp;
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left">
                                         <?php echo $other_details_booking['services'];?>
                                        </td>
                                        <td align="left">
                                          $ <?php echo $other_details_booking['amount'];?>
                                        </td>
                                        <td align="left">
                                          $ <?php echo $other_details_booking['amount'];?>
                                        </td>
                                      </tr>
                                       
                                      <tr>
                                        <td colspan="3" style="border-bottom:2px solid white;">
                                          
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left">
                                          BOOKING TOTAL
                                        </td>
                                        <td align="right" colspan="2">
                                          $<?php echo $other_details_booking['amount'] + $other_details_booking['service_type_amount'];?>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div style="margin: 0 10px;width:98%;border:0;padding:10px 15px 0 10px;text-align:left;font-size:15px;">
                <strong>Note:</strong> If you would like to track the status of your booking kindly click this link <span style="color:#065288;font-size:14px;line-height:25px;margin-bottom:10px;"> <a href="https://booking.bigbossgroup.com.au" style="color:#065288;">https://booking.bigbossgroup.com.au/{{$booking_id}}</a></span>
              </div>
                                <div>
                  <p style="margin:0 10px">*** This is an automatically generated email, please do not reply. ***</p>
                </div>
                <div>
                  <div style="margin:20px 10px;font-size:15px;"><strong>Kind regards,</strong></div>
                  <div style="margin:0px 10px;font-size:15px;">Big Boss Group</div>
                  <div style="margin:0px 10px;font-size:15px;">Call 1800 131 599</div>
                  <div style="margin:0px 10px;font-size:15px;">Email: <span><a href="mailto:info@bigbossgroup.com.au/" style="color:#065288;">info@bigbossgroup.com.au/</a></span></div>
                  <div style="margin:0px 10px;font-size:15px;">Website:  <span><a href="www.bigbossgroup.com.au" style="color:#065288;">www.bigbossgroup.com.au</a></span></div>
                </div>
            </td>
        </table>
      </td>
    </tr>
    <tr>
      <td>
         <div style="margin:30px 0; position: relative;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0"  align="center">
            <tr>
            <td valign='top' align='center'>
              <p style='color:#A8B0B6; font-size:13px;line-height: 16px;'><a target='_blank' href='[UNSUBSCRIBE]' style='color:#A8B0B6;font-weight:bold;'>Click Here</a> to unsubscribe from this newsletter.
              </p>
            </td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
  </table>
</body>
</html>