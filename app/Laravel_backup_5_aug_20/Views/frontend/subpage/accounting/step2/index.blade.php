@extends('frontend._layout.main')
@section('content')
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@include('frontend._components.topnav-main')
  {{ Session::put('percent',10) }}
<form action="" method="post">
  {{ csrf_field() }}
  <div class="container" style="padding-top: 133px;padding-bottom:7.5%;">
   
      <div class="row">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-12 text-left mt-4">
              
            </div>      
       
            <div class="col-lg-12 text-left">
               <p class="text-color-white spacing-1 mt-3 font-semibold">
                <span  class="p-2">Enter your location</span></p>
                <p class="text-color-white font-semibold spacing-1">
                <span class="p-2 text-xsmall">Postcode or Suburb <br><i class="fas fa-map-marker-alt text-color-red mr-2"></i>{{ Session::get('accounting.postcode') }} 
                <span id="states" class="text-none"></span></span>
              </p>
            </div>
          
            <div class="col-lg-12 d-flex flex-row mt-2" id="continue" style="padding-bottom: 10px;">
              <div class="input-group">       
                <input type="text" id="map-address" class="form-control p-3 text-left card-shadow map-input text-transparent" name="location" value="{{ old('location') }}">
              </div>
              <input type="hidden" name="postcode" id="postcode">
              <input type="hidden" name="sub" id="sub">
              <input type="hidden" name="street_address" id="street_address">
              <input type="hidden" name="city" id="city">
              <input type="hidden" name="state" id="state">
              <input type="hidden" name="country" id="country">
              <input type="hidden" name="geo_long" id="geo_long" value="{{ old('geo_long') }}">
              <input type="hidden" name="geo_lat" id="geo_lat" value="{{ old('geo_lat') }}">
  {{--             <button type="submit" id="next" class="next btn font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white" href="#"style="color: white !important;" ><i class="fas fa-arrow-right"></i></button> --}}
            </div>
            <div class="col-lg-12">
               <p class="text-color-white spacing-1 font-semibold text-xsmall text-left">
            </div>
        <div class="col-lg-12 mt-2 text-lg-left text-md-center text-sm-center text-xs-center">
          <button type="submit" id="next" class="card-shadow mb-sm-5 mb-md-5 mb-5  next btn font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-5 pr-5 pb-3" href="#"style="color: white !important;" >continue</button>
        </div>
          </div>  
        </div>
        <div class="col-lg-6">
          
            <div id="map" class="card-shadow mt-3"></div>
        </div>
      </div>
    
    </div>
  </div>
</form> 
@include('frontend._components.footer-sub')

@stop

@section('page-scripts')
<script src="{{asset('assets/lib/locationpicker/locationpicker.jquery.js')}}"></script>
<script src="https://maps.google.com/maps/api/js?sensor=true&v=3&libraries=places&key=AIzaSyDVyvrwSzBbJ1chFME01RAi8wOwqmmz0Ww"></script>
<script>
  @if ($errors->first('location') ? 'has-error' : NULL) {
  Swal.fire({
    type: 'error',
    title: 'Oops...',
    
    text: 'Invalid or No Selected Postcode, Please Select One to Continue',

    // footer: '<a href>Why do I have this issue?</a>'
    })
    e.preventDefault()
  }
  @endif

   $('#map-address').on('click',function(e){
    $('.map-input').removeClass('text-transparent')
    })
   $('#map').on('click',function(e){
    $('.map-input').removeClass('text-transparent')
    })

 $(function(){

  $('#map-address').on('click',function(){
    $(this).val('');
  })
         // var x = $('#map-address').val()
        $('#states').text( $("#map-address").val())
        function updateControls(addressComponents) {
          $('#street_address').val(addressComponents.addressLine1);
          $('#city').val(addressComponents.city);
          $('#state').val(addressComponents.stateOrProvince);
          $('#states').text(addressComponents.addressLine1+" "+addressComponents.country);
          $('#sub').val(addressComponents.addressLine1+" "+addressComponents.country);
          $('#country').val(addressComponents.country);
            }

        $('#map').locationpicker({
            location: {
                latitude: {{ Session::get('accounting.latitude2') }} ,
                longitude: {{ Session::get('accounting.longitude1') }}
            },
            zoom: 16,
            mapTypeId: 'terrain',
            radius: 150,
            inputBinding : {
              locationNameInput: $('#map-address'),
              latitudeInput: $('#geo_lat'),
              longitudeInput: $('#geo_long'),
            },
            enableAutocomplete: true,
            autocompleteOptions: {
                // types: ['(cities)'],
                componentRestrictions: {country: 'au'}
            },
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                updateControls(addressComponents);
            },
            oninitialized: function(component) {
                var addressComponents = $(component).locationpicker('map').location.addressComponents;
                updateControls(addressComponents);
            }
        });
    });




</script>


@stop
@section('page-styles')
<style type="text/css">

  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
  }
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
  }
  .styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
  }
  a:hover{
    color: white !important;
  }
  p{
    font-size: 17px;
    font-weight: 600;
  }
  #map {
    height: 400px !important;
    width: 100% !important; 
  }
  body {
    background-image: url(" {{ asset('frontend/background/Accounting.jpg') }}") !important;
    overflow-x: hidden !important;
    background-repeat:no-repeat !important;
    background-size: cover !important;
  } 
  @media only screen and (max-width: 767px) {
    #next{
      width: 100%;
    }
  }

  @media only screen and (max-width: 1500px) {

    #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 50%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
    }
}

@media only screen and (min-width: 1500px) {

  #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
  }
 
  .footer-bottom{
    position: absolute;
    bottom:0;


  }
}

  <style>
/*   #map {
     width: 100%;
     height: 400px;
     background-color: grey;
<<<<<<< HEAD
  }*/

</style>
@stop
