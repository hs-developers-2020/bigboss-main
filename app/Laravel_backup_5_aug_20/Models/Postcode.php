<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    protected $fillable = ['postcode','place_name'];
}
