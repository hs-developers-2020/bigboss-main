<?php

use App\Laravel\Models\Service;
use Illuminate\Database\Seeder;

class SkipServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::truncate();
        
        Service::create(['name' => "None" , 'price' => "0" , 'group' => "cleaning",'service_type' => "home,office"]);

    }
}
