<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\CleaningService;
use App\Laravel\Models\Service;
use App\Laravel\Models\ServicePackage;
use App\Laravel\Models\OtherService;
use App\Laravel\Models\Postcode;
use App\Laravel\Models\User;
use App\Laravel\Models\Finance;
use App\Laravel\Models\AccountingService;
use App\Laravel\Requests\Frontend\BookingRequest;
use App\Laravel\Requests\Frontend\AccountingRequest;
use App\Mail\BookingNumber;

use App\Http\Controllers\Controller;

use App\Laravel\Services\ResponseManager;
use Illuminate\Http\Request;

use App\Laravel\Events\SendBooking;

use Helper, Carbon, Session, Str, DB,Input,Event,PDF,Curl,Signature,Log,Auth,Mail;


class SubpageAccountingController extends Controller
{
	protected $data;
	function __construct($foo = null)
	{
		$this->data = [];
	}
	public function accounting(Request $request){
		
		$request->session()->forget('booking_info');
		$request->session()->forget('current_step');
		$request->session()->forget('accounting');
		$request->session()->forget('percent');
		$this->data['message_alert'] =  '';

		return view ("frontend.subpage.accounting.step1.index",$this->data);
	}

	public function search(Request $request)
	{
		$postcode = $request->get('term');

		$result = Postcode::where('place_name','LIKE','%'.$postcode."%")
		->orWhere('postcode','LIKE','%'.$postcode."%")	
		->get();

		return $x = json_decode($result,true);

	}


	public function accounting_index(Request $request)
	{
		$current_progress = $request->session()->get('current_progress');

		switch ($current_progress) {
			case '1':
				return view("frontend.subpage.accounting.step1.index",$this->data);
				break;
			case '2':
				return view("frontend.subpage.accounting.step{$current_progress}.index",$this->data);
				break;
			case '3':
				$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your Big Boss Accounting Experts",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';
				$this->data['accouting_service'] = Service::where('group',"accounting")->get();
				return view("frontend.subpage.accounting.step{$current_progress}.index",$this->data);
				break;
			case '4':

				$this->data['package'] = Service::whereIn('id', session()->get('accounting.services')['svc'])->get();
				return view("frontend.subpage.accounting.step{$current_progress}.index",$this->data);
				break;
			case '5':
				return view("frontend.subpage.accounting.step{$current_progress}.index",$this->data);
				break;
			case '6':
				$this->data['package'] = Service::whereIn('id', session()->get('accounting.services')['svc'])->get();
				return view("frontend.subpage.accounting.step{$current_progress}.index",$this->data);
				break;
			case '7':
				return view("frontend.subpage.accounting.step{$current_progress}.index",$this->data);
				break;
			default:
				return view("frontend.subpage.accounting.step1.index",$this->data);
				break;
		}
		return view('frontend.subpage.accounting.step1.index',$this->data);
	}

	public function accounting_store(AccountingRequest $request){
		$svc_total = 0;
		$current_progress = $request->session()->get('current_progress');
		switch ($current_progress) {
			case '1':
				$validate = Postcode::where('postcode',$request->get('postcode'))->count();
				if($validate > 0)
				{
					$request->session()->put('accounting.postcode',$request->get('postcode'));
					$request->session()->put('accounting.longitude1',$request->get('longitude'));
					$request->session()->put('accounting.latitude2',$request->get('latitude'));
				}
				else
				{
					session()->flash('notification-status','error');
					session()->flash('notification-msg','no post code found Please try again.');
					return redirect()->back();
				}
				if ($request->get('postcode')) {
					session()->put('current_progress',$current_progress + 1) ;
				}
				break;
			case '2':
				$request->session()->put('accounting.location',$request->location);
				$request->session()->put('accounting.longitude', $request->get('geo_long'));
				$request->session()->put('accounting.latitude', $request->get('geo_lat'));
				$request->session()->put('accounting.state', $request->get('state'));
				$request->session()->put('accounting.sub', $request->get('sub'));
				
				if ($request->get('location')) {
					session()->put('current_progress',$current_progress + 1) ;
					$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your Big Boss Accounting Experts",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';
				}
				break;
			case '3':
				$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your Big Boss Accounting Experts",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';

				if ($request->get('svc')) {
					$this->data['package'] = Service::whereIn('id' , $request->only('svc')['svc'])->get();
					foreach ($this->data['package'] as $key => $value) {
						$svc_name[] = $value->name;
						$svc_price[] = $value->price;
						$svc_total += $value->price;
					}

					$request->session()->put('accounting.services',$request->only('svc'));
					$request->session()->put('accounting.svc_name',$svc_name);
					$request->session()->put('accounting.svc_price',$svc_price);
					$request->session()->put('accounting.svc_total',$svc_total);
					
					session()->put('accounting.svc',$request->only('svc'));
					session()->put('current_progress',$current_progress + 1) ;
				}
				break;
			case '4':			
					$request->session()->put('booking_info.booking_id',uniqid());
					$request->session()->put('accounting.fname', $request->get('fname'));
					$request->session()->put('accounting.lname', $request->get('lname'));
					$request->session()->put('accounting.email', $request->get('email'));
					$request->session()->put('accounting.contact_number', $request->get('contact_number'));
					$request->session()->put('clean.country_code', $request->get('country_code'));
					session()->put('current_progress',$current_progress + 1) ;
				break;
			case '5':
				if ($request->get('booking_date')) {
					$request->session()->put('accounting.booking_date', $request->get('booking_date'));
					$request->session()->put('accounting.time_start', Helper::time_only_24hour($request->get('time_start')));
					$request->session()->put('accounting.time_end',  Helper::time_only_24hour($request->get('time_end')));
					session()->put('current_progress',$current_progress + 1) ;
				}
				break;
			case '6':

				if($request->session()->has('accounting.postcode')){
				   $services =  new cleaningService;
				   $services->postcode = $request->session()->get('accounting.postcode');
				   $services->location = $request->session()->get('accounting.location');
				   $services->service_id = implode(', ',$request->session()->get('accounting.svc')['svc']);
				   $services->service_type =  implode(', ',$request->session()->get('accounting.svc_name'));
				   $services->name = $request->session()->get('accounting.fname');
				   $services->lastname = $request->session()->get('accounting.lname');
				   $services->email = $request->session()->get('accounting.email');
				   $services->contact = $request->session()->get('accounting.contact_number');
				   $services->booking_id = $request->session()->get('booking_info.booking_id');
				   $services->booking_date = Carbon::parse($request->session()->get('accounting.booking_date'));
				   $services->time_start =  $request->session()->get('accounting.time_start');
				   $services->time_end =  $request->session()->get('accounting.time_end');
				   $services->booking_date_secondary =   $request->session()->get('accounting.booking_date_secondary');
				   $services->secondary_time_end =    $request->session()->get('accounting.secondary_time_end');
				   $services->secondary_time_start =    $request->session()->get('accounting.secondary_time_start');
				   $services->payment =  $request->session()->get('accounting.payment');
				   $services->longitude =  $request->session()->get('accounting.longitude');
				   $services->latitude =  $request->session()->get('accounting.latitude');
				   $services->region =  $request->session()->get('accounting.state');
				   $services->amount =   $request->session()->get('accounting.svc_total');
				   $services->booking_total =   $request->session()->get('accounting.svc_total');
				   $services->status =   "pending";
				   $services->type =   "accounting";
				   $emailAllHierarch =['Info@bigbossgroup.com.au'];
				   if ($services->save()) {
						$insert[] = [
						                'email' => $services->email,
						                'firstname' => $services->name,
						                'lastname' => $services->lname,
						                'name' =>  $services->name . " " . $services->lname,
						                'booking_id' => $services->booking_id,
						                "emailAllHierarch"=>$emailAllHierarch,
						                "other_details_booking"=>
						                [
						                	'name' =>  $services->name . " " . $services->lname,
						                	'booking_id' => $services->booking_id,
						                	'email' => $services->email,
						                	'postcode' => $services->postcode,
							                'location' => $services->location,
							                'service_type' => $services->service_type,
							                'service_type_amount' => $services->service_type_amount,
							                'services' => $services->services,
							                'property_type' => $services->property_type,
							                'contact' => $services->contact,
							                'booking_date' => $services->booking_date,
											'time_start' => $services->time_start,
											'time_end' => $services->time_end,
											'region' => $services->region,
											'amount' => $services->amount,
											'payment' => $services->payment
										]

						            ];	
						$notification_data = new SendBooking($insert);
					    Event::fire('send-booking', $notification_data);
						

						$request->session()->forget('current_progress');
						$request->session()->forget('accounting');
						$request->session()->forget('percent');
					}
					Helper::send_sms();
					$this->data['message_alert'] =  '<script>
						Swal.fire({
						position: "center",
						type: "success",
						title: "Book Successfully",
						showConfirmButton: false,
						timer: 3000
						})
						</script>
						';
						// $request->session()->put('current_step',9);
					return view('frontend.page.booking-submitted.accounting-services',$this->data);
				}else{
					return redirect()->route('frontend.accounting.index');
				}
				break;
			default:
				# code...
				break;
		}

		return redirect()->route("frontend.accounting.index");
	}

	public function revert (Request $request){
		
		$current_progress = $request->session()->get('current_progress');
		if ($current_progress > 1) {
			session()->put('current_progress',$current_progress - 1);
			
		}else{
			session()->put('current_progress', 1);
		}

		return redirect()->route("frontend.accounting.index",$this->data);
	}



	public function last_cancel(Request $request)
	{
        if(!$request->session()->has('accounting.postcode'))
		{	
			return redirect()->route('frontend.accounting.index');
		}
		
		$services =  new cleaningService;
 		$services->postcode = $request->session()->get('accounting.postcode');
		$services->location = $request->session()->get('accounting.location');
		$services->service_type =  implode(', ',$request->session()->get('accounting.svc_name'));
		$services->service_id = implode(', ',$request->session()->get('accounting.svc')['svc']);
		$services->name = $request->session()->get('accounting.fname');
		$services->lastname = $request->session()->get('accounting.lname');
		$services->email = $request->session()->get('accounting.email');
		$services->contact = $request->session()->get('accounting.contact_number');
		$services->booking_id = $request->session()->get('booking_info.booking_id');
		$services->booking_date = Carbon::parse($request->session()->get('clean.booking_date'));
		$services->time_start =  $request->session()->get('accounting.time_start');
		$services->time_end =  $request->session()->get('accounting.time_end');
		$services->booking_date_secondary =   $request->session()->get('accounting.booking_date_secondary');
		$services->secondary_time_end =    $request->session()->get('accounting.secondary_time_end');
		$services->secondary_time_start =    $request->session()->get('accounting.secondary_time_start');
		$services->payment =  $request->session()->get('accounting.payment');
		$services->longitude =  $request->session()->get('accounting.longitude');
		$services->latitude =  $request->session()->get('accounting.latitude');
		$services->region =  $request->session()->get('accounting.state');
		$services->amount =   $request->session()->get('accounting.svc_total');
		$services->booking_total =   $request->session()->get('accounting.svc_total');
		$services->status =   "cancelled";
		$services->type =   "accounting";
		$services->save();

		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "Thank you for leaving your details. Our local Big Boss Expert will call you in a while.",
			showConfirmButton: false,
			timer: 6000
			})
			</script>
			<style>
			.swal2-title {
			font-size:20px !important;
		}
			</style>';
		$request->session()->forget('current_progress');
		$request->session()->forget('accounting');
		$request->session()->forget('percent');
		$this->data["message"] = "Your local Big Boss Accounting expert will be in touch with you shortly.";
		return view('frontend.page.cancel-booking.index',$this->data);
	}


	public function cancel(AccountingRequest $request)
	{

		if(!$request->session()->has('accounting.postcode'))
		{	
			return redirect()->route('frontend.accounting.index');
		}
           $request->session()->put('accounting.payment', $request->payment);
	
          
		$services =  new cleaningService;
 		$services->postcode = $request->session()->get('accounting.postcode');
		$services->location = $request->session()->get('accounting.location');
		$services->service_type =  implode(', ',$request->session()->get('accounting.svc_name'));
		$services->service_id = implode(', ',$request->session()->get('accounting.svc')['svc']);
		$services->name =$request->get('fname');
		$services->lastname = $request->get('lname');
		$services->email = $request->get('email');
		$services->contact = $request->get('contact_number');
		$services->booking_id = $request->session()->get('booking_info.booking_id');
		$services->booking_date = Carbon::parse($request->session()->get('clean.booking_date'));
		$services->time_start =  $request->session()->get('accounting.time_start');
		$services->time_end =  $request->session()->get('accounting.time_end');
		$services->booking_date_secondary =   $request->session()->get('accounting.booking_date_secondary');
		$services->secondary_time_end =    $request->session()->get('accounting.secondary_time_end');
		$services->secondary_time_start =    $request->session()->get('accounting.secondary_time_start');
		$services->payment =  $request->session()->get('accounting.payment');
		$services->longitude =  $request->session()->get('accounting.longitude');
		$services->latitude =  $request->session()->get('accounting.latitude');
		$services->region =  $request->session()->get('accounting.state');
		$services->amount =   $request->session()->get('accounting.svc_total');
		$services->booking_total =   $request->session()->get('accounting.svc_total');
		$services->status =   "cancelled";
		$services->type =   "accounting";
		$services->save();

			
		
		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "Thank you for leaving your details. Our local Big Boss Expert will call you in a while.",
			showConfirmButton: false,
			timer: 6000
			})
			</script>
			<style>
			.swal2-title {
			font-size:20px !important;
		}
			</style>';
		$request->session()->forget('current_progress');
		$request->session()->forget('accounting');
		$request->session()->forget('percent');

		$this->data["message"] = "Your local Big Boss Accounting expert will be in touch with you shortly.";
		return view('frontend.page.cancel-booking.index',$this->data);
		
		
	}



	public function destroy($value='')
	{
		$request->session()->flush();
		return redirect()->route('frontend.accounting.index');
	}


}



