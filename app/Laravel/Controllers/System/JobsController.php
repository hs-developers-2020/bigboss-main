<?php


namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\CleaningService;
use App\Laravel\Models\Service;
use App\Laravel\Models\SubService;
use App\Laravel\Models\UserTerritory;
use App\Laravel\Models\Finance;
use App\Laravel\Models\WorkAvailability;
use App\Laravel\Models\OtherService;
use App\Laravel\Models\Invoice;
use App\Laravel\Models\User;
use App\Laravel\Models\Notes;
/**AssignedStaffRequest
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ServiceRequest;
use App\Laravel\Requests\System\UpdateBookingRequest;
use App\Laravel\Requests\System\AssignedStaffRequest;
use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;
use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Events\SendBooking;

use Helper, Carbon, Session, Str, Auth, ImageUploader,Input,PDF,FileUploader,File,DB,AuditRequest,Event;
class JobsController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"All Services",'cleaning' => "Cleaning","information_technology" => "Information Technology"];
		$this->data['sub_type'] = [ ''=>"Choose Storey",'unit' => "Unit",'single' => "Single","double" => "Double"];
		$this->data['type'] = ['' => "Choose Type" ,"regular" => "Regular" ,"vacate" => "Vacate"];
		$this->data['job_type'] = [ ''=>"Choose Type",'minutes' => "Within 10 minutes",'two_hours' => "Within 2 Hours",'one_day' => "Within 1 Day"];
		$this->data['job_progress'] = [ ''=>"Choose Progress",'pending' => "Pending",'ongoing' => "On going",'completed' => "Completed"];
		
		$this->data['rooms'] = ['' => '--Select City--'];
	}

	public function index(){

		$id =Auth::user()->id;
		$total=0;

		//$franchisee = UserTerritory::where('user_id' , $id)->first();
		
  	
  		$day = Carbon::now()->subDay(1)->toDateTimeString();
  		$hour = Carbon::now()->subHours(2)->toDateTimeString();
  		$week =Carbon::now()->subDays(6);
  		$startDate = Carbon::now()->startOfWeek();
  		$endDate = Carbon::now()->endOfWeek();
  		$date_start= Carbon::now()->startOfWeek()->format('m/d/y');
  		

  		if (Auth::user()->type == "franchisee") {

  			$this->data['unassigned']  = CleaningService::where("fit_status" , "no_fitted")->count();
	  		$this->data['two_hours']  = CleaningService::where("franchisee_id" , $id)->where('created_at' , '>' ,$hour)->count();
	  		$this->data['last_day']  = CleaningService::where("franchisee_id" , $id)->where('created_at' , '>' ,$day)->count();
	  		$this->data['jobs'] = CleaningService::where("franchisee_id" ,$id)->orderBy('created_at' , "DESC")->get();
	  		$this->data['pending'] = CleaningService::where('status' , "ongoing")->where("franchisee_id" ,$id)->orderBy('created_at' , "DESC")->count();
	  		$this->data['ongoing'] = CleaningService::where('status' , "completed")->where("franchisee_id" ,$id)->orderBy('created_at' , "DESC")->count();
	  		$this->data['total_job'] = CleaningService::where("franchisee_id" ,$id)->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['unconverted'] = CleaningService::where("franchisee_id" ,$id)->where('status' ,"ongoing")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['converted'] = CleaningService::where("franchisee_id" ,$id)->where('status' ,"completed")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['unserviced'] = CleaningService::where("franchisee_id" ,$id)->where('status' ,"cancelled")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		if ($this->data['total_job']) {
	  			$percent = $this->data['converted'] / $this->data['total_job'] * 100;
	  			$unserviced_percent = $this->data['unserviced'] / $this->data['total_job'] * 100;
	  		}else{
	  			$percent = 0;
	  			$unserviced_percent = 0;
	  		}
  		}elseif (Auth::user()->type == "area_head") {

  			$under = User::select('id')->where('type', 'franchisee')->where('head_id', Auth::user()->id)->pluck('id')->toArray();
  			$user_teritory = UserTerritory::select(DB::raw("CONCAT(latitude,' ',longitude) AS point"))->where('user_id' ,Auth::user()->id)->pluck('point')->toArray();
			$polygon = implode(", ",array_merge($user_teritory,[ $user_teritory[0]]));

			$this->data['jobs'] = CleaningService::withinTeritory($polygon)->orderBy('created_at' , "DESC")->get();
  			
  			$this->data['unassigned']  = CleaningService::withinTeritory($polygon)->where("fit_status" , "no_fitted")->count();
  			$this->data['two_hours']  = CleaningService::withinTeritory($polygon)->where('created_at' , '>' ,$hour)->count();
	  		$this->data['last_day']  = CleaningService::withinTeritory($polygon)->where('created_at' , '>' ,$day)->count();

	  		$this->data['pending'] = CleaningService::withinTeritory($polygon)->where('status' , "pending")->orderBy('created_at' , "DESC")->count();
	  		$this->data['ongoing'] = CleaningService::withinTeritory($polygon)->where('status' , "ongoing")->orderBy('created_at' , "DESC")->count();
	  		$this->data['total_job'] = CleaningService::withinTeritory($polygon)->whereBetween('created_at' , [$startDate ,$endDate])->count();

	  		$this->data['unconverted'] = CleaningService::withinTeritory($polygon)->where('status' ,"pending")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['converted'] = CleaningService::withinTeritory($polygon)->where('status' ,"ongoing")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['completed_leads'] = CleaningService::withinTeritory($polygon)->where('status' ,"completed")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['unserviced'] = CleaningService::withinTeritory($polygon)->where('status' ,"cancelled")->whereBetween('created_at' , [$startDate ,$endDate])->count();

	  		if ($this->data['total_job']) {
	  			$percent = $this->data['converted'] / $this->data['total_job'] * 100;
	  			$unserviced_percent = $this->data['unserviced'] / $this->data['total_job'] * 100;
	  		}else{
	  			$percent = 0;
	  			$unserviced_percent = 0;
	  		}
  		}
  		else{
  			$this->data['unassigned']  = CleaningService::where("fit_status" , "no_fitted")->count();
  			$this->data['two_hours']  = CleaningService::where('created_at' , '>' ,$hour)->count();
	  		$this->data['last_day']  = CleaningService::where('created_at' , '>' ,$day)->count();
	  		$this->data['jobs'] = CleaningService::orderBy('created_at' , "DESC")->get();
	  		$this->data['pending'] = CleaningService::where('status' , "pending")->orderBy('created_at' , "DESC")->count();
	  		$this->data['ongoing'] = CleaningService::where('status' , "ongoing")->orderBy('created_at' , "DESC")->count();

	  		$this->data['total_job'] = CleaningService::whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['unconverted'] = CleaningService::where('status' ,"pending")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['converted'] = CleaningService::where('status' ,"on_going")->whereBetween('created_at' , [$startDate ,$endDate])->count();
	  		$this->data['unserviced'] = CleaningService::where('status' ,"cancelled")->whereBetween('created_at' , [$startDate ,$endDate])->count();

	  		if ($this->data['total_job']) {
	  			$percent = $this->data['converted'] / $this->data['total_job'] * 100;
	  			$unserviced_percent = $this->data['unserviced'] / $this->data['total_job'] * 100;
	  		}else{
	  			$percent = 0;
	  			$unserviced_percent = 0;
	  		}
  		}
  		
  		$this->data['date_start'] = $date_start;
  		//$this->data['total'] = $total;
  		$this->data['percent'] = $percent;
  		$this->data['unserviced_percent'] = $unserviced_percent;
		return view ('system.jobs.index',$this->data);
	}

	public function show(PageRequest $request ){
		$id =Auth::user()->id;
		$minutes = Carbon::now()->subMinutes(10)->toDateTimeString();
		$hour = Carbon::now()->subHours(2)->toDateTimeString();
		$day = Carbon::now()->subDay(1)->toDateTimeString();
		$this->data['keyword'] = $request->get('keyword', NULL);
		$this->data['progress'] = $request->get('progress',null);
		$this->data['type'] = $request->get('type',null);


		switch ($this->data['type']) {
			case 'minutes':
				$value_type = $minutes;
				break;
			case 'two_hours':
				$value_type = $hour;
				break;
			case 'one_day':
				$value_type = $day;
				break;
			
			default:
				$value_type = "";
				break;
		}

		if (Auth::user()->type == "franchisee") {
			$this->data['jobs'] = CleaningService::keyword($this->data['keyword'])->where('created_at' , '>' ,$value_type)->progress($this->data['progress'])->where("franchisee_id" ,$id)->orderBy('created_at' , "DESC")->paginate(10);
		}elseif (Auth::user()->type == "area_head") {
			$user_teritory = UserTerritory::select(DB::raw("CONCAT(latitude,' ',longitude) AS point"))->where('user_id' ,Auth::user()->id)->pluck('point')->toArray();
			$polygon = implode(", ",array_merge($user_teritory,[ $user_teritory[0]]));
			//$under = User::select('id')->where('type', 'franchisee')->where('head_id', Auth::user()->id)->pluck('id')->toArray();
			$this->data['jobs'] = CleaningService::withinTeritory($polygon)->keyword($this->data['keyword'])->where('created_at' , '>' ,$value_type)->progress($this->data['progress'])
					->orderBy('created_at' , "DESC")->paginate(10);
		}
		else{
			$this->data['jobs'] = CleaningService::keyword($this->data['keyword'])->where('created_at' , '>' ,$value_type)->progress($this->data['progress'])->orderBy('created_at' , "DESC")->paginate(10);
		}
		$this->data['franchisee_details']=[];
	  	foreach($this->data['jobs'] as $val){
	  		if(intval($val->franchisee_id)){
	  			$user_details = User::select('*')->where('id',$val->franchisee_id)->get()->first();
	  			if(count($user_details)){
	  				$val->franchisee_name=$user_details->firstname." ".$user_details->lastname;
	  			}else{
	  				$val->franchisee_name="Not Found";
	  			}
	  		}else{
	  			$val->franchisee_name="Not Found";
	  		}
	  		$this->data['franchisee_details'][$val->franchisee_id]=$val->franchisee_name;
	  	}
	  	//$this->data['jobs']=$joblist;
		
		return view ('system.jobs.show',$this->data);
	}

	public function details($id = NULL){
		$total = 0;
	 	$jobs = CleaningService::find($id);
	 	$this->data['others'] = OtherService::where('booking_id',$jobs->booking_id)->get();
	 	$this->data['franchisee_lists'] = User::get();
	 	//$this->data['franchisee_lists'] = User::where('user.type', 'franchisee')->get();

	 	$service_standard = explode(",",$jobs->standard_services); 

	 	
	 	foreach ($service_standard as $key) {
	 		$service = Service::find($key);

	 		if($service) {
	 			$total += $service->price;
	 		}
	 	}

	
	 	if ($jobs->number_of_rooms) {
	 		$sub_price = $jobs->subservice->price;
	 	}else{
	 		$sub_price = 0;
	 	}

	 	//dd();

	 	$date_today = Carbon::now()->format('D');

	 	//dd($date_today);
	 	
	 	$region = "";
	 	$this->data['services'] = Service::where('group',$jobs->type)->orderBy('created_at' , "DESC")->get();
		//dd( $jobs->property_type);
	 	if (!$jobs) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.jobs.index');
		}

		if ($jobs->region =="NT") {
	  		$region = "Northern Territory";
	  	}
	  	if ($jobs->region =="NSW") {
	  		$region = "New South Wales";
	  	}
	  	if ($jobs->region =="QLD") {
	  		$region = "Queensland";
	  	}
	  	if ($jobs->region =="SA") {
	  		$region = "South Australia";
	  	}
	  	if ($jobs->region =="TAS") {
	  		$region = "Tasmania";
	  	}
	  	if ($jobs->region =="VIC") {
	  		$region = "Victoria";
	  	}
	  	if ($jobs->region =="WA") {
	  		$region = "Western Australia";
	  	}


	    //$total_booking = $total + $sub_price + $jobs->amount;

	    //dd($total_booking);

		$this->data['jobs'] = $jobs;
		$this->data['region'] = $region;

		if ($jobs->status == "completed" or $jobs->status == "cancelled") {
			return view ('system.jobs.completed',$this->data);
		}else{
			
			return view ('system.jobs.details',$this->data);		
		}
		
	}

	public function save_details(UpdateBookingRequest $request, $id = null){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		$total = 0;
		$price = 0;

		try{
		$job = CleaningService::find($id);
		if ($job) {
			$job->others()->delete();
		}

		$job->unit = $request->get('unit');
		$job->number_of_rooms = $request->get('number_of_rooms');
		$job->room_type = $request->get('room_type');

	 	foreach ($request->get('other_services') as $key => $value) { 
	 		$new_other_service = new OtherService;
			$new_other_service->services = $request->get('other_services')[$key];
			$new_other_service->price = $request->get('other_services_price')[$key];
			
			$new_other_service->booking_id = $job->booking_id;
			
			$price +=$request->get('other_services_price')[$key];
			$new_other_service->save();
	 	}

	 	
		
	 	//$job->standard_service_amount = $total;
	 	if ($job->number_of_rooms) {
	 		$sub_price = $job->subservice->price;
	 	}else{
	 		$sub_price = 0;
	 	}

	 	$job->booking_total = $price + $sub_price + $job->amount + $job->service_type_amount;
		
		

		if($job->save()) {
			$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "UPDATED_BOOKING", 'remarks' => Auth::user()->full_name." was successfully updated this booking id ({$job->booking_id})",'ip' => $ip , 'franchisee_id' => NULL]);	
			Event::fire('log-activity', $log_data);
			session()->flash('notification-status','success');
			Session::flash('alert-type','success');
			session()->flash('notification-msg',"New record has been added.");
			return redirect()->route('system.jobs.show');
		}
		session()->flash('notification-status','failed');
		Session::flash('alert-type','failed');
		session()->flash('notification-msg','Something went wrong.');

		return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		
	}


	public function assigned($id = NULL){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		$jobs = CleaningService::find($id);

		$this->data['user_long'] = $jobs->longitude;
		$this->data['user_lat'] = $jobs->latitude;
		$this->data['postcode_jobs']  = $jobs->postcode;
		//dd($jobs);
		if (!$jobs) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.jobs.index');
		}

		/*********
		$franchisee_ids = UserTerritory::whereIn('user_id', function($query){
										$query->select('id')
												->where('type', 'franchisee')
												->where('head_id', '<>' , NULL)
												->from('user');
									})
									->distance([0,100],$this->data['user_lat'],$this->data['user_long'])
									->pluck('user_id')->toArray();
									************/


			$franchisee_ids_new=User::select('user.id')	->where('user.type', 'franchisee')
												->where('user.head_id', '<>' , NULL)
												->where('user_info.post_code', $jobs->postcode)
												->join('user_info', 'user.id', '=', 'user_info.user_id')->get();
			$franchisee_ids=[];
			if(count($franchisee_ids_new)){
				foreach($franchisee_ids_new as $key=>$val){
					$franchisee_ids[]=$val->id;
				}
			}
		//echo "----------".count($franchisee_ids);dd($franchisee_ids);
		/*********
		$work_avail = WorkAvailability::whereIn('user_id',count($franchisee_ids) > 0 ? $franchisee_ids : [0])
							->where('date' , $jobs->booking_date)->where('work_type',$jobs->property_type)->orderByRaw("FIELD(work_stated,'territory','local','all_areas')")->get();

							*************/

		$work_avail = WorkAvailability::whereIn('user_id',count($franchisee_ids) > 0 ? $franchisee_ids : [0])
							->orderByRaw("FIELD(work_stated,'territory','local','all_areas')")->get();

		$selected_franchisee = 0;

		foreach($work_avail as $index => $franchisee){
			$count_lead = CleaningService::where('franchisee_id',$franchisee->user_id)
										->where('booking_date' , $jobs->booking_date)->where('status' , "pending")->count();
			if($count_lead < $franchisee->max_leads){
				$with_parallel = CleaningService::select("*",DB::raw("TIME_TO_SEC('{$jobs->booking_date} {$jobs->time_start}') AS timetosec"),DB::raw("TIME_TO_SEC(CONCAT(booking_date,' ',time_start)) AS timetosec2"))->where('franchisee_id',$franchisee->user_id)
										->where('booking_date' , $jobs->booking_date)
										->where(function($query) use($jobs){
											return $query->where(function($q) use($jobs){
													return $q->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_start)) <= TIME_TO_SEC('{$jobs->booking_date} {$jobs->time_start}')")
																->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_end)) >= TIME_TO_SEC('{$jobs->booking_date} {$jobs->time_start}')");
											})->orwhere(function($q) use($jobs){
												return $q->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_start)) >= TIME_TO_SEC('{$jobs->booking_date} {$jobs->time_end}')")
															->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_end)) <= TIME_TO_SEC('{$jobs->booking_date} {$jobs->time_end}')");
											});
											
										})->count();
				if($with_parallel > 0){
					goto skip_me;
				}
				$selected_franchisee= $franchisee->user_id;
				goto jump_here;
			}
			skip_me:

			if($index == count($work_avail)-1){
				// $selected_franchisee = $work_avail[0]
				//unservice
			}
		}		

		jump_here:

		if(isset($_GET['franchisee_selected_custom']) && intval($_GET['franchisee_selected_custom'])){
			$selected_franchisee =$_GET['franchisee_selected_custom'];
			$jobs->franchisee_id = $selected_franchisee;
			$jobs->fit_status = 'fitted';
			$jobs->status = 'ongoing';
			if ($jobs->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ASSIGNED_STAFF", 'remarks' => Auth::user()->full_name." was successfully asssigned {$jobs->staff->full_name} to this booking id ({$jobs->booking_id})",'ip' => $ip, 'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				$emailAllHierarch = ['info@bigbossgroup.com.au'];
				$selected_user_details=User::where('user.id', intval($_GET['franchisee_selected_custom']))->get()->toArray();
				$emailAllHierarch [] =$selected_user_details[0]['email'];
				// echo '<pre>';
				// print_r($selected_user_details);
				// print_r($jobs);exit;

				$insert[] = [
						                'email' => $jobs->email,
						                'firstname' => $jobs->name,
						                'lastname' => $jobs->lname,
						                'name' =>  $jobs->name . " " . $jobs->lname,
						                'booking_id' => $jobs->booking_id,
						                "emailAllHierarch"=>$emailAllHierarch,
						                "other_details_booking"=>
						                [
						                	'name' =>  $jobs->name . " " . $jobs->lname,
						                	'booking_id' => $jobs->booking_id,
						                	'email' => $jobs->email,
						                	'postcode' => $jobs->postcode,
							                'location' => $jobs->location,
							                'service_type' => $jobs->service_type,
							                'service_type_amount' => $jobs->service_type_amount,
							                'services' => $jobs->services,
							                'property_type' => $jobs->property_type,
							                'contact' => $jobs->contact,
							                'booking_date' => $jobs->booking_date,
											'time_start' => $jobs->time_start,
											'time_end' => $jobs->time_end,
											'region' => $jobs->region,
											'amount' => $jobs->amount,
											'payment' => $jobs->payment
										]

						            ];	
							$notification_data = new SendBooking($insert);
						    Event::fire('send-booking', $notification_data);


				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Staff was sucessfully assigned to the job.");
				return redirect()->route('system.jobs.details',$id);
			}	

		}else if ($selected_franchisee == 0) {
			$jobs->fit_status = 'no_fitted';
			$jobs->franchisee_id = NULL;
			if ($jobs->save()) {
				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"No Fitted Staff for this job.");
				return redirect()->route('system.jobs.details',$id);
			}	
		}else{
			$jobs->franchisee_id = $selected_franchisee;
			$jobs->fit_status = 'fitted';
			$jobs->status = 'ongoing';
			if ($jobs->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ASSIGNED_STAFF", 'remarks' => Auth::user()->full_name." was successfully asssigned {$jobs->staff->full_name} to this booking id ({$jobs->booking_id})",'ip' => $ip, 'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);
				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Staff was sucessfully assigned to the job.");
				return redirect()->route('system.jobs.details',$id);
			}	
		}
			
		
		/*$this->data['jobs'] = $jobs;

		return view ('system.jobs.assigned',$this->data);*/
	}
	
	/*public function save_assigned(AssignedStaffRequest $request, $id = null){
		
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$job = CleaningService::find($id);
			$job->franchisee_id = $request->get('staff_id');
			$job->status = 'ongoing';

			if (!$job) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.jobs.index');
			}

			if($job->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ASSIGNED_STAFF", 'remarks' => Auth::user()->full_name." was successfully asssigned {$job->staff->full_name} to this booking id ({$job->booking_id})",'ip' => $ip]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Staff was sucessfully assigned to the job.");
				return redirect()->route('system.jobs.details',$id);
			}

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}

	}*/

	public function remove_staff($id = NULL){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		$remove = CleaningService::find($id);
		$staff_name = $remove->staff->full_name;

		$remove->franchisee_id = NULL;
		$remove->status = 'pending';
		if ($remove->save()) {
			$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "REMOVED_STAFF", 'remarks' => Auth::user()->full_name." was successfully removed {$staff_name} to this booking id ({$remove->booking_id})",'ip' => $ip , 'franchisee_id' => NULL]);
			Event::fire('log-activity', $log_data);
			session()->flash('notification-status','success');
			Session()->flash('alert-type','success');
			session()->flash('notification-msg',"Staff revoked  assignment to this job.");
			return redirect()->route('system.jobs.details',$id);
		}		
	}
	public function complete_booking ($id = null){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		$job = CleaningService::find($id);
		
		if(!$job){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.jobs.index');
		}

		$job->status = 'completed';

		if($job->save()) {
			$new_finance = new Finance;
			$new_finance->category = "cleaning";
			$new_finance->group = "cleaning";
			$new_finance->details = $job->service_type ;
			$new_finance->property_type = $job->property_type;
			$new_finance->type = "Income";
			$new_finance->amount = $job->booking_total;
			$new_finance->save();
			
			$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "COMPLETE_BOOKING", 'remarks' => Auth::user()->full_name." was successfully completed booking of ({$job->booking_id})",'ip' => $ip, 'franchisee_id' => NULL]);


			Session()->flash('notification-status',"Success");
			Session()->flash('alert-type','success');
			session()->flash('notification-msg',"Booking was sucessfully completed.");
			return redirect()->route('system.jobs.index');
		}
	}

	public function generate_invoice($id = NULL){
		//dd($id);

		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		$user_id =Auth::user()->id;
		$invoice = Invoice::where('booking_id' , $id)->first();

		$booking_details = CleaningService::where('booking_id' , $id)->first();
		
		if ($invoice) {
			$invoice->invoice_to = $booking_details->full_name;
			$invoice->amount = $booking_details->booking_total;
			$invoice->booking_id = $booking_details->booking_id;
			$invoice->user_id = $user_id;
			$invoice->status = "pending";
			$invoice->franchisee_id = $booking_details->franchisee_id;	
			$invoice->head_id = $booking_details->staff->head_id;	
			$invoice->invoice_number = str_pad($booking_details->id, 4, "0", STR_PAD_LEFT);
			$invoice->save();
			$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "INVOICE_CREATED", 'remarks' => Auth::user()->full_name." Has successfully generated invoice of {$invoice->booking_id}",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

		}else{
			$new_invoice = new Invoice;
			$new_invoice->invoice_to = $booking_details->full_name;
			$new_invoice->amount = $booking_details->booking_total;
			$new_invoice->booking_id = $booking_details->booking_id;
			$new_invoice->user_id = $user_id;
			$new_invoice->status = "pending";
			$new_invoice->franchisee_id = $booking_details->franchisee_id;	
			$new_invoice->head_id = $booking_details->staff->head_id;	
			$new_invoice->invoice_number = str_pad($booking_details->id, 4, "0", STR_PAD_LEFT);
			$new_invoice->save();
			$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "INVOICE_CREATED", 'remarks' => Auth::user()->full_name." has successfully generated invoice of {$new_invoice->booking_id}",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);
		}

		Session()->flash('notification-status',"Success");
		Session()->flash('alert-type','success');
		session()->flash('notification-msg',"Invoice has been successfully generated.");
		return redirect()->route('system.invoice.index');

	}

	public function get_rooms(){
		$id = Input::get('id');

		//$job = CleaningService::find(Input::get('input_type'));

		if(Input::get('input_type') != "regular")
			$regular = SubService::where('type', "vacate")->where('storey', $id)->get();
		else 
			$regular = SubService::where('type', "regular")->where('storey', $id)->get();

		return response()->json($regular);
	}

	public function cancel_quote($id = NULL){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		
		try{

			$cancel_jobs = CleaningService::find($id);


			if (!$cancel_jobs) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.jobs.index');
			}

			$cancel_jobs->status = "cancelled";

			if($cancel_jobs->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "QUOTE_UPDATED", 'remarks' => Auth::user()->full_name." has successfully cancel quotation {$cancel_jobs->booking_id} booking information. ",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Quote has been successfully cancelled.");
				return redirect()->route('system.jobs.show');
			}
				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Something went wrong.');
				return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}

	}

	public function cancelled(PageRequest $request){
		$id =Auth::user()->id;
		$minutes = Carbon::now()->subMinutes(10)->toDateTimeString();
		$hour = Carbon::now()->subHours(2)->toDateTimeString();
		$day = Carbon::now()->subDay(1)->toDateTimeString();
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['progress'] = $request->get('progress',false);
		$this->data['type'] = $request->get('type',false);


		switch ($this->data['type']) {
			case 'minutes':
				$value_type = $minutes;
				break;
			case 'two_hours':
				$value_type = $hour;
				break;
			case 'one_day':
				$value_type = $day;
				break;
			
			default:
				$value_type = "";
				break;
		}

		$user_teritory = UserTerritory::select(DB::raw("CONCAT(latitude,' ',longitude) AS point"))->where('user_id' ,Auth::user()->id)->pluck('point')->toArray();
		if($user_teritory){
			$polygon = implode(", ",array_merge($user_teritory,[$user_teritory[0]]));
		}else{
			$polygon = NULL;
		}

		$this->data['jobs'] = CleaningService::withinTeritory($polygon)->keyword($this->data['keyword'])->where('created_at' , '>' ,$value_type)->where('status', 'cancelled')->orderBy('created_at' , "DESC")->paginate(10);
		
		return view ('system.jobs.cancelled',$this->data);
	}

	public function save_notes(PageRequest $request){
		$notes = new Notes;

		$notes->user_id = Auth::user()->id;
		$notes->job_id = $request->get('job_id');
		$notes->notes = $request->get('notes');

		if($notes->save()){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.jobs.details', [$request->get('job_id')]);
		}

		session()->flash('notification-status',"failed");
		session()->flash('notification-msg',"Something went wrong.");
		return redirect()->route('system.jobs.details', [$request->get('job_id')]);
	}


	public function unserviced(PageRequest $request){

		$this->data['keyword'] = $request->get('keyword',false);

		$user_teritory = UserTerritory::select(DB::raw("CONCAT(latitude,' ',longitude) AS point"))->where('user_id' ,Auth::user()->id)->pluck('point')->toArray();
		if($user_teritory){
			$polygon = implode(", ",array_merge($user_teritory,[$user_teritory[0]]));
		}else{
			$polygon = NULL;
		}


		$this->data['jobs'] = CleaningService::withinTeritory($polygon)->keyword($this->data['keyword'])->where('fit_status', 'no_fitted')->orderBy('created_at' , "DESC")->paginate(10);
		
		return view ('system.jobs.unserviced',$this->data);
	}


	public function reports(PageRequest $request){
		if(isset($_POST['sdate'])){
			
			$franchisee_lists = User::get()->toArray();
			$franchisee_lists_id_wise =[];
			$franchisee_lists_id_wise [0] = " Not Assign Staff ";

			foreach($franchisee_lists as $key=>$val){
				$franchisee_lists_id_wise [$val['id']] = $val['firstname'] ."    ". $val['lastname'];
			}
			$sdate =date('Y-m-d',strtotime($_POST['sdate']));
			$edate =date('Y-m-d',strtotime($_POST['edate']));

			$franchisee_id =$_POST['franchisee_selected_custom'];

			$csv_data =[];
			$filename = 'custom-reports-'.'-'.time().'.csv';
			$cols =array();	

			$cols[] = 'name';
			$cols[] = 'email';
			$cols[] = 'contact';
			$cols[] = 'booking_id';	
			$cols[] = 'booking_date';
			$cols[] = 'postcode';		
			$cols[] = 'location';		
			$cols[] = 'services';
			$cols[] = 'payment';
			$cols[] = 'status';
			$cols[] = 'longitude';
			$cols[] = 'latitude';
			$cols[] = 'amount';
			$cols[] = 'type';
			$cols[] = 'created_at';
			$cols[] = 'Assign Staff';

			$csv_data[] = $cols;

			if(intval($franchisee_id)){
				$this->data['jobs'] = CleaningService::where("franchisee_id" , $franchisee_id)->whereBetween('created_at', [$sdate, $edate])->get();
			}else{
				$this->data['jobs'] = CleaningService::whereBetween('created_at', [$sdate, $edate])->get();
			}
			

			foreach($this->data['jobs'] as $key=>$val){
				//echo '<pre>'; print_r($val);exit;
				$csv_data[] =[$val->name , $val->email , $val->contact , $val->booking_id , $val->booking_date , $val->postcode , $val->location , $val->services , $val->payment , $val->status , $val->longitude , $val->latitude  , $val->amount  , $val->type  , $val->created_at,$franchisee_lists_id_wise [intval($val->franchisee_id )]];
			}
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=\"$filename\"");
			header("Pragma: no-cache");
			header("Expires: 0");

			$handle = fopen('php://output', 'w');

			foreach ($csv_data as $csv_data_val) {
				fputcsv($handle, $csv_data_val);
			}
			fclose($handle);
			exit;


		}
		$this->data['franchisee_lists'] = User::get();
		$this->data['jobs'] = CleaningService::where('fit_status', 'no_fitted')->orderBy('created_at' , "DESC")->paginate(10);
		return view ('system.jobs.reports',$this->data);
	}

}
