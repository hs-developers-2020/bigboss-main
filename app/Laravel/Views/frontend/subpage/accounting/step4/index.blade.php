@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
<style type="text/css">
  .hidden
  {
    display: none;
  }

</style>
{{ Session::put('percent',70) }}

<div class="container" style="padding-top: 125px;">
  <form action="" method="POST">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-12" id="card-margin">
      <div class="panel-header hidden"></div>
        <div class="card card-shadow" style="border-radius: 0px !important;margin-bottom: 20px">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row"  style="margin-left: 0px !important;margin-right: 0px !important;">
              <div class="col-lg-5 p-4" id="customer-details" style="" >
                <div class="row">
                  <div class="col-lg-12 text-center header-booking--details">   
                    <p class="text-color-blue font-semibold spacing-1 text-small text-uppercase">Booking <span class="text-color-red">Customer Details</span></p>
                  </div>
                  <div class="col-lg-12 text-center header-cancel--booking hidden mt-3">   
                    <p class="text-color-gray font-semibold spacing-1 text-xsmall">If you wanted us to call you to discuss the quotation and your need, Please fill up the following information.</p>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <p class="text-color-gray font-semibold text-xsmall text-color-gray ">Enter your Details</p>
                  </div>
                  <div class="col-lg-12 mb-2">
                    <div class="text-color-gray mb-2 {{$errors->first('group') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">First Name<span class="text-danger">*</span></span>
                    </div>
                    <input type="text" class="form-control text-xsmall bg-gray--input text-white" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="fname" id="fname" value="{{ Auth::check() ? Auth::user()->firstname : old('lname') }}">
                    @if($errors->first('fname'))
                      <span class="help-block" style="color:red;">{{$errors->first('fname')}}</span>
                    @endif                       
                  </div>
                  <div class="col-lg-12 mb-2 mt-2">
                    <div class="text-color-gray mb-2 {{$errors->first('lname') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">Last Name<span class="text-danger">*</span></span>
                    </div>
                    <input type="text" class="form-control text-xsmall bg-gray--input text-white" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="lname" id="lname" value="{{ Auth::check() ? Auth::user()->lastname : old('lname') }}">
                    @if($errors->first('lname'))
                      <span class="help-block" style="color:red;">{{$errors->first('lname')}}</span>
                    @endif   
                  </div>
                  <?php
                  /***********
                  <div class="col-lg-12 mt-2">
                    <div class="text-color-gray mb-2 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">Contact Number<span class="text-danger">*</span></span>
                    </div>
                    <div class="input-icons"> 
                      <i class="icon text-color-blue p-3" style="padding: 5px !important; padding-left: 10px !important;">
                      <p class="text-white font-weight-bold" style="font-style: normal;font-size: 14px; margin-top: 2px;">+61 |</p></i>
                      <input type="number" class="input-field form-control text-white text-xsmall bg-gray--input" style="padding-left: 50px !important; padding-bottom: 8px !important; padding-top: 8px !important;" name="contact_number" id="contact_number" value="{{ Auth::check() ? Auth::user()->contact_number :  old('contact_number') }}">
                    </div>
                    @if($errors->first('contact_number'))
                      <span class="help-block" style="color: red;">{{$errors->first('contact_number')}}</span>
                    @endif    
                  </div>
                  ********************/
                  ?>


                   <div class="col-lg-12 mt-2 {{$errors->first('country_code') ? 'has-error' : NULL}}">
                    <div class="text-color-gray mb-2 {{$errors->first('country_code') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">Country Code<span class="text-danger">*</span></span>
                    </div>
                    
                     <div class="form-group">
    <select name="country_code" id="country_code" class="form-control">
    <option data-countryCode="AU" value="61" <?php if(0 == 61) echo 'selected';?> >Australia (+61)</option>
    <option data-countryCode="DZ" value="213"  <?php if(0 == 213) echo 'selected';?> >Algeria (+213)</option>
    <option data-countryCode="AD" value="376"  <?php if(0 == 376) echo 'selected';?> >Andorra (+376)</option>
    <option data-countryCode="AO" value="244"  <?php if(0 == 244) echo 'selected';?> >Angola (+244)</option>
    <option data-countryCode="AI" value="1264"  <?php if(0 == 1264) echo 'selected';?> >Anguilla (+1264)</option>
    <option data-countryCode="AG" value="1268"  <?php if(0 == 1268) echo 'selected';?> >Antigua &amp; Barbuda (+1268)</option>
    <option data-countryCode="AR" value="54"  <?php if(0 == 54) echo 'selected';?> >Argentina (+54)</option>
    <option data-countryCode="AM" value="374"  <?php if(0 == 374) echo 'selected';?> >Armenia (+374)</option>
    <option data-countryCode="AW" value="297"  <?php if(0 == 297) echo 'selected';?> >Aruba (+297)</option>    
    <option data-countryCode="AT" value="43"  <?php if(0 == 43) echo 'selected';?> >Austria (+43)</option>
    <option data-countryCode="AZ" value="994"  <?php if(0 == 994) echo 'selected';?> >Azerbaijan (+994)</option>
    <option data-countryCode="BS" value="1242" <?php if(0 == 1242) echo 'selected';?> >Bahamas (+1242)</option>
    <option data-countryCode="BH" value="973" <?php if(0 == 973) echo 'selected';?> >Bahrain (+973)</option>
    <option data-countryCode="BD" value="880" <?php if(0 == 880) echo 'selected';?> >Bangladesh (+880)</option>
    <option data-countryCode="BB" value="1246" <?php if(0 == 1246) echo 'selected';?> >Barbados (+1246)</option>
    <option data-countryCode="BY" value="375" <?php if(0 == 375) echo 'selected';?> >Belarus (+375)</option>
    <option data-countryCode="BE" value="32"  <?php if(0 == 32) echo 'selected';?> >Belgium (+32)</option>
    <option data-countryCode="BZ" value="501" <?php if(0 == 501) echo 'selected';?> >Belize (+501)</option>
    <option data-countryCode="BJ" value="229"  <?php if(0 == 229) echo 'selected';?> >Benin (+229)</option>
    <option data-countryCode="BM" value="1441" <?php if(0 == 1441) echo 'selected';?> >Bermuda (+1441)</option>
    <option data-countryCode="BT" value="975"  <?php if(0 == 975) echo 'selected';?> >Bhutan (+975)</option>
    <option data-countryCode="BO" value="591"  <?php if(0 == 591) echo 'selected';?> >Bolivia (+591)</option>
    <option data-countryCode="BA" value="387"  <?php if(0 == 387) echo 'selected';?> >Bosnia Herzegovina (+387)</option>
    <option data-countryCode="BW" value="267"  <?php if(0 == 267) echo 'selected';?> >Botswana (+267)</option>
    <option data-countryCode="BR" value="55"  <?php if(0 == 55) echo 'selected';?> >Brazil (+55)</option>
    <option data-countryCode="BN" value="673"  <?php if(0 == 673) echo 'selected';?> >Brunei (+673)</option>
    <option data-countryCode="BG" value="359"  <?php if(0 == 359) echo 'selected';?> >Bulgaria (+359)</option>
    <option data-countryCode="BF" value="226"  <?php if(0 == 226) echo 'selected';?> >Burkina Faso (+226)</option>
    <option data-countryCode="BI" value="257"  <?php if(0 == 257) echo 'selected';?> >Burundi (+257)</option>
    <option data-countryCode="KH" value="855"  <?php if(0 == 855) echo 'selected';?> >Cambodia (+855)</option>
    <option data-countryCode="CM" value="237"  <?php if(0 == 237) echo 'selected';?> >Cameroon (+237)</option>
    <option data-countryCode="CA" value="1"  <?php if(0 == 1) echo 'selected';?> >Canada (+1)</option>
    <option data-countryCode="CV" value="238"  <?php if(0 == 238) echo 'selected';?> >Cape Verde Islands (+238)</option>
    <option data-countryCode="KY" value="1345"  <?php if(0 == 1345) echo 'selected';?> >Cayman Islands (+1345)</option>
    <option data-countryCode="CF" value="236" <?php if(0 == 236) echo 'selected';?> >Central African Republic (+236)</option>
    <option data-countryCode="CL" value="56"  <?php if(0 == 56) echo 'selected';?> >Chile (+56)</option>
    <option data-countryCode="CN" value="86"  <?php if(0 == 86) echo 'selected';?> >China (+86)</option>
    <option data-countryCode="CO" value="57"  <?php if(0 == 57) echo 'selected';?> >Colombia (+57)</option>
    <option data-countryCode="KM" value="269"  <?php if(0 == 269) echo 'selected';?> >Comoros (+269)</option>
    <option data-countryCode="CG" value="242"  <?php if(0 == 242) echo 'selected';?> >Congo (+242)</option>
    <option data-countryCode="CK" value="682"  <?php if(0 == 682) echo 'selected';?> >Cook Islands (+682)</option>
    <option data-countryCode="CR" value="506"  <?php if(0 == 506) echo 'selected';?> >Costa Rica (+506)</option>
    <option data-countryCode="HR" value="385"  <?php if(0 == 385) echo 'selected';?> >Croatia (+385)</option>
    <option data-countryCode="CU" value="53"  <?php if(0 == 53) echo 'selected';?> >Cuba (+53)</option>
    <option data-countryCode="CY" value="90392"  <?php if(0 == 90392) echo 'selected';?> >Cyprus North (+90392)</option>
    <option data-countryCode="CY" value="357"  <?php if(0 == 357) echo 'selected';?> >Cyprus South (+357)</option>
    <option data-countryCode="CZ" value="42"  <?php if(0 == 42) echo 'selected';?> >Czech Republic (+42)</option>
    <option data-countryCode="DK" value="45"  <?php if(0 == 45) echo 'selected';?> >Denmark (+45)</option>
    <option data-countryCode="DJ" value="253"  <?php if(0 == 253) echo 'selected';?> >Djibouti (+253)</option>
    <option data-countryCode="DM" value="1809"  <?php if(0 == 1809) echo 'selected';?> >Dominica (+1809)</option>
    <option data-countryCode="DO" value="1809"  <?php if(0 == 1809) echo 'selected';?> >Dominican Republic (+1809)</option>
    <option data-countryCode="EC" value="593"  <?php if(0 == 593) echo 'selected';?> >Ecuador (+593)</option>
    <option data-countryCode="EG" value="20"  <?php if(0 == 20) echo 'selected';?> >Egypt (+20)</option>
    <option data-countryCode="SV" value="503"  <?php if(0 == 503) echo 'selected';?> >El Salvador (+503)</option>
    <option data-countryCode="GQ" value="240"  <?php if(0 == 240) echo 'selected';?> >Equatorial Guinea (+240)</option>
    <option data-countryCode="ER" value="291"  <?php if(0 == 291) echo 'selected';?> >Eritrea (+291)</option>
    <option data-countryCode="EE" value="372"  <?php if(0 == 372) echo 'selected';?> >Estonia (+372)</option>
    <option data-countryCode="ET" value="251"  <?php if(0 == 251) echo 'selected';?> >Ethiopia (+251)</option>
    <option data-countryCode="FK" value="500"  <?php if(0 == 500) echo 'selected';?> >Falkland Islands (+500)</option>
    <option data-countryCode="FO" value="298"  <?php if(0 == 298) echo 'selected';?> >Faroe Islands (+298)</option>
    <option data-countryCode="FJ" value="679"  <?php if(0 == 679) echo 'selected';?> >Fiji (+679)</option>
    <option data-countryCode="FI" value="358"  <?php if(0 == 358) echo 'selected';?> >Finland (+358)</option>
    <option data-countryCode="FR" value="33"  <?php if(0 == 33) echo 'selected';?> >France (+33)</option>
    <option data-countryCode="GF" value="594"  <?php if(0 == 594) echo 'selected';?> >French Guiana (+594)</option>
    <option data-countryCode="PF" value="689"  <?php if(0 == 689) echo 'selected';?> >French Polynesia (+689)</option>
    <option data-countryCode="GA" value="241" <?php if(0 == 241) echo 'selected';?> >Gabon (+241)</option>
    <option data-countryCode="GM" value="220" <?php if(0 == 220) echo 'selected';?> >Gambia (+220)</option>
    <option data-countryCode="GE" value="7880" <?php if(0 == 7880) echo 'selected';?> >Georgia (+7880)</option>
    <option data-countryCode="DE" value="49" <?php if(0 == 49) echo 'selected';?> >Germany (+49)</option>
    <option data-countryCode="GH" value="233" <?php if(0 == 233) echo 'selected';?> >Ghana (+233)</option>
    <option data-countryCode="GI" value="350" <?php if(0 == 350) echo 'selected';?> >Gibraltar (+350)</option>
    <option data-countryCode="GR" value="30" <?php if(0 == 30) echo 'selected';?> >Greece (+30)</option>
    <option data-countryCode="GL" value="299" <?php if(0 == 299) echo 'selected';?> >Greenland (+299)</option>
    <option data-countryCode="GD" value="1473" <?php if(0 == 1473) echo 'selected';?> >Grenada (+1473)</option>
    <option data-countryCode="GP" value="590" <?php if(0 == 590) echo 'selected';?> >Guadeloupe (+590)</option>
    <option data-countryCode="GU" value="671" <?php if(0 == 671) echo 'selected';?> >Guam (+671)</option>
    <option data-countryCode="GT" value="502" <?php if(0 == 502) echo 'selected';?> >Guatemala (+502)</option>
    <option data-countryCode="GN" value="224" <?php if(0 == 224) echo 'selected';?> >Guinea (+224)</option>
    <option data-countryCode="GW" value="245" <?php if(0 == 245) echo 'selected';?> >Guinea - Bissau (+245)</option>
    <option data-countryCode="GY" value="592" <?php if(0 == 592) echo 'selected';?> >Guyana (+592)</option>
    <option data-countryCode="HT" value="509" <?php if(0 == 509) echo 'selected';?> >Haiti (+509)</option>
    <option data-countryCode="HN" value="504" <?php if(0 == 504) echo 'selected';?> >Honduras (+504)</option>
    <option data-countryCode="HK" value="852" <?php if(0 == 852) echo 'selected';?> >Hong Kong (+852)</option>
    <option data-countryCode="HU" value="36" <?php if(0 == 36) echo 'selected';?> >Hungary (+36)</option>
    <option data-countryCode="IS" value="354" <?php if(0 == 354) echo 'selected';?> >Iceland (+354)</option>
    <option data-countryCode="IN" value="91"  <?php if(0 == 91) echo 'selected';?> >India (+91)</option>
    <option data-countryCode="ID" value="62"  <?php if(0 == 62) echo 'selected';?> >Indonesia (+62)</option>
    <option data-countryCode="IR" value="98"  <?php if(0 == 98) echo 'selected';?> >Iran (+98)</option>
    <option data-countryCode="IQ" value="964"  <?php if(0 == 964) echo 'selected';?> >Iraq (+964)</option>
    <option data-countryCode="IE" value="353"  <?php if(0 == 353) echo 'selected';?> >Ireland (+353)</option>
    <option data-countryCode="IL" value="972"  <?php if(0 == 972) echo 'selected';?> >Israel (+972)</option>
    <option data-countryCode="IT" value="39"  <?php if(0 == 39) echo 'selected';?> >Italy (+39)</option>
    <option data-countryCode="JM" value="1876"  <?php if(0 == 1876) echo 'selected';?> >Jamaica (+1876)</option>
    <option data-countryCode="JP" value="81"  <?php if(0 == 81) echo 'selected';?> >Japan (+81)</option>
    
    <option data-countryCode="KW" value="965"  <?php if(0 == 965) echo 'selected';?> >Kyrgyzstan (+996)</option>
    <option data-countryCode="LA" value="856" <?php if(0 == 856) echo 'selected';?>  >Laos (+856)</option>
    <option data-countryCode="LV" value="371" <?php if(0 == 371) echo 'selected';?> >Latvia (+371)</option>
    <option data-countryCode="LB" value="961" <?php if(0 == 961) echo 'selected';?> >Lebanon (+961)</option>
    <option data-countryCode="LS" value="266" <?php if(0 == 266) echo 'selected';?> >Lesotho (+266)</option>
    <option data-countryCode="LR" value="231"  <?php if(0 == 231) echo 'selected';?> >Liberia (+231)</option>
    <option data-countryCode="LY" value="218"  <?php if(0 == 218) echo 'selected';?> >Libya (+218)</option>
    <option data-countryCode="LI" value="417"  <?php if(0 == 417) echo 'selected';?> >Liechtenstein (+417)</option>
    <option data-countryCode="LT" value="370"  <?php if(0 == 370) echo 'selected';?> >Lithuania (+370)</option>
    <option data-countryCode="LU" value="352"  <?php if(0 == 352) echo 'selected';?> >Luxembourg (+352)</option>
    <option data-countryCode="MO" value="853"  <?php if(0 == 853) echo 'selected';?> >Macao (+853)</option>
    <option data-countryCode="MK" value="389"  <?php if(0 == 389) echo 'selected';?> >Macedonia (+389)</option>
    <option data-countryCode="MG" value="261"  <?php if(0 == 261) echo 'selected';?> >Madagascar (+261)</option>
    <option data-countryCode="MW" value="265"  <?php if(0 == 265) echo 'selected';?> >Malawi (+265)</option>
    <option data-countryCode="MY" value="60"  <?php if(0 == 60) echo 'selected';?> >Malaysia (+60)</option>
    <option data-countryCode="MV" value="960"  <?php if(0 == 960) echo 'selected';?> >Maldives (+960)</option>
    <option data-countryCode="ML" value="223"  <?php if(0 == 223) echo 'selected';?> >Mali (+223)</option>
    <option data-countryCode="MT" value="356"  <?php if(0 == 356) echo 'selected';?> >Malta (+356)</option>
    <option data-countryCode="MH" value="692"  <?php if(0 == 692) echo 'selected';?> >Marshall Islands (+692)</option>
    <option data-countryCode="MQ" value="596"  <?php if(0 == 596) echo 'selected';?> >Martinique (+596)</option>
    <option data-countryCode="MR" value="222"  <?php if(0 == 222) echo 'selected';?> >Mauritania (+222)</option>
    <option data-countryCode="YT" value="269"  <?php if(0 == 269) echo 'selected';?> >Mayotte (+269)</option>
    <option data-countryCode="MX" value="52"  <?php if(0 == 52) echo 'selected';?> >Mexico (+52)</option>
    <option data-countryCode="FM" value="691"  <?php if(0 == 691) echo 'selected';?> >Micronesia (+691)</option>
    <option data-countryCode="MD" value="373"  <?php if(0 == 373) echo 'selected';?> >Moldova (+373)</option>
    <option data-countryCode="MC" value="377"  <?php if(0 == 377) echo 'selected';?> >Monaco (+377)</option>
    <option data-countryCode="MN" value="976"  <?php if(0 == 976) echo 'selected';?> >Mongolia (+976)</option>
    <option data-countryCode="MS" value="1664"  <?php if(0 == 1664) echo 'selected';?> >Montserrat (+1664)</option>
    <option data-countryCode="MA" value="212"  <?php if(0 == 212) echo 'selected';?> >Morocco (+212)</option>
    <option data-countryCode="MZ" value="258"  <?php if(0 == 258) echo 'selected';?> >Mozambique (+258)</option>
    <option data-countryCode="MN" value="95"  <?php if(0 == 95) echo 'selected';?> >Myanmar (+95)</option>
    <option data-countryCode="NA" value="264"  <?php if(0 == 264) echo 'selected';?> >Namibia (+264)</option>
    <option data-countryCode="NR" value="674"  <?php if(0 == 674) echo 'selected';?> >Nauru (+674)</option>
    <option data-countryCode="NP" value="977"  <?php if(0 == 977) echo 'selected';?> >Nepal (+977)</option>
    <option data-countryCode="NL" value="31"  <?php if(0 == 31) echo 'selected';?> >Netherlands (+31)</option>
    <option data-countryCode="NC" value="687"  <?php if(0 == 687) echo 'selected';?> >New Caledonia (+687)</option>
    <option data-countryCode="NZ" value="64"  <?php if(0 == 64) echo 'selected';?> >New Zealand (+64)</option>
    <option data-countryCode="NI" value="505"  <?php if(0 == 505) echo 'selected';?> >Nicaragua (+505)</option>
    <option data-countryCode="NE" value="227"  <?php if(0 == 227) echo 'selected';?> >Niger (+227)</option>
    <option data-countryCode="NG" value="234"  <?php if(0 == 234) echo 'selected';?> >Nigeria (+234)</option>
    <option data-countryCode="NU" value="683"  <?php if(0 == 683) echo 'selected';?> >Niue (+683)</option>
    <option data-countryCode="NF" value="672"  <?php if(0 == 672) echo 'selected';?> >Norfolk Islands (+672)</option>
    <option data-countryCode="NP" value="670"  <?php if(0 == 670) echo 'selected';?> >Northern Marianas (+670)</option>
    <option data-countryCode="NO" value="47"  <?php if(0 == 47) echo 'selected';?> >Norway (+47)</option>
    <option data-countryCode="OM" value="968"  <?php if(0 == 968) echo 'selected';?> >Oman (+968)</option>
    <option data-countryCode="PW" value="680"  <?php if(0 == 680) echo 'selected';?> >Palau (+680)</option>
    <option data-countryCode="PA" value="507"  <?php if(0 == 507) echo 'selected';?> >Panama (+507)</option>
    <option data-countryCode="PG" value="675"  <?php if(0 == 675) echo 'selected';?> >Papua New Guinea (+675)</option>
    <option data-countryCode="PY" value="595"  <?php if(0 == 595) echo 'selected';?> >Paraguay (+595)</option>
    <option data-countryCode="PE" value="51"  <?php if(0 == 51) echo 'selected';?> >Peru (+51)</option>
    <option data-countryCode="PH" value="63"  <?php if(0 == 63) echo 'selected';?> >Philippines (+63)</option>
    <option data-countryCode="PL" value="48"  <?php if(0 == 48) echo 'selected';?> >Poland (+48)</option>
    <option data-countryCode="PT" value="351"  <?php if(0 == 351) echo 'selected';?> >Portugal (+351)</option>
    <option data-countryCode="PR" value="1787"  <?php if(0 == 1787) echo 'selected';?> >Puerto Rico (+1787)</option>
    <option data-countryCode="QA" value="974"  <?php if(0 == 974) echo 'selected';?> >Qatar (+974)</option>
    <option data-countryCode="RE" value="262"  <?php if(0 == 262) echo 'selected';?> >Reunion (+262)</option>
    <option data-countryCode="RO" value="40"  <?php if(0 == 40) echo 'selected';?> >Romania (+40)</option>
    <option data-countryCode="RW" value="250"  <?php if(0 == 250) echo 'selected';?> >Rwanda (+250)</option>
    <option data-countryCode="SM" value="378"  <?php if(0 == 378) echo 'selected';?> >San Marino (+378)</option>
    <option data-countryCode="ST" value="239"  <?php if(0 == 239) echo 'selected';?> >Sao Tome &amp; Principe (+239)</option>
    <option data-countryCode="SA" value="966"  <?php if(0 == 966) echo 'selected';?> >Saudi Arabia (+966)</option>
    <option data-countryCode="SN" value="221"  <?php if(0 == 221) echo 'selected';?> >Senegal (+221)</option>
    <option data-countryCode="CS" value="381"  <?php if(0 == 381) echo 'selected';?> >Serbia (+381)</option>
    <option data-countryCode="SC" value="248"  <?php if(0 == 248) echo 'selected';?> >Seychelles (+248)</option>
    <option data-countryCode="SL" value="232"  <?php if(0 == 232) echo 'selected';?> >Sierra Leone (+232)</option>
    <option data-countryCode="SG" value="65"  <?php if(0 == 65) echo 'selected';?> >Singapore (+65)</option>
    <option data-countryCode="SK" value="421"  <?php if(0 == 421) echo 'selected';?> >Slovak Republic (+421)</option>
    <option data-countryCode="SI" value="386"  <?php if(0 == 386) echo 'selected';?> >Slovenia (+386)</option>
    <option data-countryCode="SB" value="677"  <?php if(0 == 677) echo 'selected';?> >Solomon Islands (+677)</option>
    <option data-countryCode="SO" value="252"  <?php if(0 == 252) echo 'selected';?> >Somalia (+252)</option>
    <option data-countryCode="ZA" value="27"  <?php if(0 == 27) echo 'selected';?> >South Africa (+27)</option>
    <option data-countryCode="ES" value="34"  <?php if(0 == 34) echo 'selected';?> >Spain (+34)</option>
    <option data-countryCode="LK" value="94"  <?php if(0 == 94) echo 'selected';?> >Sri Lanka (+94)</option>
    <option data-countryCode="SH" value="290"  <?php if(0 == 290) echo 'selected';?> >St. Helena (+290)</option>
    <option data-countryCode="KN" value="1869"  <?php if(0 == 1869) echo 'selected';?> >St. Kitts (+1869)</option>
    <option data-countryCode="SC" value="1758"  <?php if(0 == 1758) echo 'selected';?> >St. Lucia (+1758)</option>
    <option data-countryCode="SD" value="249"  <?php if(0 == 249) echo 'selected';?> >Sudan (+249)</option>
    <option data-countryCode="SR" value="597"  <?php if(0 == 597) echo 'selected';?> >Suriname (+597)</option>
    <option data-countryCode="SZ" value="268"  <?php if(0 == 268) echo 'selected';?> >Swaziland (+268)</option>
    <option data-countryCode="SE" value="46"  <?php if(0 == 46) echo 'selected';?> >Sweden (+46)</option>
    <option data-countryCode="CH" value="41"  <?php if(0 == 41) echo 'selected';?> >Switzerland (+41)</option>
    <option data-countryCode="SI" value="963"  <?php if(0 == 963) echo 'selected';?> >Syria (+963)</option>
    <option data-countryCode="TW" value="886"  <?php if(0 == 886) echo 'selected';?> >Taiwan (+886)</option>
    <option data-countryCode="TH" value="66"  <?php if(0 == 66) echo 'selected';?> >Thailand (+66)</option>
    <option data-countryCode="TG" value="228"  <?php if(0 == 228) echo 'selected';?> >Togo (+228)</option>
    <option data-countryCode="TO" value="676"  <?php if(0 == 676) echo 'selected';?> >Tonga (+676)</option>
    <option data-countryCode="TT" value="1868"  <?php if(0 == 1868) echo 'selected';?> >Trinidad &amp; Tobago (+1868)</option>
    <option data-countryCode="TN" value="216"  <?php if(0 == 216) echo 'selected';?> >Tunisia (+216)</option>
    <option data-countryCode="TR" value="90"  <?php if(0 == 90) echo 'selected';?> >Turkey (+90)</option>
    <option data-countryCode="TM" value="7"  <?php if(0 == 7) echo 'selected';?> >Turkmenistan (+7)</option>
    <option data-countryCode="TM" value="993"  <?php if(0 == 993) echo 'selected';?> >Turkmenistan (+993)</option>
    <option data-countryCode="TC" value="1649"  <?php if(0 == 1649) echo 'selected';?> >Turks &amp; Caicos Islands (+1649)</option>
    <option data-countryCode="TV" value="688"  <?php if(0 == 688) echo 'selected';?> >Tuvalu (+688)</option>
    <option data-countryCode="UG" value="256"  <?php if(0 == 256) echo 'selected';?> >Uganda (+256)</option>
    <option data-countryCode="GB" value="44"  <?php if(0 == 44) echo 'selected';?> >UK (+44)</option>
    <option data-countryCode="UA" value="380"  <?php if(0 == 380) echo 'selected';?> >Ukraine (+380)</option>
    <option data-countryCode="AE" value="971"  <?php if(0 == 971) echo 'selected';?> >United Arab Emirates (+971)</option>
    <option data-countryCode="UY" value="598"  <?php if(0 == 598) echo 'selected';?> >Uruguay (+598)</option>
    <option data-countryCode="US" value="1"  <?php if(0 == 1) echo 'selected';?> >USA (+1)</option>
    <option data-countryCode="UZ" value="7"  <?php if(0 == 7) echo 'selected';?> >Uzbekistan (+7)</option>
    <option data-countryCode="VU" value="678">  <?php if(0 == 678) echo 'selected';?> Vanuatu (+678)</option>
    <option data-countryCode="VA" value="379"  <?php if(0 == 379) echo 'selected';?> >Vatican City (+379)</option>
    <option data-countryCode="VE" value="58"  <?php if(0 == 58) echo 'selected';?> >Venezuela (+58)</option>
    <option data-countryCode="VN" value="84"  <?php if(0 == 84) echo 'selected';?> >Vietnam (+84)</option>
    <option data-countryCode="VG" value="84"  <?php if(0 == 84) echo 'selected';?> >Virgin Islands - British (+1284)</option>
    <option data-countryCode="VI" value="84"  <?php if(0 == 84) echo 'selected';?> >Virgin Islands - US (+1340)</option>
    <option data-countryCode="WF" value="681"  <?php if(0 == 681) echo 'selected';?> >Wallis &amp; Futuna (+681)</option>
    <option data-countryCode="YE" value="969"  <?php if(0 == 969) echo 'selected';?> >Yemen (North)(+969)</option>
    <option data-countryCode="YE" value="967"  <?php if(0 == 967) echo 'selected';?> >Yemen (South)(+967)</option>
    <option data-countryCode="ZM" value="260"  <?php if(0 == 260) echo 'selected';?> >Zambia (+260)</option>
    <option data-countryCode="ZW" value="263"  <?php if(0 == 263) echo 'selected';?> >Zimbabwe (+263)</option>
    </select>
</div>


                    @if($errors->first('country_code'))
                      <span class="help-block" style="color: red;">{{$errors->first('country_code')}}</span>
                    @endif    
                  </div>


                  <div class="col-lg-12 mt-2 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
                    <div class="text-color-gray mb-2 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">Contact Number<span class="text-danger">*</span></span>
                    </div>
                    
                     
                      <input type="number" class="form-control text-xsmall bg-gray--input text-white text-white" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="contact_number" id="contact_number" value="{{ Auth::check() ? Auth::user()->contact_number : old('contact_number') }}">
                    
                    @if($errors->first('contact_number'))
                      <span class="help-block" style="color: red;">{{$errors->first('contact_number')}}</span>
                    @endif    
                  </div>

                  
                    <div class="col-lg-12 mt-2">
                      <div class="text-color-gray mb-2 {{$errors->first('email') ? 'has-error' : NULL}}">
                        <i class="fas fa-angle-double-right text-color-blue"></i>
                        <span class="ml-2 text-xxsmall font-semibold text-color-gray ">Email Address<span class="text-danger">*</span></span>
                      </div>
                      <input type="email" class="form-control text-xsmall bg-gray--input text-white text-white" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="email" id="email" value="{{ Auth::check() ? Auth::user()->email : old('email') }}">
                      @if($errors->first('email'))
                        <span class="help-block" style="color:red;">{{$errors->first('email')}}</span>
                      @endif   
                    </div>
                    <div class="col-lg-6"></div>
                </div>
              </div>
              <div class="col-lg-7 pr-4" id="gradient-background" style="padding-top: 16px !important;">
                <div class="row p-3">
                  <div class="col-lg-12 text-left">
                    <p class="text-xsmall font-semibold spacing-1 text-color-white">You Selected</p>
                  </div>
                  @forelse($package as $index)   
                
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 mt-2">
                    <div class="styled-radio">
                      <input type="checkbox" disabled checked="" id="regular" name="service-type">
                      <label for="regular" class="text-xxxsmall spacing-1 card-shadow  style-cbox" style="color: white;">
                      @if($index->id == "1")
                        <img src="{{ asset('frontend/images/skip.png') }}" class=" mb-2 img-fluid" style="height:50% !important;width:50% !important"> 
                      @else
                        <img src="{{asset($index->directory.'/'.$index->filename)}}" class=" mb-2 img-fluid" style="height:50% !important;width:60% !important"> 
                      @endif<br>
                      <span class="mobile-text">{{Str::title($index->name)}}</span></label>
                    </div>
                  </div>
                  @empty
                  <h1>No Selected Package</h1>
                  @endforelse
                  <div class="col-lg-12">
                    <div class="row">
                      <div class="col-lg-12 mt-3">
                        <span class="text-color-white font-semibold">Do you want to secure a spot now?</span>
                      </div>
                      <div class="col-md-6 col-12 text-left">
                        <button type="submit"  id="next" class="next card-shadow btn font-semibold text-uppercase text-xsmall text-color-white w-100 pt-3 pb-3 mt-3" style="background-color:#4472C4;color: white !important;">Yes</button>
                        <p class="text-white mt-2 text-xxxsmall spacing-1">Give me a quote and secure booking now. </p>
                      </div>
                      <div class="col-md-6 col-12 text-left">
                        <a  href="#modal_call" data-toggle="modal"  data-role="view" style="text-decoration: none;"><button class="btn text-color-white mt-3 w-100 font-semibold card-shadow text-uppercase text-xsmall  pt-3 pb-3" style="background-color: #972121;">No</button></a>
                        <p class="text-white mt-2 text-xxxsmall spacing-1">Call me and discuss price and schedule.</p>
                      </div>     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div class="col-lg-3"></div>
  </div>
  </form>
  <form action="{{ route('frontend.accounting.cancel') }}" method="POST" id="cancel_form">
    <input type="hidden" name="fname" id="input_fname_cancel">
    <input type="hidden" name="lname" id="input_lname_cancel">
    <input type="hidden" name="email" id="input_email_cancel">
    <input type="hidden" name="contact_number" id="input_contact_cancel">
    {{ csrf_field() }}
   
  </form>
</div>



<div class="modal fade" id="modal_call" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top: 25% !important">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <div class="modal-content">
      <div class="modal-header d-flex justify-content-end">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       
      <div class="modal-body p-4 text-left">
        <p class="text-color-gray font-semibold"><span class="text-errors show">Are you sure you wanted to cancel your booking?<br>Please Make sure you fill up all the required fields</span>
        <div class="row justify-content-center">
          <button id ="proceeds" class="btn bg-color-blue w-50 text-white text-errors text-uppercase show">Proceed</button>
        </div>
      </div>
   
     
    </div>
  </div>
</div>
<!-- end Modal -->
@include('frontend._components.footer-sub')

@stop
@section('scripts')
<script type="text/javascript">
  

$(document).ready(function(){

  $(document).on('click','a[data-role=view]',function(){
    var fname = $('#fname').val()
    var lname = $('#lname').val()
    var email = $('#email').val()
    var contact = $('#contact_number').val()

    $('#input_fname_cancel').val(fname)
    $('#input_lname_cancel').val(lname)
    $('#input_email_cancel').val(email)
    $('#input_contact_cancel').val(contact)

    $('.text-errors').addClass('show')
    $('.text').addClass('d-none')
  })


})

$('#proceeds').on('click',function(){
  var fname = $('#fname').val()
  var lname = $('#lname').val()
  var email = $('#email').val()
  var contact = $('#contact_number').val()

  $('#input_fname_cancel').val(fname)
  $('#input_lname_cancel').val(lname)
  $('#input_email_cancel').val(email)
  $('#input_contact_cancel').val(contact)
  $('#cancel_form').submit();

})

</script>
@stop
@section('scripts-content')
<script>



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
    padding: 10px !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
    padding: 10px !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Accounting.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 
 #gradient-background {
    background-color: #2047A4;
}

@media only screen and (min-width: 1501px) {

  .footer-bottom{
    position: absolute;
    bottom: 0%;
    top: 100% !important;
    }
  }
@media only screen and (max-width: 1500px) {

  .footer-bottom{
    margin-top: 50px
    }
  }


  @media only screen and (max-width: 992px) {
.mobile-text {
  font-size: 13px;
}
}
 @media only screen and (min-width: 992px) {
.mobile-text {
  font-size: 11px;
}
}
  a:hover{
    color: white !important;
  }
  .hidden {
    display: none;
  }
   .show {
    display: block !important;
  }


</style>
@stop