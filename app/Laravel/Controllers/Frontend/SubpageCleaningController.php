<?php



namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\CleaningService;
use App\Laravel\Models\ServicePackage;
use App\Laravel\Models\Service;
use App\Laravel\Models\OtherService;
use App\Laravel\Models\Postcode;
use App\Laravel\Models\User;
use App\Laravel\Models\Finance;
use App\Laravel\Requests\Frontend\BookingRequest;
use App\Mail\BookingNumber;

use App\Http\Controllers\Controller;
use App\Laravel\Models\UserTerritory;
use App\Laravel\Services\ResponseManager;
use Illuminate\Http\Request;
use App\Laravel\Events\SendBooking;
use App\Laravel\Models\WorkAvailability;

use Helper, Carbon, Session, Str, DB,Input,Event,PDF,Curl,Signature,Log,Auth,Mail;


class SubpageCleaningController extends Controller
{
	protected $data;
	function __construct($foo = null)
	{
		$this->data = [];
	}
	public function cleaning(Request $request){
		
		$request->session()->forget('booking_info');
		$request->session()->forget('current_step');
		$request->session()->forget('clean');
		$request->session()->forget('percent');
	$this->data['message_alert'] =  '';
		return view ("frontend.subpage.cleaning.step1.index",$this->data);
	}

	
	public function accounting(){
		return view ('frontend.subpage.accounting.index');
	}
	public function forex(){
		return view ('frontend.subpage.forex.index');
	}
	public function itService(){
		return view ('frontend.subpage.it-service.index');
	}

	public function back(Request $request)
	{
		$current_step = $request->session()->get('current_step');
		$request->session()->get('current_step') - 2;
		 // return $request->session()->get('current_step');
		return redirect()->route('frontend.cleaning.index');
	}

	public function create(BookingRequest $request){
		
		
	}

	public function getAllHierarchForUsers($id){
		$parent = array();
		if(intval($id)){
			$userDetails=User::select('*')->where('id', intval($id))->get()->first()->toArray();
			$parent[] = $userDetails['email'];

			if (intval($userDetails['head_id']) == 0) {
	           return $parent;
	       } else {
	           $push = $this->getAllHierarchForUsers($userDetails['head_id']);
	           array_push($parent,$push);
	       }  
		}
		

       return $parent;   
	}

	public function search(Request $request)
	{
		$postcode = $request->get('term');

		$result = Postcode::where('place_name','LIKE','%'.$postcode."%")
		->orWhere('postcode','LIKE','%'.$postcode."%")	
		->get();

		return $x = json_decode($result,true);

	}

	public function cleaning_index(Request $request)
	{
		$current_progress = $request->session()->get('current_progress');

		switch ($current_progress) {
			case '1':
				return view("frontend.subpage.cleaning.step1.index",$this->data);
				break;
			case '2':
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			case '3':
				
			$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your Big Boss Cleaning Experts",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			case '4':
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			case '5':
				$property = session()->get('clean.property');				
		     	$this->data['cleaning_service'] = Service::orderBy('created_at','DESC')->where('id','<>',"1")->where('group',"cleaning")->where('service_type' ,'LIKE' ,'%'.$property.'%')->take(5)->get();
				$this->data['oven'] = ServicePackage::where('service_name','oven')->first();
				$this->data['carpet'] = ServicePackage::where('service_name','carpet')->first();
				$this->data['window'] = ServicePackage::where('service_name','window')->first();
				$this->data['rubbish'] = ServicePackage::where('service_name','rubbish')->first();
				$this->data['lawn'] = ServicePackage::where('service_name','lawn');
				$this->data['servcices_count'] = ServicePackage::all()->count();
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			case '6':
				$this->data['package'] = Service::whereIn('id', session()->get('clean.services')['svc'])->get();
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			case '7':
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			case '8':
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			case '9':
				return view("frontend.subpage.cleaning.step{$current_progress}.index",$this->data);
				break;
			default:
				return view("frontend.subpage.cleaning.step1.index",$this->data);
				break;
		}
	}


	public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
	{
	  $i = $j = $c = 0;
	  for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
	    if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
	     ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
	       $c = !$c;
	  }
	  return $c;
	}


	public function cleaning_store(BookingRequest $request)
	{

		$svc_total = 0;
		$current_progress = $request->session()->get('current_progress');
		
		switch ($current_progress) {
			case '1':
				$validate = Postcode::where('postcode',$request->get('postcode'))->count();
				if($validate > 0)
				{
					$request->session()->put('clean.postcode',$request->get('postcode'));
					$request->session()->put('clean.longitude1',$request->get('longitude'));
					$request->session()->put('clean.latitude2',$request->get('latitude'));
				}
				else
				{
					session()->flash('notification-status','error');
					session()->flash('notification-msg','no post code found Please try again.');
					return redirect()->back();
				}
				if ($request->get('postcode')) {
					session()->put('current_progress',$current_progress + 1) ;
				}
				break;
			case '2':
				$request->session()->put('clean.location',$request->location);
				$request->session()->put('clean.postcode',$request->get('postcode'));
				$request->session()->put('clean.longitude', $request->get('geo_long'));
				$request->session()->put('clean.latitude', $request->get('geo_lat'));
				$request->session()->put('clean.state', $request->get('state'));
				$request->session()->put('clean.sub', $request->get('sub'));
				$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your local big boss cleaning expert",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';
				session()->put('current_progress',$current_progress + 1) ;
				break;
			case '3':

				$request->session()->put('clean.property_type',$request->get('property'));
				
				if ($request->get('property') == "other") {
					session()->put('current_progress',$current_progress + 2) ;
				}else{
					session()->put('current_progress',$current_progress + 1) ;
				}
				
				break;
			case '4':

				$request->session()->put('clean.type',$request->get('type'));
				session()->put('current_progress',$current_progress + 1) ;
				break;
			case '5':
				if(!is_null($request->get('svc'))){
				$this->data['package'] = Service::whereIn('id' , $request->only('svc')['svc'])->get();
				}else{
				$this->data['package'] = Service::whereIn('id' , [1])->get();
				}
				foreach ($this->data['package'] as $key => $value) {
				$svc_name[] = $value->name;
				$svc_price[] = $value->price;
				$svc_total += $value->price;
				}
				$request->session()->put('clean.services',$request->only('svc'));
				$request->session()->put('clean.svc_name',$svc_name);
				$request->session()->put('clean.svc_total',$svc_total);
				session()->put('current_progress',$current_progress + 1) ;
				break;
			case '6':
				$request->session()->put('booking_info.booking_id',uniqid());
				$request->session()->put('clean.fname', $request->get('fname'));
				$request->session()->put('clean.lname', $request->get('lname'));
				$request->session()->put('clean.email', $request->get('email'));
				$request->session()->put('clean.contact', $request->get('contact_number'));
				$request->session()->put('clean.country_code', $request->get('country_code'));
				session()->put('current_progress',$current_progress + 1) ;
				break;
			case '7':
				if(time()>strtotime($request->booking_date." ".Helper::time_only_24hour($request->time_start))){
					session()->flash('notification-status','error');
					session()->flash('notification-msg','Previous time is not possible!');
					return redirect()->back();
				}
				// echo "<pre>"; 
				// echo date('d-m-y-h-i-s',strtotime($request->booking_date." ".Helper::time_only_24hour($request->time_start)));
				// echo '$request->booking_date-----'.$request->booking_date;
				// echo '<br> $request->time_start-----'.Helper::time_only_24hour($request->time_start);
				// exit;
				$request->session()->put('clean.booking_date', $request->booking_date);
				$request->session()->put('clean.time_start', Helper::time_only_24hour($request->time_start));
				$request->session()->put('clean.time_end',  Helper::time_only_24hour($request->time_end));
				session()->put('current_progress',$current_progress + 1) ;
				break;
			case '8':
				session()->put('current_progress',$current_progress + 1) ;
				break;
			case '9':
				$request->session()->put('clean.payment', $request->get('payment'));
				if($request->session()->has('clean.postcode')){	
					$services =  new CleaningService;
					$services->customer_id =  Auth::check() ? auth()->user()->id : NULL;
					$services->postcode = $request->session()->get('clean.postcode');
					$services->location = $request->session()->get('clean.location');
					$services->service_type = $request->session()->get('clean.type');
					$services->service_type_amount = $request->session()->get('clean.service_type_amount');
					$services->property_type = $request->session()->get('clean.property_type');
					$services->services = implode(",",$request->session()->get('clean.svc_name'));
					$services->service_id = implode(",",$request->session()->get('clean.services.svc'));
					$services->name = $request->session()->get('clean.fname');
					$services->lastname = $request->session()->get('clean.lname');
					$services->email = $request->session()->get('clean.email');
					$services->contact = $request->session()->get('clean.contact');
					$services->booking_id = $request->session()->get('booking_info.booking_id');
					$services->booking_date = Carbon::parse($request->session()->get('clean.booking_date'));
					$services->time_start =  $request->session()->get('clean.time_start');
					$services->time_end =  $request->session()->get('clean.time_end');
					$services->payment =  $request->session()->get('clean.payment');
					$services->longitude =  $request->session()->get('clean.longitude');
					$services->latitude =  $request->session()->get('clean.latitude');
					$services->region =  $request->session()->get('clean.state');
					$services->amount =   $request->session()->get('clean.svc_total');
					$services->booking_total =   $request->session()->get('clean.service_price');
					$services->status =   "pending";
					$services->type =   "cleaning";


					
					$UserTerritory_details = UserTerritory::whereIn('user_id', function($query){
										$query->select('id')
												->where('type', 'franchisee')
												->where('head_id', '<>' , NULL)
												->from('user');
									})->get()->toArray();
			  		$franchisee_details =[];
			  		$franchisee_ids =[];

			  		foreach($UserTerritory_details as $key=>$val){
			  			$franchisee_details[$val['user_id']]['vertices_x'] [] = $val ['longitude'] ;
			  			$franchisee_details[$val['user_id']]['vertices_y'] [] = $val ['latitude'] ;
			  		}

			  		foreach($franchisee_details as $key=>$val){
			  			$points_polygon = count($val['vertices_x']) - 1;
			  			$longitude_x = $services->longitude;
			  			$latitude_y  = $services->latitude;
			  			if($this->is_in_polygon($points_polygon, $val['vertices_x'], $val['vertices_y'], $longitude_x, $latitude_y)){
			  				$franchisee_ids [] =$key;
			  			}
			  		}

			  		/****
					$franchisee_ids_new=User::select('user.id')	->where('user.type', 'franchisee')
												->where('user.head_id', '<>' , NULL)
												->where('user_info.post_code', $request->session()->get('clean.postcode'))
												->join('user_info', 'user.id', '=', 'user_info.user_id')->get();
					*****/

					if(count($franchisee_ids) == 0){
						$franchisee_ids = UserTerritory::whereIn('user_id', function($query){
										$query->select('id')
												->where('type', 'franchisee')
												->where('head_id', '<>' , NULL)
												->from('user');
									})
									->distance([0,10],$services->latitude,$services->longitude)
									->pluck('user_id')->toArray();
					} 
					
			  		$work_avail = WorkAvailability::whereIn('user_id',count($franchisee_ids) > 0 ? $franchisee_ids : [0])->where('date' , $services->booking_date)
							->orderByRaw("FIELD(work_stated,'territory','local','all_areas')")->get();

					$selected_franchisee = 0;

		foreach($work_avail as $index => $franchisee){
			$count_lead = CleaningService::where('franchisee_id',$franchisee->user_id)
										->where('booking_date' , $services->booking_date)->where('status' , "pending")->count();
			if($count_lead < $franchisee->max_leads){
				$with_parallel = CleaningService::select("*",DB::raw("TIME_TO_SEC('{$services->booking_date} {$services->time_start}') AS timetosec"),DB::raw("TIME_TO_SEC(CONCAT(booking_date,' ',time_start)) AS timetosec2"))->where('franchisee_id',$franchisee->user_id)
										->where('booking_date' , $services->booking_date)
										->where(function($query) use($services){
											return $query->where(function($q) use($services){
													return $q->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_start)) <= TIME_TO_SEC('{$services->booking_date} {$services->time_start}')")
																->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_end)) >= TIME_TO_SEC('{$services->booking_date} {$services->time_start}')");
											})->orwhere(function($q) use($services){
												return $q->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_start)) >= TIME_TO_SEC('{$services->booking_date} {$services->time_end}')")
															->whereRaw("TIME_TO_SEC(CONCAT(booking_date,' ',time_end)) <= TIME_TO_SEC('{$services->booking_date} {$services->time_end}')");
											});
											
										})->count();
				if($with_parallel > 0){
					//goto skip_me;
					continue;
				}
				$selected_franchisee= $franchisee->user_id;
				break;
			}
			//skip_me:

			if($index == count($work_avail)-1){
				// $selected_franchisee = $work_avail[0]
				//unservice
			}
		}	


					//echo '<pre>'; print_r($franchisee_ids_new);exit;
					//$franchisee_ids=[];
					//if(count($franchisee_ids_new)){
						// foreach($franchisee_ids_new as $key=>$val){
						// 	$franchisee_ids[]=$val->id;
						// }
					//}
					$emailAllHierarch =['Info@bigbossgroup.com.au'];
					//$franchisee_ids[0] =37;
					if($selected_franchisee !=0){
						$selected_franchisee= $selected_franchisee;
						$services->franchisee_id = $selected_franchisee;
						$services->fit_status = 'fitted';
						$services->status = 'ongoing';


						$email_lict_cc=$this->getAllHierarchForUsers($selected_franchisee);
						//$emailAllHierarch =[];
						foreach($email_lict_cc as $key=>$val){
							if(is_array($val)){
								foreach($val as $key2=>$val2){


								if(is_array($val2)){
									foreach($val2 as $key3=>$val3){
										$emailAllHierarch []=$val3;
									}
								}else{
									$emailAllHierarch []=$val2;
								}

								}
							}else{
								$emailAllHierarch []=$val;
							}
						}

						 
					}


					if ($services->save()) {
						
						if ($request->session()->get('clean.email')) {
							$insert[] = [
						                'email' => $services->email,
						                'firstname' => $services->name,
						                'lastname' => $services->lname,
						                'name' =>  $services->name . " " . $services->lname,
						                'booking_id' => $services->booking_id,
						                "emailAllHierarch"=>$emailAllHierarch,
						                "other_details_booking"=>
						                [
						                	'name' =>  $services->name . " " . $services->lname,
						                	'booking_id' => $services->booking_id,
						                	'email' => $services->email,
						                	'postcode' => $services->postcode,
							                'location' => $services->location,
							                'service_type' => $services->service_type,
							                'service_type_amount' => $services->service_type_amount,
							                'services' => $services->services,
							                'property_type' => $services->property_type,
							                'contact' => $services->contact,
							                'booking_date' => $services->booking_date,
											'time_start' => $services->time_start,
											'time_end' => $services->time_end,
											'region' => $services->region,
											'amount' => $services->amount,
											'payment' => $services->payment
										]

						            ];	
							$notification_data = new SendBooking($insert);
						    Event::fire('send-booking', $notification_data);
						}

						$request->session()->forget('current_progress');
						$request->session()->forget('clean');
						$request->session()->forget('percent');
					}
					Helper::send_sms();
					$this->data['message_alert'] =  '<script>
						Swal.fire({
						position: "center",
						type: "success",
						title: "Book Successfully",
						showConfirmButton: false,
						timer: 3000
						})
						</script>
						';
					return view('frontend.page.booking-submitted.index',$this->data);
				}else{
					return redirect()->route('frontend.cleaning.index');
				}
			
			default:
				# code...
				break;
		}

		return redirect()->route("frontend.cleaning.index");
	
	} 



	public function revert (Request $request){
		
		$current_progress = $request->session()->get('current_progress');
		if ($current_progress > 1) {
			session()->put('current_progress',$current_progress - 1);
			
		}else{
			session()->put('current_progress', 1);
		}

		return redirect()->route("frontend.cleaning.index",$this->data);
	}


	public function cancel(BookingRequest $request)
	{
		$type_amount = 0;
		
        if($request->session()->get('clean.type') == "regular" && $request->session()->get('clean.property_type') == "home")
        {
            $type_amount = 90;
        }
        if($request->session()->get('clean.type') == "once-off" && $request->session()->get('clean.property_type') == "home")
        {
            $type_amount = 250;
        }
        if($request->session()->get('clean.type') == "lease" && $request->session()->get('clean.property_type') == "home")
        {
            $type_amount = 350;
        }

        

        if($request->session()->get('clean.type') == "regular" && $request->session()->get('clean.property_type') == "office")
        {
            $type_amount = 100;
        }
        if($request->session()->get('clean.type') == "once-off" && $request->session()->get('clean.property_type') == "office")
        {
            $type_amount = 250;
        }
        if($request->session()->get('clean.type') == "lease" && $request->session()->get('clean.property_type') == "office")
        {
            $type_amount = 350;
        }

        $total = $type_amount + $request->session()->get('clean.svc_total');

        if(!$request->session()->has('clean.postcode')){
			return redirect()->route('frontend.cleaning.index');
		}

		$request->session()->put('clean.payment', $request->payment);
		$services =  new CleaningService;
		$services->customer_id =  Auth::check() ? auth()->user()->id : "";
		$services->postcode = $request->session()->get('clean.postcode');
		$services->location = $request->session()->get('clean.location');
		$services->service_type = $request->session()->get('clean.type');
		$services->service_type_amount = $type_amount;
		$services->property_type = $request->session()->get('clean.property_type');
		$services->services =  implode(",",$request->session()->get('clean.svc_name'));
	  	$services->name =$request->get('fname');
		$services->lastname = $request->get('lname');
		$services->email = $request->get('email');
		$services->contact = $request->get('contact_number');
		$services->booking_id = $request->session()->get('booking_info.booking_id');
		$services->booking_date = Carbon::parse($request->session()->get('clean.booking_date'));
		$services->time_start =  $request->session()->get('clean.time_start');
		$services->time_end =  $request->session()->get('clean.time_end');
		$services->payment =  $request->session()->get('clean.payment');
		$services->longitude =  $request->session()->get('clean.longitude');
		$services->latitude =  $request->session()->get('clean.latitude');
		$services->region =  $request->session()->get('clean.state');
		$services->amount =   $request->session()->get('clean.svc_total');
		$services->booking_total =  $total;
		$services->status =   "cancelled";
		 $services->type =   "cleaning";

		if ($services->save()) {
			$request->session()->forget('current_progress');
			$request->session()->forget('clean');
			$request->session()->forget('percent');
		}
			// $request->session()->put('current_step',9);
		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "Thank you for leaving your details. Our local Big Boss Expert will call you in a while.",
			showConfirmButton: false,
			timer: 6000
			})
			</script>
			<style>
			.swal2-title {
			font-size:20px !important;
		}
			</style>';

	
		$this->data["message"] = "Your local Big Boss Cleaning expert will be in touch with you shortly.";
		return view('frontend.page.cancel-booking.index',$this->data);
	}

	public function last_cancel(BookingRequest $request)
	{

        if(!$request->session()->has('clean.postcode')){
			return redirect()->route('frontend.cleaning.index');
		}
		$request->session()->put('clean.payment', $request->payment);

		$services =  new CleaningService;
		$services->customer_id =  Auth::check() ? auth()->user()->id : "";
		$services->postcode = $request->session()->get('clean.postcode');
		$services->location = $request->session()->get('clean.location');
		$services->service_type = $request->session()->get('clean.type');
		$services->service_type_amount = $request->session()->get('clean.service_type_amount');
		$services->property_type = $request->session()->get('clean.property_type');
		$services->services =  implode(",",$request->session()->get('clean.svc_name'));
	  	$services->name = $request->session()->get('clean.fname');
		$services->lastname = $request->session()->get('clean.lname');
		$services->email = $request->session()->get('clean.email');
		$services->contact = $request->session()->get('clean.contact');
		$services->booking_id = $request->session()->get('booking_info.booking_id');
		$services->booking_date = Carbon::parse($request->session()->get('clean.booking_date'));
		$services->time_start =  $request->session()->get('clean.time_start');
		$services->time_end =  $request->session()->get('clean.time_end');
		$services->payment =  $request->session()->get('clean.payment');
		$services->longitude =  $request->session()->get('clean.longitude');
		$services->latitude =  $request->session()->get('clean.latitude');
		$services->region =  $request->session()->get('clean.state');
		$services->amount =   $request->session()->get('clean.svc_total');
		$services->booking_total =  $request->session()->get('clean.svc_total');
		$services->status =   "cancelled";
		 $services->type =   "cleaning";

		if ($services->save()) {
			$request->session()->forget('current_progress');
			$request->session()->forget('clean');
			$request->session()->forget('percent');
		}
			// $request->session()->put('current_step',9);
		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "Thank you for leaving your details. Our local Big Boss Expert will call you in a while.",
			showConfirmButton: false,
			timer: 6000
			})
			</script>
			<style>
			.swal2-title {
			font-size:20px !important;
		}
			</style>';

		$this->data["message"] = "Your local Big Boss Cleaning expert will be in touch with you shortly.";
		return view('frontend.page.cancel-booking.index',$this->data);
	}


	public function destroy($value='')
	{
		$request->session()->flush();
		return redirect()->route('frontend.cleaning.index');
	}

}



