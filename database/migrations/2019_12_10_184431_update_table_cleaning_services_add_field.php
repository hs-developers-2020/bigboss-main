<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCleaningServicesAddField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cleaning_services', function($table){
            
            $table->text('extras_id')->nullable();
            $table->text('other_services')->nullable();
            $table->text('other_services_price')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cleaning_services', function($table){
            $table->dropColumn(['extras_id','other_services','other_services_price']);
        });
    }
}
