@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
{{ Session::put('percent',50) }}
<form action="{{ route('frontend.infotech.six') }}" onsubmit="return validateForm()" method="post">
  {{ csrf_field() }}
<div class="container" style="padding-top: 153px;padding-bottom:7%;">
   <div class="row">
       <div class="col-12 text-center text-xs-center text-md-right text-lg-right text-right">
          <p class="text-color-white font-semibold text-uppercase spacing-1">Service Quote for: {{ Session::get('infotech.postcode') }},  {{ Session::get('infotech.sub') }}</p>
      </div>
      <div class="col-lg-12 text-center">
         <p class="text-white font-semibold spacing-1 mt-3">EXTRAS (You can choose more than one)</p>
      </div>
    </div>


  <div class="row">
    @foreach($addons_svc as $key => $subname)
      <div class="col-lg-2 col-6 text-center mt-3 ">
        <div class="styled-radio mt-2 mb-2">
          
          <input type="checkbox" class=""  id="{{$subname->id}}" name="svc_extras[]" value="{{$subname->id}}">
            <label for="{{$subname->id}}" class="text-xxsmall spacing-1 pt-4 pb-4" style="color: white;">
           <!--  <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/landing page.svg') }}" class="img-fluid"><br> -->
            <span class="mobile-text">{{Str::limit($subname->name)}}</span></label>
        </div>
      </div>
    @endforeach  
  </div>
</div>
        <div class="row">
          <div class="col-lg-12 mt-2 text-center">
              <button  type="submit"  class="next btn font-semibold card-shadow spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 " href="#"style="color: white !important;">Continue</button>
          </div>
        </div>
   </div>
</form>
@include('frontend._components.footer-main')
@stop
@section('scripts-content')
<script>
// var totalQuestions = 8;
// var currentQuestion = 0;
// var $progressbar = $("#progressbar");

// $(".next").on("click", function(){
//   if (currentQuestion >= totalQuestions){ return; }
//   currentQuestion++;
//   $progressbar.css("width", Math.round(100 * currentQuestion / totalQuestions) + "%");
// });

function validateForm() {
    var radios = document.getElementsByName("svc_extras[]");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked) formValid = true;
        i++;        
    }

    if (!formValid)  Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Please Select Service Package!',
  // footer: '<a href></a>'
});
    return formValid;
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 

@media only screen and (max-width: 1500px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
 .footer-bottom{
margin-top: 100px;
  
}
}

@media only screen and (min-width: 1500px) {

   #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
  }
  .picture-size {
  height:5%; width: 7%;
  }
  .footer-bottom{
  position: absolute;
  bottom:0;

}
  }

 .hidden {
  display: none ;
}
.show {
  display: block ;
}

   @media only screen and (max-width: 992px) {
.mobile-text {
  font-size: 20px;
}
}
   @media only screen and (min-width: 992px) {
.mobile-text--HR {
  font-size: 13px;
}
}

 
</style>
@stop