@extends('frontend._layout.main')

@section('content')
@include('frontend._components.topnav-main')
<div class="video-container-subpage" style="height: auto; padding-top: 70px;">
  <video loop muted autoplay  width="1280" height="1820">
    <source src="{{ asset('frontend/Video/main1.mp4') }}" type="video/mp4">
  </video>

 <div class="container mt-5">
     <div class="row ">
       <div class="col-lg-12">
          <div class="d-flex flex-row text-color-white pointer">
            <a class="d-flex pt-2 flex-row text-color-white spacing-1" style="text-decoration: none;" href="{{route('frontend.main')}}">
            <i class="fas fa-caret-left mt-1 mr-2"></i><p class="text-color-white font-semibold">GO BACK</p></a>
          </div>
      </div>
      <div class="col-lg-12 text-center pt-1">
        <h2 class="text-color-white text-uppercase text-header font-weight-bold text-headerspacing-2">IT SERVICES</h2>
      </div> 
      <div class="col-lg-12">
        <div class="row"> 
          <div class="col-lg-1"></div>
          <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 text-center pt-4 pb-2">
             <div class="card card-1" style="background-color: transparent;">
              <div class="card-body" style="background-color: rgba(0,0,0,.5);">
              <img src="{{ asset('frontend/Logos/Infotech.png')}}" class=" img-fluid  p-3 rounded" style="width:60% !important;">
              <p class="text-xsmall text-color-white text-justify font-semibold text-limit-5">We offer various services about Web Development and Internet Marketing.<br><br><br><br>

            </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 text-center pt-4">
            <div class="card rounded card-1" style="background-color: transparent;">
              <div class="card-body" style="background-color: rgba(0,0,0,.5);">
              <img src="{{ asset('frontend/images/it.png')}}" class=" img-fluid p-3 rounded" style="width:100% !important;">
              <p class="text-xsmall text-color-white text-justify font-semibold text-limit-5">Highly Succeed is a Philippines-based IT servicing and product company that specializes on a wide range of web based services. We are composed of professionals in graphic design, web development, custom application creation, and mobile application.</p>
            </div>
            </div>
          </div>
          <div class="col-lg-1"></div>
        </div>
    </div>
      <div class="col-lg-12 text-center mt-3">
        <a href="https://www.highlysucceed.com/" class="btn bg-color-blue text-white font-semibold pt-3 pb-3" target="_blank" style="padding-left: 60px;padding-right: 60px;">BOOK NOW</a>
      </div>
    </div>
</div>

@stop

@section('style-script')
<style type="text/css">
  
  .bg-color-white {
    background-color: white !important;
  }

@media only screen and (max-width: 1000px) {
  .desktop-margin {
    padding-top: 30px !important;
    width: 90% !important;

  }
  .image-logo {
    width: 40% !important;
  }
  .padding-container {
    padding-left: 1rem !important;
    padding-right: 1rem !important;
  }

  .icon-size {
      font-size: 1em !important;
    } 
  .mobile-padding {
    padding-bottom: 20px;
  }
    .video-hidden {
    display: none;
  }
}
@media only screen and (min-width: 1000px) {
  .video-container-subpage {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;

}
.video-container-subpage video {
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: 1400px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
}

@media only screen and (max-width: 1000px) {
    .video-container-subpage {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    overflow-x: hidden;
  }
.video-container-subpage video {
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: 1400px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
}
@media only screen and (min-width: 1000px) {

  .desktop-margin {
    padding-top: 60px !important;
    width: 40% !important;
  }
  .image-logo {
    width: 25% !important;
  }

    .padding-container {
    padding-left: 3rem !important;
    padding-right: 3rem !important;
  }
    .icon-size {
      font-size: 2em !important;
    }
  }

</style>
@stop
