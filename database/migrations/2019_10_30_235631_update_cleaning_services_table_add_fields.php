<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCleaningServicesTableAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('cleaning_services', function($table){
            $table->string('unit')->nullable()->after('services');
            $table->string('number_of_rooms')->nullable()->after('services');
            $table->text('standard_services')->nullable()->after('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('cleaning_services', function($table){
            $table->dropColumn(['unit', 'number_of_rooms', 'standard_services']);
        });
    }
}
