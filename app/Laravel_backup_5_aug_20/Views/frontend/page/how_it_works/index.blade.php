@extends('frontend._layout.main')
@section('style-script')
<style type="text/css">
body {
background-color: #F4F4F4 !important;
}

@media only screen and (max-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center; 
    background-size: cover; \
    background-position: center;
}
.picture-size {
height: 15%; width: 30%;
}

}

@media only screen and (min-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center; 
    background-size: cover; 
     background-position: center;
}
.picture-size {
height:5%; width: 7%;
}

  }
  a:hover{
    color: white !important;
  }
</style>
@stop
@section('content')

@include('frontend._components.topnav-main')

<div class="container mt-4 text-center" id="gradient-background"  style="margin-top: 170px !important; padding:100px;">
    <h3 class="text-white text-uppercase">How it works</h3>
</div>

<div class="container mt-2" style="background-color: white; ">         
    <div class="content">
      <div class="">
        <div class="card p-4" style="border: 0px solid rgba(0,0,0,.125);">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row">
                  <div class="col-lg-12 pl-5 pr-5 pt-5" style="background-color: white;">
                    <div class="row">
                      <p class="text-color-gray text-xsmall text-justify">
                        <span class="font-semibold text-small"> How are we better</span>
                    <br><br>

                    <strong>Our services</strong> – wide range of asset maintenance services 
                      <br><br>
                    We cater a wide range of asset maintenance services like residential cleaning, commercial offices and business buildings cleaning, real estate jobs, aged care home services, builder’s clean, and national housing cleaning jobs. 
                      <br><br>
                    For several years, we are once part of a cleaning franchise giant in Australia that honed our skills and knowledge about professional cleaning. Through those years we have exploit the cleaning industry not only in terms of the usual internal and external cleaning but have also gained specialized skills and expertise in other services like lawn mowing, carpet cleaning, windows cleaning, stripping and polishing floors, pressure washing and rubbish removal. 
                      </p>
                      
                    </div>
                  </div>         
             </div>
          </div>
        </div>  
   </div>
  </div>
    </div>

@include('frontend._components.footer')


@stop