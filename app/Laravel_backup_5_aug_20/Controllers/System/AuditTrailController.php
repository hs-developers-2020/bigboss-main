<?php

namespace App\Laravel\Controllers\System;
use App\Laravel\Models\AuditTrail;

use App\Laravel\Services\ResponseManager;

use Helper, Carbon, Session, Str, Auth;
class AuditTrailController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		
	}

  	public function index(){
  		if (Auth::user()->type == "franchisee") {
			$this->data['logs'] = AuditTrail::where("user_id" ,Auth::user()->id)->orderBy('created_at',"DESC")->paginate(10);
		}elseif (Auth::user()->type == "area_head") {
			$this->data['logs'] = AuditTrail::where("user_id" ,Auth::user()->id)->orderBy('created_at',"DESC")->paginate(10);
		}
		else{
			$this->data['logs'] = AuditTrail::orderBy('created_at',"DESC")->paginate(10);
		}

		
		return view ('system.audit-trail.index',$this->data);
	}

	



}
