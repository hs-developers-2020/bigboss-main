@extends('frontend._layout.main')
@section('content')
@include('frontend._components.topnav-main')
<form action="" method="POST">
    {{ csrf_field() }}
    <input type="hidden" value="{{ $booking->booking_id }}" name="booking_id">
    <input type="hidden" value="{{ $booking->franchisee_id }}" name="franchisee_id">
    <div class="container" style="background-color: white; margin-top: 150px !important;">   
      
      <div class="row">
          <div class="col-lg-12" style="padding-top: 10px;">
            @include('frontend._components.notifications')    
          </div>
          <div class="col-lg-6 pl-3 pr-5 pt-5 pb-3">
            <div id="" class=" ml-4">
              <h4 class="text-color-blue text-uppercase text-xmedium">Booking <span class="text-color-red">Finished</span></h4>
              <p class="text-color-blue pt-2 font-semibold text-medium text-uppercase spacing-1">Congratulations!</p>
              <p class="text-color-gray text-xsmall font-semibold">Thank you for choosing Big Boss Cleaning.</p>

              @if($feedback)
                <div class="rate mr-2 mt-3">
                  @for($i = 0; $i < $feedback->rate; $i++)
                    <i class="fas fa-star fa-2x mr-2 text-color--yellow"></i>
                  @endfor
                  @for($i = 0; $i < (5 - $feedback->rate); $i++)
                  <i class="fas fa-star fa-2x mr-2 text-color--gray"></i>
                  <i class="fas fa-star fa-2x mr-2 text-color--gray"></i>
                  @endfor
                </div> 
              @else
                <div class="rate mr-2">
                  <input type="radio" id="star5" name="rate" value="5" />
                  <label for="star5" title="text">5 stars</label>
                  <input type="radio" id="star4" name="rate" value="4" />
                  <label for="star4" title="text">4 stars</label>
                  <input type="radio" id="star3" name="rate" value="3" />
                  <label for="star3" title="text">3 stars</label>
                  <input type="radio" id="star2" name="rate" value="2" />
                  <label for="star2" title="text">2 stars</label>
                  <input type="radio" name="rate" value="1" if="star1"/>
                  <label for="star1" title="text">1 star</label>
                </div>
              @endif
                <p class="text-color-light text-xsmall pt-4 font-semibold"> </p> 
                <p class="text-color-gray pt-5 text-justify">We constantly seek feedback to improve our service, to help
                us achieve this, please provide a few comments on your cleaning expert and overall cleaning experience.</p>
                <textarea class="w-100 mt-1 p-3 pb-3" rows="6" placeholder="Provide Feedback Here" name="feedback" {{ $feedback ? 'disabled' : '' }}>{{ old('feedback', $feedback ? $feedback->feedback : '') }}</textarea>
            </div>
          </div>  
          <div class="col-lg-6 p-5" id="gradient-background">
            <div class="row">
                <div class="col-lg-12 text-left">
                    <p class="text-color-white text-small font-semibold text-uppercase ">Booking ID : {{ $booking->booking_id }}</p>
                </div>     
                @if($booking->staff)     
                <div class="col-lg-12">
                    <p class="text-color-white text-uppercase spacing-1 text-xsmall"><i class="fas fa-user text-color-white mr-2"></i>Booking Personnel</p>
                </div>    
                <div class="col-lg-12">
                    <div class="d-flex flex-row">
                      <img src="{{ asset('frontend/images/user.jpg') }}" class="rounded-circle mr-3 mt-2 picture-size">
                        <p class="text-xsmall text-color-white mt-3 pt-2 font-weight-bold ml-2 spacing-1 text-uppercase">{{ $booking->staff ? $booking->staff->name : '' }}<br>
                          <span class="font-semibold text-uppercase text-color-white text-xxsmall text-uppercase">{{ $booking->staff->info ? $booking->staff->info->business_name : '' }}</span>
                        </p>
                    </div>
                </div>
                @endif
                <div class="col-lg-12 mt-4">
                  <p class="text-color-white text-uppercase spacing-1 text-xsmall"><i class="fas fa-user text-color-white mr-2"></i>Service Details</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left mt-2">
                  <p class="text-color-white text-xsmall font-semibold text-uppercase">Service Type</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left mt-2">
                  <p class="text-color-white text-xsmall font-semibold text-uppercase">{{ Str::title($booking->property_type) }} | {{ Str::title($booking->service_type) }}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left mt-2">
                  <p class="text-color-white text-xsmall font-semibold text-uppercase">Email Address</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left mt-2">
                  <p class="text-color-white text-xsmall font-semibold text-uppercase">{{ $booking->email }}</p>
                </div>                 
                <div class="col-lg-12 mt-4">
                  <p class="text-color-white text-uppercase spacing-1 text-xsmall"><i class="fas fa-user text-color-white mr-2"></i>Payment Details</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left">
                  <p class="text-color-white font-semibold text-xsmall text-uppercase mt-2">Payment Method</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left">
                  <p class="text-color-white text-xsmall text-uppercase font-semibold mt-2">{{ $booking->payment }}</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left mt-2">
                  <p class="text-color-white font-semibold text-xsmall text-uppercase">Payment Status</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-sm-left mt-2">
                  <p class="text-color-white font-semibold text-xsmall text-uppercase">Paid</p>
                </div>                        
            </div>
          </div>        
      </div>        
    </div>
    @if(!$feedback)
    <div class="container">
       <div class="row">
         <div class="col-lg-12 mt-3">
                <button type="submit" class="btn bg-color-blue text-color-white pl-5 pr-5 pt-3 pb-3">SUBMIT</button>
          </div>
       </div>
    </div>
    @endif
</form>
@include('frontend._components.footer')
@stop

 

@section('style-script')
<style type="text/css">

.text-color--yellow {
  color:#ffc700;
}
.text-color--gray {
  color: #8b9aab;
}
body {
background-color: #F4F4F4 !important;
}

 

.rate {
    float: left;
    height: 46px;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:50px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
@media only screen and (max-width: 1000px) {

 

 #gradient-background {
    /*width: 100%;
    padding-top: 70px; 
    height: 1000px; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
    background-color: #2047A4;
}

 

.picture-size {
height: 15%; width: 30%;
}
.input-size {
  width: 90% !important;
}
}

 

@media only screen and (min-width: 1000px) {

 

 #gradient-background {
   /* width: 100%;
    padding-top: 70px; 
    height: 600px; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
     background-color: #2047A4;
}
.picture-size {
height:5%; width: 7%;
}
.input-size {
  width: 30% !important;
}
  }
</style>
@stop