<?php

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Finance;
use App\Laravel\Models\Regions;
use App\Laravel\Models\User;



/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\FinanceRequest;


use App\Laravel\Events\AuditTrailActivity;
use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str,DB,Auth,AuditRequest,Event,PDF,Excel;

class FinanceController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"All Services",'cleaning' => "Cleaning","information_technology" => "Information Technology"];
		$this->data['finance_category'] = [ ''=>"Select Category",'transportation' => "Transportation","equipment" => "Equipment Technology"];
		$this->data['category_type'] = [ ''=>"Select type",'expense' => "Expense","income" => "Income"];
		$this->data['reg'] = ['' => "Choose Regions"] + Regions::pluck('name', 'name')->toArray();
		
	}

	public function index(PageRequest $request){

		$finance_chart = [];
		$StartDate = Carbon::now()->startOfWeek()->format('F d');
		$weekEndDate = Carbon::now()->endOfWeek()->format('F d-Y');
		$weekStartDate = Carbon::now()->startOfWeek();
		$start_date = $weekStartDate->subDay();


		if (Auth::user()->type == "area_head") {
			$finance_id = Auth::user()->id;
		}
		if (Auth::user()->type == "franchisee") {
			$finance_id = Auth::user()->head_id;
		}

		$this->data['business_name'] = null;


		if(in_array(Str::lower(Auth::user()->type), ['area_head',
                            'franchisee'])) {

			foreach(range(1,7) as $index => $value){
				$total = Finance::whereRaw("DATE(created_at) = '".$start_date->addDay()->format("Y-m-d")."'")->count();
				$expense = Finance::whereRaw("DATE(created_at) = '".$start_date->format("Y-m-d")."'")->where('type' , "Expense")->sum('amount');
				$income = Finance::whereRaw("DATE(created_at) = '".$start_date->format("Y-m-d")."'")->where('type' , "Income")->sum('amount');
				array_push($finance_chart, ['date' => $start_date->format("Y-m-d"),'weekly_finance' => $total, 'income' => $income , 'expense' => $expense]);
			}


			$total_income = Finance::whereBetween('created_at' , [Carbon::now()->startOfWeek() , Carbon::now()->endOfWeek()])->where('type',"Income")->sum('amount');
			$total_expense = Finance::whereBetween('created_at' , [Carbon::now()->startOfWeek() , Carbon::now()->endOfWeek()])->where('type',"Expense")->sum('amount');
		
			$top_expense = Finance::where('type' ,"Expense")->select("*")->selectRaw('SUM(finance.amount) as total')->orderBy('total' , "DESC")->groupBy("category")->take(2)->get();
			$top_income = Finance::where('type' ,"Income")->select("*")->selectRaw('SUM(finance.amount) as total')->orderBy('total' , "DESC")->groupBy("details")->take(2)->get();

			$this->data['finance'] = Finance::orderBy('created_at' , "DESC")->take(5)->get();
			$this->data['business_name'] = User::find($finance_id);


		}else{

			foreach(range(1,7) as $index => $value){
				$total = Finance::whereRaw("DATE(created_at) = '".$start_date->addDay()->format("Y-m-d")."'")->count();
				$expense = Finance::whereRaw("DATE(created_at) = '".$start_date->format("Y-m-d")."'")->where('type' , "Expense")->sum('amount');
				$income = Finance::whereRaw("DATE(created_at) = '".$start_date->format("Y-m-d")."'")->where('type' , "Income")->sum('amount');
				array_push($finance_chart, ['date' => $start_date->format("Y-m-d"),'weekly_finance' => $total, 'income' => $income , 'expense' => $expense]);
			}



			$total_income = Finance::whereBetween('created_at' , [Carbon::now()->startOfWeek() , Carbon::now()->endOfWeek()])->where('type',"Income")->sum('amount');
			$total_expense = Finance::whereBetween('created_at' , [Carbon::now()->startOfWeek() , Carbon::now()->endOfWeek()])->where('type',"Expense")->sum('amount');
		
			$top_expense = Finance::where('type' ,"Expense")->select("*")->selectRaw('SUM(finance.amount) as total')->orderBy('total' , "DESC")->groupBy("category")->take(2)->get();
			$top_income = Finance::where('type' ,"Income")->select("*")->selectRaw('SUM(finance.amount) as total')->orderBy('total' , "DESC")->groupBy("details")->take(2)->get();


			$this->data['finance'] = Finance::orderBy('created_at' , "DESC")->take(5)->get();

		}
		



			//dd($top_expense);

		// $weekly_finance = Finance::whereBetween('created_at',[$weekStartDate , $weekEndDate])->count();
		//dd($mentorships);

		// array_push($finance_chart, ['date' => $now->format("Y-m-d"),'weekly_finance' => $weekly_finance ]);

		

		
		$this->data['finance_chart'] = json_encode($finance_chart);
		$this->data['total_income'] = $total_income;
		$this->data['total_expense'] = $total_expense;
		$this->data['top_expense'] = $top_expense;
		$this->data['top_income'] = $top_income;
		$this->data['StartDate'] = $StartDate;
		$this->data['weekEndDate'] = $weekEndDate;
		return view ('system.finance.index',$this->data);
	}
	
	public function show(PageRequest $request){
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['type'] = $request->get('type',false);

		if (!$this->data['from']) {
			$from = Finance::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
			$this->data['to'] = Carbon::now()->format("Y-m-d");
		}

		//dd($this->data['from']);
		$this->data['finance'] = Finance::type($this->data['type'])->dateRange($this->data['from'],$this->data['to'])->orderBy('created_at' , "DESC")->paginate(10);
		return view ('system.finance.show',$this->data);
	}

	public function create(){
		return view ('system.finance.create',$this->data);
	}

	public function store(FinanceRequest $request){

		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
			try {

			$new_finance = new Finance;
			$new_finance->fill($request->all());
			$new_finance->type ="Expense";
			$new_finance->user_id = Auth::user()->id;
			
			

			if($new_finance->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FINANCE_CREATED", 'remarks' => Auth::user()->full_name." has successfully created {$new_finance->type} information. [{$new_finance->type}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);
				session()->flash('notification-status','success');
				Session::flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.finance.index');
			}
			session()->flash('notification-status','failed');
			Session::flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


		
	}

	public function print(PageRequest $request){

		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['type'] = $request->get('type',NULL);


		if (!$this->data['from']) {
			$from = Finance::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
			$this->data['to'] = Carbon::now()->format("Y-m-d");
		}

		$header = array_merge([
				"Transaction #", "Booking ID", "Type", "Amount", "Franchisee","Date" , "Added By"
			]);

		Excel::create("Exported Finance Data", function($excel) use($header){
				$excel->setTitle("Exported Finance Data")
			    		->setCreator('HSI')
			          	->setCompany('Highly Suceed Inc.')
			          	->setDescription('Exported Finance data.');
		$excel->sheet('Finance', function($sheet) use($header){
			    	$sheet->freezeFirstRow();

			    	$sheet->setOrientation('landscape');

			    	  
			       	$sheet->row(1, $header);

			       	$counter = 2;
		$finance = Finance::type($this->data['type'])
								->dateRange($this->data['from'],$this->data['to'])
								->orderBy('created_at',"DESC")
								->chunk(20000, function($chunk) use($sheet, $counter) {
							foreach ($chunk as $finance) {
								$data = [str_pad($finance->id, 6, "0", STR_PAD_LEFT) , $finance->booking_id ?: "N/A" ,$finance->type,$finance->amount,$finance->franchisee ? $finance->franchisee->full_name : "N/A",$finance->created_at,$finance->user ? $finance->user->full_name :"N/A"
									];

								$sheet->row($counter++, $data);
								}	
						});
				  });
			})->export('xls');

		

	}
}
