@extends('frontend._layout.main')
@section('content')
<div class="video-container" style="height: auto;">
  <video loop muted autoplay  width="1280" height="820">
    <source src="{{ asset('frontend/Video/main1.mp4') }}" type="video/mp4">
  </video>
</div>

 <div class="container d-flex justify-content-center" style="margin-top: 110px">
    <div class="row">
    	<form action="" method="POST">
    		{{ csrf_field() }}
	      	<div class="col-lg-12 text-center pt-sm-0 pt-lg-3 pt-md-3 pb-lg-5 pb-md-5 pb-sm-2 bg-color-white">
	       		<img src="{{ asset('frontend/Logos/MainLogo.png') }}" class="img-fluid image-logo">
	       		<div class="container padding-container">
		       		<p class="text-color-blue font-semibold spacing-1 text-xsmall text-uppercase">Login to your account</p>
					@include('frontend._components.notifications')
		       		<input type="text" class="form-control mt-2" placeholder="Username" name="username">
		      		<input type="password" class="form-control mt-3" placeholder="Password" name="password">
	       			
					<div class="row">
						<div class="col-lg-12 mt-2">
							<button type="submit" class="btn bg-color-blue text-color-white w-100 text-uppercase">Login</button>
						</div>
					</div>
					<div class="row">
		       			<div class="col-lg-12 col-12 text-left mt-2 mb-1 text-center">		
		  					<a href="#" data-toggle="modal" data-target="#addNotes" class="text-color-blue pointer">Forgot Password ?</a>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<hr>
							<p class="text-color-gray mt-4 text-xsmall mobile-padding">Don't have an account?
								<span class="font-semibold">
									<a class="text-color-blue" href="{{route('frontend.create')}}"> Register Here</a>
								</span>
							</p>
						</div>
					</div>
	      		</div>
	    	</div>
    	</form>
 	</div>
</div>
<form method="POST" action="{{ route('frontend.forgot') }}">
	{{ csrf_field() }}
	<div class="modal" role="dialog" id="addNotes">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title">Reset Password</h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		              <span aria-hidden="true">&times;</span>
		            </button>
		        </div>
			    <div class="modal-body">
			        <div class="text-color-gray mb-2 {{$errors->first('reset_email') ? 'has-error' : NULL}}">
				        <i class="fas fa-angle-double-right font-small text-color-blue"></i>
				        <span class="ml-2 text-xxsmall font-semibold text-uppercase">Reset your password
				        	<span class="text-color-red ml-1">*</span>
				        </span><br><br>
			        	<span>Enter your username or email address and we will send you a link to reset your password.</span>
			        	@include('frontend._components.notifications')
			        	<input type="text" name="reset_email" id="reset_email"  class="form-control mt-3">
				        @if($errors->first('reset_email'))
				        	<span class="help-block" style="color: red;font-size: 12px;">*{{$errors->first('reset_email')}}</span>
				        @endif
			        </div>
			    </div>
	          	<div class="modal-footer">
	          		<button type="submit" class="btn btn-primary">Reset Password</button>
	            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	          	</div>
	        </div>
	    </div>
	</div>
</form>

@stop



@section('style-script')
<style type="text/css">
	a:hover {
		text-decoration: none;

	}
	.bg-color-white {
		background-color: white !important;
	}

@media only screen and (max-width: 1000px) {
	.desktop-margin {
		padding-top: 30px !important;
		width: 90% !important;

	}
	.image-logo {
		width: 40% !important;
	}
	.padding-container {
		padding-left: 1rem !important;
		padding-right: 1rem !important;
	}

	.icon-size {
			font-size: 1em !important;
		}	
	.mobile-padding {
		padding-bottom: 20px;
	}
	.hidden-sm {
		display: none;
	}
	.hidden-lg {
		display: block;
	}
}

@media only screen and (min-width: 1000px) {
	.desktop-margin {
		padding-top: 60px !important;
		width: 40% !important;
	}
	.image-logo {
		width: 25% !important;
	}

		.padding-container {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}
		.icon-size {
			font-size: 2em !important;
		}
	.hidden-lg {
		display: none;
	}
	.hidden-sm {
		display: block;
	}
	}
</style>
@stop
@section('scripts')
<script type="text/javascript">
	@if ($errors->first('reset_email') or Session::get('error'))
    	$('#addNotes').modal('show');
	@endif
</script>

@stop