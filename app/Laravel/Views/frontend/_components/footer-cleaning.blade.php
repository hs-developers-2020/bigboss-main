    <div class="footer-bottom" style="width: 100%;margin-top: 25px;">
        <div style="background-color: rgba(0,0,0,.5);">
        <div class="container p-2">
       <form action="{{ route('frontend.cleaning.revert') }}" method="GET">
            {{ csrf_field() }}
          <div class="row">

            <div class="col-lg-6 col-md-6 pt-2">
              <div class="d-flex flex-row text-color-white pointer">
                <button type="submit" class="d-flex pt-2 flex-row text-color-white spacing-1" style="text-decoration: none; background-color: transparent; border-color: transparent;">  <i class="fas fa-caret-left mt-1 mr-2"></i><p class="text-color-white font-semibold">GO BACK</p></button>
              </div>
            </div>
           
            <div class="col-lg-6 col-md-6">
              <div class="row">
                <div class="col-4 pt-3">
                  <p class="text-color-white text-uppercase">Completed</p>
                </div>
                <div class="col-8">
              {{-- <div class="progress">
                  <div class="progress-bar bg-info" role="progressbar" style="" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div> --}}
                <div style="padding-top: 20px;">
                  <div class="progress">
                      <div id="progressbar" class="progress-bar progress-bar-striped active" style="width:{{ Session::get('percent') }}%"></div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
           </form>
        </div>
      </div>
        <footer style="background-color: white;">
          <div class="container pt-3">
          <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 pb-1 pt-2">
              <h4 class="text-xsmall text-lg-left text-md-left text-sm-center text-center text-xs-center">
              <script>document.write(new Date().getFullYear());</script> Big Boss Group. All rights reserved |<a class="no-decoration text-color-gray" href="{{route('frontend.terms')}}"> Terms of Use </a>| <a href="{{route('frontend.privacy')}}" class="no-decoration text-color-gray">Privacy Policy</a></h4>
            </div>
            <div class="col-lg-4 col-md-4 pb-2 col-sm-12 text-lg-right text-center text-md-right">
              <h4 class="  text-color-gray text-xsmall">Connect with us
                <a href="https://www.facebook.com/BigBossGroupAU/" class="ml-2 fab fa-facebook" style="text-decoration:none;font-size: 20px; color: #4C60A2 !important;" target="_blank"></a>
                <a href="https://www.instagram.com/BigBossGroupAU/" class="fab fa-instagram ml-2" style="text-decoration:none;font-size: 20px; color: #ED7EAC !important;" target="_blank"></a>
                <a href="https://twitter.com/BigBossGroupAU1" class="fab fa-twitter ml-2" style="text-decoration:none;font-size: 20px; color: #127CC2 !important;" target="_blank"></a>
{{--                 <i class="fas fa-phone-square ml-2" style="font-size: 20px; color: #9A171F;"></i>
 --}}            <a class="fas fa-envelope-square ml-2 mr-2" href="mailto:info@bigbossgroup.com.au?Subject" style="text-decoration:none;font-size: 20px; color: #9A171F !important;" target="_blank"></a>
              </h4>

            </div>
          </div>
      </div>
</footer>
</div>