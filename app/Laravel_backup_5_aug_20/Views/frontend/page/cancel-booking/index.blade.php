@extends('frontend._layout.main')

@section('content')

@include('frontend._components.topnav-main')

<div class="container mt-5 margin-top">

</div>

        <div class="container card-shadow  pr-3 mt-2" style="background-color: white; ">       
          <div class="row">
             <div class="col-lg-12">
            <div id="gradient-background" class=" ml-3">
              <h3 class="text-color-blue">Cancelled Booking</h3>
              <h3 class="text-color-red">{{ Session::get('booking_info.fname') }}</h3>
              <p class="text-color-gray pt-2 font-semibold">{{$message}}</p>
              <p class="text-color-gray font-semibold ">
            {{--     <span>Your Booking Reference number is </span> --}}
             {{--    <span class="badge badge-info text-small bg-color-blue text-uppercase">{{ Session::get('booking_info.booking_id') }}</span> --}}
                {{-- <a href="#" style="text-decoration: none;" class="text-color-red">Your Booking Reference number is {{ Session::get('booking_info.booking_id') }}</a> --}}
              </p>
              {{-- <div class="col-lg-12 text-left">
                <div class="">
                  <a href="{{route('frontend.book-approved')}}"  style="color:white !important;text-decoration: none;" class="bg-color-blue btn mt-3 text-xxsmall pl-5 pr-5 pt-3 pb-3 spacing-1">CONTINUE</a>
                </div>
              </div> --}}
            </div>
            </div>       
         </div> 
    </div>

    {!!$message_alert == "" ? :  $message_alert  !!}

    {{ Session::forget('current_step') }}
@include('frontend._components.footer')
@stop

@section('style-script')
<style type="text/css">
body {
background-color: #F4F4F4 !important;
}
a:hover {
  color: white;
}
  @media only screen and (min-width: 1200px) {
.footer-bottom{
  position: absolute;
  bottom:0;
}

  }
@media only screen and (max-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px;  
    height: 450px; 
    background: linear-gradient(-90deg,transparent,transparent, white, white), url("frontend/background/bbg_bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
.input-size {
  width: 50%;
}
.margin-top {
   margin-top: 130px !important;
  }
}

@media only screen and (min-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 450px; 
    background: linear-gradient(-90deg,transparent,transparent, white, white), url("{{ asset('frontend/background/bbg_bg.png') }}") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height:5%; width: 7%;
}
.input-size {
  width: 25%;
}
.margin-top {
   margin-top: 160px !important;
  }
}

</style>
@stop
@section('content')

@include('frontend._components.topnav-main')

<div class="container mt-5 margin-top">

</div>

        <div class="container  pr-3 mt-2" style="background-color: white; ">       
          <div class="row">
             <div class="col-lg-12">
            <div id="gradient-background" class=" ml-3">
              <h3 class="text-color-blue">Thank You</h3>
              <h3 class="text-color-red">{{ Session::get('booking_info.fname') }}</h3>
              <p class="text-color-gray pt-2 font-semibold">Our Cleaning expert will be in touch with your shortly.</p>
              <p class="text-color-gray font-semibold ">
                <span>Your Booking Reference number is </span><span class="badge badge-info">{{ Session::get('booking_info.booking_id') }}</span>
               
              </p>
              <div class="col-lg-12 text-left">
                <div class="">
                  <a href="{{route('frontend.main')}}"  style="color:white !important;text-decoration: none;" class="bg-color-blue btn mt-3 text-xxsmall pl-5 pr-5 pt-3 pb-3 spacing-1">Back to home</a>
                </div>
              </div>
            </div>
            </div>       
         </div> 
    </div>
@include('frontend._components.footer')

@stop

<script>
    load_alert();
  

  function load_alert(argument) {
 Swal.fire({
  position: 'top-end',
  type: 'success',
  title: 'Your work has been saved',
  showConfirmButton: false,
  timer: 1500
})
  }
</script>