<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth,Helper, Str;

class BusinessGroupFile extends Model
{
    use DateFormatterTrait;

    protected $table = "business_group_file";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
           'name', 'label','file_type','date_issue','date_expiry','reference','is_high_risk'
    ];
    
    public $timestamps = true;

    public function user() {
        return $this->belongsTo("App\Laravel\Models\User", "user_id" ,"id");
    }

    public function business_group() {
        return $this->belongsTo("App\Laravel\Models\BusinessGroup", "group_id" ,"user_id");
    }

    public function scopeKeyword($query,$keyword){
        if($keyword){
            $key = Str::lower($keyword);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("reference LIKE '%{$key}%'")
                          ->orWhereRaw("name LIKE '%{$key}%'");
                               
            });

        }
    }


}
