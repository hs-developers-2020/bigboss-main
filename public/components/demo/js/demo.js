// Cookie functions
var createCookie = function(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
var readCookie = function(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
var eraseCookie = function (name) {
	createCookie(name,"",-1);
}

var switchStyle = function(styleName) {
	$('link[rel*=style][title]').each(function(i) {
		this.disabled = true;
		if (this.getAttribute('title') == styleName) this.disabled = false;
	});
	createCookie('style', styleName, 365);
	console.log(readCookie('style'));
}

function demoHeaderHide() {
	$('#demoHeaderClose').on('click', function() {
		$(".demo-header").hide();
	});
}

function iframeDisallow() {
	top.location != self.location && (top.location = self.location.href);
}

$(document).ready(function() {
	var c = readCookie('style'),
	controlPanel = $('.control-panel');
	panelControlWidth = controlPanel.outerWidth();
	console.log(c);
	if (c) switchStyle(c);

	$('.color-switcher a').click(function() {
		switchStyle(this.getAttribute("data-rel"));
		$(".color-switcher div").removeClass("selected");
		$(this.getElementsByTagName('div')).addClass("selected");
		return false;
	});
	$('.control-panel > .panel-icon').click(function(){

		if(controlPanel.hasClass("active")) {
			panelControlLeft = (0 - panelControlWidth);
			controlPanel.animate({
					"left": panelControlLeft
				}, function(){
					controlPanel.toggleClass("active");
				}
			);						
		}
		else{
			controlPanel.animate({
				"left":"0px"
			}, function(){
				controlPanel.toggleClass("active");
			});			
		} 
	});
	demoHeaderHide();
	iframeDisallow();
});