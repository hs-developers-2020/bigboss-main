<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css" integrity="sha384-i1LQnF23gykqWXg6jxC2ZbCbUMxyw5gLZY6UiUS98LYV5unm8GWmfkIS6jqJfb4E" crossorigin="anonymous">	
<link href="https://fonts.googleapis.com/css?family=Lato|Montserrat|Muli|Open+Sans|Poppins&display=swap" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/index.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/slick-theme.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/slick.css')}}"/>
<link rel="stylesheet" type="text/css" href="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css" rel="stylesheet" />

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
@yield('style-script')
<style type="text/css">
  .bg-masked {
    background-color: rgba(0,0,0,.15) !important;
    width: 100%;height: 100%;position: absolute;top: 0;
  }
  .main-button--hover:hover {
    background-color: rgba(32, 71, 164,.9) !important;
    -webkit-box-shadow: 7px 7px 7px #212121;
    -moz-box-shadow:    7px 7px 7px #212121;
    box-shadow:         7px 7px 7px #212121;
  }
  .postcode--dropdown {
    max-height:  150px; font-size: 16px; overflow-y: scroll; background-color: white !important; padding: 8px;
  }
  .no-decoration {
    text-decoration: none !important;
  }
	.hide {
		display: none;
	}
  h1,h2,h3,h4,h5,p,span{
    font-family: 'Poppins', sans-serif !important;
  }
	@media only screen and (max-width: 1000px) {
  .picture-size {
  height: 15%; width: 30%;
  }
  .input-size {
  width: 90% !important;
  }
  .logo-size {
  width: 17%;
  }
  .image-size {
    height: 50%;
  }
  .text-mobile {
  font-size: 30px;
  }
}
.bg-gray--input {
  background-color: #95a5a6 !important;
}
@media only screen and (min-width: 992px) {
  .hidden-sm {
    display: none;
  }
}
@media only screen and (max-width: 993px) {
  .hidden-lg {
    display: none;
  }
  }
@media only screen and (min-width: 1000px) {


.picture-size {
height:15%; width: 17%;
}
.input-size {
  width: 30% !important;
}
.logo-size {
  width:9%;
  }
.text-mobile {
  font-size: 20px;
}
}
.text-limit-5 {
   overflow: hidden;
   text-overflow: ellipsis;
   display: -webkit-box;
   line-height: 25px;     /* fallback */
         /* fallback */
   -webkit-line-clamp: 5; /* number of lines to show */
   -webkit-box-orient: vertical;
}

.card-1 {
 box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
}
.custom-nav {
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
    z-index:999;
}
.card-shadow {
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
.card-shadow1 {
    -webkit-box-shadow: 2px 2px 2px #333;
    -moz-box-shadow:    2px 2px 2px #333;
    box-shadow:         2px 2px 2px #333;
}
.hidden {
  display: none !important; 
}
  .text-transparent {
    color: transparent !important; 
  }
  .text-none {
    display: none !important;
  }
/*DatePicker*/
* {
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
}



input:focus {outline: none;}
#ui-datepicker-div {
  display: none;
  background-color: #fff;
  box-shadow: 0 0.125rem 0.5rem rgba(0,0,0,0.1);
  margin-top: 0.25rem;
  border-radius: 0.5rem;
  padding: 0.5rem;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}
.ui-datepicker-calendar thead th {
  padding: 0.25rem 0;
  text-align: center;
  font-size: 0.75rem;
  font-weight: 400;
  color: #78909C;
}
.ui-datepicker-calendar tbody td {
  width: 2.5rem;
  text-align: center;
  padding: 0;
}
.ui-datepicker-calendar tbody td a {
  display: block;
  border-radius: 0.25rem;
  line-height: 2rem;
  transition: 0.3s all;
  color: #546E7A;
  font-size: 0.875rem;
  text-decoration: none;
}
.ui-datepicker-calendar tbody td a:hover {  
  background-color: #E0F2F1;
}
.ui-datepicker-calendar tbody td a.ui-state-active {
  background-color: #009688;
  color: white;
}
.ui-datepicker-header a.ui-corner-all {
  cursor: pointer;
  position: absolute;
  top: 0;
  width: 2rem;
  height: 2rem;
  margin: 0.5rem;
  border-radius: 0.25rem;
  transition: 0.3s all;
}
.ui-datepicker-header a.ui-corner-all:hover {
  background-color: #ECEFF1;
}
.ui-datepicker-header a.ui-datepicker-prev {  
  left: 0;  
  background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMyIgdmlld0JveD0iMCAwIDEzIDEzIj48cGF0aCBmaWxsPSIjNDI0NzcwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik03LjI4OCA2LjI5NkwzLjIwMiAyLjIxYS43MS43MSAwIDAgMSAuMDA3LS45OTljLjI4LS4yOC43MjUtLjI4Ljk5OS0uMDA3TDguODAzIDUuOGEuNjk1LjY5NSAwIDAgMSAuMjAyLjQ5Ni42OTUuNjk1IDAgMCAxLS4yMDIuNDk3bC00LjU5NSA0LjU5NWEuNzA0LjcwNCAwIDAgMS0xLS4wMDcuNzEuNzEgMCAwIDEtLjAwNi0uOTk5bDQuMDg2LTQuMDg2eiIvPjwvc3ZnPg==");
  background-repeat: no-repeat;
  background-size: 0.5rem;
  background-position: 50%;
  transform: rotate(180deg);
}
.ui-datepicker-header a.ui-datepicker-next {
  right: 0;
  background: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMyIgdmlld0JveD0iMCAwIDEzIDEzIj48cGF0aCBmaWxsPSIjNDI0NzcwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik03LjI4OCA2LjI5NkwzLjIwMiAyLjIxYS43MS43MSAwIDAgMSAuMDA3LS45OTljLjI4LS4yOC43MjUtLjI4Ljk5OS0uMDA3TDguODAzIDUuOGEuNjk1LjY5NSAwIDAgMSAuMjAyLjQ5Ni42OTUuNjk1IDAgMCAxLS4yMDIuNDk3bC00LjU5NSA0LjU5NWEuNzA0LjcwNCAwIDAgMS0xLS4wMDcuNzEuNzEgMCAwIDEtLjAwNi0uOTk5bDQuMDg2LTQuMDg2eiIvPjwvc3ZnPg==');
  background-repeat: no-repeat;
  background-size: 10px;
  background-position: 50%;
}
.ui-datepicker-header a>span {
  display: none;
}
.ui-datepicker-title {
  text-align: center;
  line-height: 2rem;
  margin-bottom: 0.25rem;
  font-size: 0.875rem;
  font-weight: 500;
  padding-bottom: 0.25rem;
}
.ui-datepicker-week-col {
  color: #78909C;
  font-weight: 400;
  font-size: 0.75rem;
}
.dropdown-item:hover {
  background-color: white !important;
  color:#9B3034 !important;
}


  .text-first--letter::first-letter {
    text-transform: uppercase;
  }

  .ui-timepicker-standard a {
    text-transform: uppercase;
    text-align: left !important
}
.ui-timepicker-standard .ui-state-hover {
  background-color: #1C3E8F !important;
}

.ui-datepicker-calendar tbody td {
  color: red !important
}
.panel-header {
  height: 5px;background-color: #2047A4;
}
.letter-height-1 {
line-height:35px
}
.text-first-letter::first-letter {
  text-transform:uppercase;
}
a:hover {
  color: #a1000a !important 
}
.border-dash-white {
  border-style: solid !important;
  border-radius: 10px
}
.styled-radio label {
      border: 2px solid #e2e6e7 !important
}
.img-payment--size {
  height: 80px; width: auto;
  margin-bottom: 10px !important;
}

.shadow {
  -moz-box-shadow:    1px 2px 1px 2px #333;
  -webkit-box-shadow: 1px 2px 1px 2px #333;
  box-shadow:         1px 2px 1px 2px #333;
}
</style>