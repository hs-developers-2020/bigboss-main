<!DOCTYPE html>
<html lang="en">
  
<head>
    @include('system._components.metas')
    @include('system._components.styles')
   
  </head>
  <body>
     
    @yield('content')
	
    @yield('page-modals')
    @include('system._components.footer')
    @include('system._components.scripts')

{{--     @include('system._components.footer')   --}} 
  </body>
</html>