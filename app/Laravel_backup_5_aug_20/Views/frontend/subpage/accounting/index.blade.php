@extends('frontend._layout.main')

@section('content')
@include('frontend._components.topnav-main')
<div class="video-container-subpage" style="height: auto; padding-top: 70px;">
  <video loop muted autoplay  width="1280" height="1820">
    <source src="{{ asset('frontend/Video/main1.mp4') }}" type="video/mp4">
  </video>

 <div class="container mt-5">
     <div class="row ">
       <div class="col-lg-12">
          <div class="d-flex flex-row text-color-white pointer">
            <a class="d-flex pt-2 flex-row text-color-white spacing-1" style="text-decoration: none;" href="{{route('frontend.main')}}">
            <i class="fas fa-caret-left mt-1 mr-2"></i><p class="text-color-white font-semibold">GO BACK</p></a>
          </div>
      </div>
      <div class="col-lg-12 text-center pt-1">
        <h2 class="text-color-white text-uppercase text-header font-weight-bold text-headerspacing-2">ACCOUNTING</h2>
          </div>
      <div class="col-lg-12">
        <div class="row"> 
          <div class="col-lg-1"></div>
          <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 text-center pt-4 pb-2">
             <div class="card rounded card-1" style="background-color: transparent;">
              <div class="card-body" style="background-color: rgba(0,0,0,.5);">
              <img src="{{ asset('frontend/Logos/Accountants.png')}}" class=" img-fluid w-75 p-3 rounded">
              <p class="text-xsmall text-color-white text-justify font-semibold text-limit-5">Backed by 20 years experience in public practice and in corporate world, we can offer our franchisors and franchisees in-depth knowledge and business management skills to grow their franchise. We have a reputation for having passion for customer-first service and our work reasonably priced.
                </p>

            </div>
            </div>
          </div>                    
          <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 text-center pt-4">
            <div class="card rounded card-1" style="background-color: transparent;">
              <div class="card-body" style="background-color: rgba(0,0,0,.5);">
              <img src="{{ asset('frontend/images/your_web_accountant1.png')}}" class=" img-fluid p-3 rounded" style="width: 72% !important;">
              <p class="text-xsmall text-color-white text-justify font-semibold text-limit-5">Your Webaccountant (YWA)  are a committed team of accredited public accountants, registered tax agents and SMSF auditors & advisers offering a wide range of services to the clients.  We are here to help you grow your business and to look after your business as if they were our own.

              We take away all your worries in all aspects of accounting, tax & SMSF matters thereby giving you real peace of mind in real time.

              We stand by the quality of service that we provide and we always aim to give you a 100% service satisfaction guarantee.

              Founded by Cherry Tomenio Dimapilis in 2011, the firm originally began as a sole practice servicing individuals to lodge their tax returns with the tax office. Backed by more than twenty years experience in professional accounting here in Australia and overseas, the clientele grew from only servicing individuals to small businesses and now also looking after self-managed superannuation funds. We are now a team of accountants, tax agents and SMSF auditors and advisers.

              As members of the Institute of Public Accountants (IPA), National Tax & Accountants Association (NTAA), and SMSF Advisers Network (SAN), you can be sure that we are equipped with professional & up-to-date accounting & taxation knowledge.</p>
            </div>
            </div>
          </div>
          <div class="col-lg-1"></div>
        </div>
    </div>
      <div class="col-lg-12 text-center mt-3">
        <a href="http://www.yourwebaccountant.com/" target="_blank"><button class="btn bg-color-blue text-white font-semibold pt-3 pb-3" style="padding-left: 60px;padding-right: 60px;">BOOK NOW</button></a>
      </div>
  </div>
</div>

@stop

@section('style-script')
<style type="text/css">
  
  .bg-color-white {
    background-color: white !important;
  }
@media only screen and (min-width: 1000px) {
  .video-container-subpage {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;

}
.video-container-subpage video {
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: 1400px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
}

@media only screen and (max-width: 1000px) {
    .video-container-subpage {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    overflow-x: hidden;
  }
.video-container-subpage video {
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: 1400px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
}
@media only screen and (max-width: 1000px) {
  .desktop-margin {
    padding-top: 30px !important;
    width: 90% !important;

  }
  .image-logo {
    width: 40% !important;
  }
  .padding-container {
    padding-left: 1rem !important;
    padding-right: 1rem !important;
  }

  .icon-size {
      font-size: 1em !important;
    } 
  .mobile-padding {
    padding-bottom: 20px;
  }
    .video-hidden {
    display: none;
  }
}

@media only screen and (min-width: 1000px) {

  .desktop-margin {
    padding-top: 60px !important;
    width: 40% !important;
  }
  .image-logo {
    width: 25% !important;
  }

    .padding-container {
    padding-left: 3rem !important;
    padding-right: 3rem !important;
  }
    .icon-size {
      font-size: 2em !important;
    }
  }

</style>
@stop