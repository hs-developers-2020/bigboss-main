<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminAccountSeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(PostcodeSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(SkipServiceSeeder::class);
        // $this->call(ServicePackageSeeder::class);
    }
}
