<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('applicant', function($table){
            $table->increments('id');
            $table->bigInteger('account_manager_id')->unsigned()->nullable();
            $table->string('firstname', 255)->nullable();
            $table->string('lastname', 255)->nullable();
            $table->string('contact', 100)->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('number_of_children')->nullable();
            $table->string('educational_attainment')->nullable();
            $table->string('ownership_of_house')->nullable();
            $table->string('length_of_stay')->nullable();
            $table->string('has_id')->nullable()->default('no');
            $table->longText('government_id')->nullable();
            $table->string('has_bank_account')->nullable();
            $table->longText('bank_name')->nullable();
            $table->string('has_outstanding_loan')->nullable();
            $table->longText('outstanding_loan')->nullable();
            $table->string('weekly_payment')->nullable(); 
            $table->string('years_in_business')->nullable();
            $table->string('store_size')->nullable();
            $table->string('ownership_of_store')->nullable();
            $table->string('average_monthy_sales')->nullable();
            $table->longText('others_business')->nullable();
            $table->longText('others')->nullable();
            $table->string('has_business_license')->nullable();
            $table->longText('business_license')->nullable();
            $table->string('has_online_payment')->nullable();
            $table->longText('online_payment_gateway')->nullable();
            $table->string('manual_process')->nullable();
            $table->string('midas_system')->nullable();
            $table->decimal('credit_score', 10, 2)->nullable();
            $table->string('status')->nullable()->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('applicant');
    }
}
