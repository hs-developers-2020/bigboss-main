@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')

{{-- <form action="" method="post" > --}}
<div class="myDivsTab pt-1">
  <div class="content" style="padding-bottom:7%;">
      <form action="" method="post">
        {{ csrf_field() }}
       <div class="container" style="padding-top: 155px;">
          <div class="row">
            
            <div class="col-lg-12 text-center">
            
               <p class="text-white font-semibold mt-3 text-uppercase">Enter your postcode or suburb to check <br>
                    which services are available</p>
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-6 offset-3">
              <div class="card">
                <div class="card-body p-5 pt-3 pb-4 text-center">
                      <p class="text-color-blue font-weight-bold text-uppercase">ENTER YOUR POSTCODE OR SUBURB</p>
                      <input type="text" id="postcode" name="postcode" class="form-control text-center" placeholder="" style="border-top: white;border-left:white;border-right: white;">

                      @if($errors->first('postcode'))
                      <span class="text-danger">{{$errors->first('postcode')}}</span>
                      @endif
                </div>
              </div>
            </div>
            
          <div id="continue" class="col-lg-12 text-center">
            <div class="">
              <a name="next" id="next"  class="nextone spacing-1" style="text-decoration: none;">
              <button type="submit" class="next btn text-color-white mt-3 font-semibold spacing-1 text-xsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #1F46A3;">CONTINUE</button></a>
            </div>
            
          </div>
       
         </div>
        </div>
      </form>
   </div>
  <div class="content"  style="padding-bottom:18%;">
    <div class="container" style="padding-top: 153px;">
       <div class="row">
            <div class="col-lg-6"></div>
            <div class="col-lg-6 text-right">
                <p class="text-color-white font-semibold text-uppercase spacing-1">Service Quote for: Armidale 2350</p>
            </div>
            <div class="col-lg-12 text-center">
            
               <p class="text-white spacing-1 mt-3 text-uppercase font-semibold">Pin your location</p>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-8 d-flex flex-row mt-2" id="continue">
                <input type="text" class="form-control p-3" placeholder="Selected Location" id="location">
            </div>
            <div class="col-lg-2 mt-2">
             <a name="next" onclick="myFunction()" id="next" class="next btn font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 w-100" href="#"style="color: white !important;">Continue</a>
           </div>
        </div>
    </div>
  </div>
  <div class="content"  style="padding-bottom:5.7%;">
    <div class="container" style="padding-top: 153px;">
       <div class="row">
          <div class="col-lg-6"></div>
          <div class="col-lg-6 text-right">
              <p class="text-color-white font-semibold spacing-1 text-uppercase">Service Quote for: Armidale 2350</p>
          </div>
          <div class="col-lg-12 text-center">  
             <p class="text-white text-uppercase font-semibold spacing-1 mt-3">Select your property type</p>
          </div>
          <div class="col-lg-4"></div>
          <div class="col-lg-2 col-5 text-center mt-3">
              <div class="styled-radio mt-2 mb-2 ">
                <input type="radio" id="home"  name="property-type" value="home">
                <label for="home" class="text-xxsmall text-uppercase pt-4 pb-4" style="color: white;">
                  <img src="{{ asset('frontend/button-icons/home selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Home
                </label>
              </div>
          </div>
          <div class="col-lg-2 col-6 text-center mt-3">
              <div class="styled-radio mt-2 mb-2 ">
                  <input type="radio" id="office"  name="property-type" value="office">
                  <label for="office" class="text-xxsmall text-uppercase pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/business selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Office</label>
              </div>
          </div>
          <div class="col-lg-4"></div>
          <div class="col-lg-12 mt-2 text-center">
              <a name="next"  id="next" class="next btn font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 " href="#"style="color: white !important;">Continue</a>
          </div>
        </div>
    </div>
  </div>
  <div class="content"  style="padding-bottom:5.7%;">
     <div class="container" style="padding-top: 153px;">
        <div class="row">
          <div class="col-lg-6"></div>
          <div class="col-lg-6 text-right">
              <p class="text-color-white font-semibold text-uppercase spacing-1">Service Quote for: Armidale 2350</p>
          </div>
          <div class="col-lg-12 text-center">   
             <p class="text-white font-semibold text-uppercase spacing-1 mt-3 ">Select service type</p>
          </div>
          <div class="col-lg-3"></div>
            <div class="col-lg-2 col-6 text-center mt-3">
                <div class="styled-radio mt-2 mb-2 ">
                  <input type="radio" id="regular" name="service-type">
                  <label for="regular" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/regular selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Regular</label>
                </div>
            </div>
            <div class="col-lg-2 col-6 text-center mt-3">
                <div class="styled-radio mt-2 mb-2 ">
                  <input type="radio" id="Once-Off1" name="service-type">
                  <label for="Once-Off1" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/once off selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Once-Off</label>
                  </div>
            </div>
            <div class="col-lg-2 col-6  text-center mt-3">
                <div class="styled-radio mt-2 mb-2">
                  <input type="radio" id="End-of-Lease" name="service-type">
                  <label for="End-of-Lease" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/end off lease selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>End of Lease</label>
                  </div>
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-12 mt-2 text-center">
                  <a name="next" onclick="myFunction()" id="next" class="next btn font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 " href="#"style="color: white !important;">Continue</a>
            </div>
        </div>
    </div>
  </div>
  <div class="content"  style="padding-bottom:4.3%;">
    <div class="container" style="padding-top: 153px;">
     <div class="row">
        <div class="col-lg-6">
          
      </div>
      <div class="col-lg-6 text-right">
          <p class="text-color-white font-semibold text-uppercase spacing-1">Service Quote for: Armidale 2350</p>
      </div>
      <div class="col-lg-12 text-center">
        
         <p class="text-white font-semibold text-uppercase spacing-1 mt-3">Other service (You can choose more than one)</p>
      </div>
      <div class="col-lg-1"></div>
        <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="checkbox" id="Oven" name="oven" value="oven">
                <label for="Oven" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/oven seleected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Oven Cleaning</label>
              </div>
            </div>
            <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="checkbox" id="Carpet" name="carpet" value="carpet">
                <label for="Carpet" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/carpet selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Carpet Cleaning</label>
              </div>
            </div>
            <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="checkbox" id="Window">
                <label for="Window" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/window selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Window Cleaning</label>
              </div>
            </div>
            <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="checkbox" id="Lawn">
                <label for="Lawn" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/lawn and gradening selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Lawn & Gardening</label>
              </div>
            </div>
            <div class="col-lg-2 col-6  text-center mt-3">
            <div class="styled-radio mt-2 mb-2">
              <input type="checkbox" id="rubbish">
                <label for="rubbish" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"> <img src="{{ asset('frontend/button-icons/rubbish removal selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Rubbish Removal</label>
              </div>
            </div>
            <div class="col-lg-1"></div>
             <div class="col-lg-12 mt-2 text-center">
                <a name="next"  id="next_continue" class="next btn font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 " href="#"style="color: white !important;">Continue</a>
            </div>
      </div>
   </div>
  </div>
   <div class="content"  style="padding-bottom:4.3%;">
     <div class="container" style="padding-top: 175px;">
        <div class="card">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row"  style="margin-left: 0px !important;margin-right: 0px !important;">
                  <div class="col-lg-7 p-5" style="">
                    <div class="row">
                      <div class="col-lg-12">
                        
                        <p class="text-color-blue font-semibold spacing-1 text-small">Booking <span class="text-color-red">Details</span></p>
                      </div>
                       <div class="col-lg-12">
                        <p class="text-color-gray font-semibold text-xsmall">User Details</p>
                      </div>
                      <div class="col-lg-6">
                         <div class="text-color-gray mb-2">
                          <i class="fas fa-angle-double-right text-color-blue"></i>
                          <span class="ml-2 text-xxsmall font-semibold text-color-gray">First Name</span>
                        </div>
                         <input type="text" placeholder="First Name" class="form-control text-xsmall" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="">
                      </div>
                      <div class="col-lg-6">
                          <div class="text-color-gray mb-2">
                          <i class="fas fa-angle-double-right text-color-blue"></i>
                          <span class="ml-2 text-xxsmall font-semibold text-color-gray">Last Name</span>
                        </div>
                         <input type="text" placeholder="Last Name" class="form-control text-xsmall" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="">
                      </div>
                      <div class="col-lg-6 offset-lg-6 mt-2">
                          <div class="text-color-gray mb-2">
                          <i class="fas fa-angle-double-right text-color-blue"></i>
                          <span class="ml-2 text-xxsmall font-semibold text-color-gray">Contact Number</span>
                        </div>
                         <div class="input-icons"> 
                          <i class="icon text-color-blue p-3" style="padding: 5px !important; padding-left: 10px !important;">
                            <p class="text-color-blue font-weight-bold" style="font-style: normal;font-size: 14px; margin-top: 2px;">+61 |</p></i>
                         <input type="text" placeholder="Contact number" class="input-field form-control text-xsmall" style="padding-left: 50px !important; padding-bottom: 8px !important; padding-top: 8px !important;" name="">
                       </div>
                      </div>
                      <div class="col-lg-6"></div>
                       <div class="col-lg-6 mt-2">
                         <div class="text-color-gray mb-2">
                          <i class="fas fa-angle-double-right text-color-blue"></i>
                          <span class="ml-2 text-xxsmall font-semibold text-color-gray">Email Address</span>
                        </div>
                         <input type="text" placeholder="Email Address" class="form-control text-xsmall" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="">
                      </div>
                      <div class="col-lg-6"></div>
          
                    </div>
                  </div>
                  <div class="col-lg-5 pr-4 pb-4" id="gradient-background" style="padding-top: 36px !important;">
                    <div class="row p-3">
                      <div class="col-lg-12 text-right">
                           <p class="text-uppercase text-small font-semibold spacing-2 text-color-white">BOOKING ID : 31231231</p>
                        </div>
                        <div class="col-lg-12 text-left">
                           <p class="text-small spacing-1 text-color-white">You Selected</p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 col-6">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/home selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Home</label>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 col-6">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/regular selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Regular</label>
                            </div>
                        </div>
                       <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 col-6">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/carpet selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Carpet Cleaning</label>
                            </div>
                        </div>
                    </div>
                  </div>        
             </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 mt-3">
            <span class="text-color-white font-semibold">Do you want to secure a spot now?</span>
          </div>
           <div class="col-lg-5">
                <a name="next" onclick="myFunction()" id="next" class="next btn font-semibold spacing-1 text-uppercase text-xxxsmall bg-color-blue text-color-white pl-5 pr-5 pt-3 pb-3 mt-3" href="#"style="color: white !important;">Yes, Give me a quote and secure booking now. </a>
            </div>
            <div class="col-lg-7">
                <div id="cancel" class="">
                    <a name="prev" class="spacing-1" style="text-decoration: none;"><button class="btn text-color-white mt-3 font-semibold spacing-1 text-uppercase text-xxxsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #972121;">No, Call me and discuss price and schedule</button></a>
                  </div>
              </div>     
      </div>
   </div>
  </div>
  <div class="content"  style="padding-bottom:4.3%;">
      <div class="container" style="padding-top: 153px;">
    <div class="row">
    
     <div class="col-lg-12 text-right">
         <p class="text-color-white font-semibold">Service Quote for: Armidale 2350</p>
     </div>
     <div class="col-lg-12 text-center">
        <p class="text-white font-semibold text-uppercase spacing-1 mt-3">Select your booking date</p>
     </div>
     <div class="col-lg-1"></div>
       <div class="col-lg-5 text-center mt-3">
           <div class="card p-4">
             <div class="card-body">
               <div class="row">
                 <div class="col-lg-12 text-left">
                   <p class="font-semibold text-uppercase text-xsmall text-color-blue">Select Booking Date</p>
                 </div>
                 <div class="col-lg-12">
                   <input type="date" class="form-control" name="">
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-uppercase  text-xxsmall">Time Start</label>
                   <input type="time" class="form-control" name="">
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-uppercase text-xxsmall">Time End</label>
                   <input type="time" class="form-control" name="">
                 </div>
               </div>
             </div>
           </div>
       </div>
      <div class="col-lg-5 text-center mt-3">
           <div class="card p-4">
             <div class="card-body">
               <div class="row">
                 <div class="col-lg-12 text-left">
                   <p class="font-semibold text-uppercase text-xsmall text-color-blue">Select Secondary Booking Date</p>
                 </div> 
                 <div class="col-lg-12">
                   <input type="date" class="form-control" name="">
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-uppercase text-xxsmall">Time Start</label>
                   <input type="time" class="form-control" name="">
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-uppercase  text-xxsmall">Time End</label>
                   <input type="time" class="form-control" name="">
                 </div>
               </div>
             </div>
           </div>
       </div>
       <div class="col-lg-1"></div>
           <div id="continue" class="col-lg-12 text-center mt-2">
             <div class=""><button class="btn text-color-white mt-3 text-xxsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #1F46A3;"><a name="next" class="spacing-1" style="text-decoration: none;">CONTINUE</a></button>
             </div>
           </div>
     </div>
  </div>
  </div>
  <div class="content">
     <div class="container pb-5" style="padding-top: 155px;">
        <div class="card">
          <div class="card-body" style="padding: 1px !important;">
            <div class="row" style="padding-left: 0px !important;padding-right: 0px !important;">
                  <div class="col-lg-6 p-5" style="">
                    <div class="row">
                      <div class="col-lg-12">
                        
                        <p class="text-color-gray font-semibold text-uppercase spacing-1 text-xsmall">Confirm Booking Summary</p>
                      </div>
                       <div class="col-lg-12">
                        <p class="text-color-gray font-weight-bold text-uppercase text-xsmall"><i class="fas fa-list-alt mr-2 text-color-blue"></i>Service Details</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase">Service Type</p>
                      </div>
                      <div class="col-lg-6">
                        <p  id="service_type"class="text-color-blue text-uppercase text-xsmall">House of Standard</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase">Service Package</p>
                      </div>
                      <div class="col-lg-6">
                       <p class="text-color-blue text-uppercase text-xsmall">One Off</p>
                      </div>
                      <div class="col-lg-12">
                        <p class="text-color-gray font-weight-bold text-uppercase text-xsmall"><i class="fas fa-calendar-alt text-color-blue mr-2"></i>Booking Time/Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase">Booking Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue text-uppercase text-xsmall">27/02/2019</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase">Secondary Booking Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue text-uppercase text-xsmall">27/02/2019</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase">Service Time</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue text-uppercase text-xsmall">10:00 am - 01:00 pm</p>
                      </div>
                       <div class="col-lg-12">
                        <p class="text-color-gray font-weight-bold text-uppercase text-xsmall"><i class="fas fa-map-marker-alt mr-2 text-color-blue"></i>Service Location</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase">Address</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue text-uppercase text-xsmall">Level 4/235 Macquarie St., Sydney, NSQ</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase">Post</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue text-uppercase text-xsmall">2000</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 p-5" id="gradient-background">
                      <div class="">
                          <div class="row">                    
                              <div class="col-lg-6">
                                  <p class="text-color-white font-weight-bold text-uppercase text-xsmall"><i class="fas fa-user text-color-white mr-2"></i>Customer Details</p>
                              </div>
                               <div class="col-lg-6 text-right">
                                  <p class="text-color-white font-semibold text-uppercase spacing-1">Booking ID : 878968552</p>
                              </div>
                              <div class="col-lg-12">
                                <div class="d-flex flex-row">
                                  <img src="{{ asset('frontend/images/user.jpg') }}" class="rounded-circle mr-3 mt-2 picture-size">
                                  <p class="text-xsmall text-color-white mt-3 pt-2 font-weight-bold ml-2 spacing-1">Emmet Brown<br>
                                    <span class="font-semibold text-uppercase text-color-white text-xxsmall">Da Cleaners</span>
                                  </p>
                                </div>
                              </div>
                              <div class="col-lg-6 mt-3">
                                  <p class="text-color-white font-semibold text-xsmall text-uppercase">Contact Number</p>
                              </div>
                              <div class="col-lg-6 text-right mt-3">
                                  <p class="text-color-white font-semibold text-xsmall text-uppercase">(+61) 0416 001 630</p>
                              </div>
                              <div class="col-lg-6">
                                  <p class="text-color-white font-semibold text-xsmall text-uppercase">Email Address</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                  <p class="text-color-white text-uppercase text-xsmall">john.gallows@gmail.com</p>
                              </div>
                              <div class="col-lg-12">
                                  <p class="text-color-white text-xsmall font-weight-bold text-uppercase"><i class="fas fa-user text-color-white mr-2"></i>Payment Summary</p>
                              </div>
                              <div class="col-lg-12">
                                  <p class="text-color-white text-xsmall font-semibold text-uppercase mt-2"><i class="fas fa-user text-color-white mr-2"></i>Payment Details</p>
                              </div>
                              <div class="col-lg-6">
                                  <p class="text-color-white font-semibold text-xsmall text-uppercase">Service Type</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                  <p class="text-color-white text-uppercase">$ 5.00</p>
                              </div>
                              <div class="col-lg-6">
                                  <p class="text-color-white font-semibold text-xsmall text-uppercase">Service Package</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                  <p class="text-color-white text-uppercase">$ 30.00</p>
                              </div>
                              <div class="col-lg-12">
                                  <hr class="text-color-white" style="height: 2px; background-color: white;">
                              </div>
                              <div class="col-lg-6">
                                  <p class="text-color-white font-semibold text-uppercase text-small">Estimated Total</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                  <p class="text-color-white text-uppercase text-small">$ 35.00</p>
                              </div>
                            </div>
                        </div>
                  </div>      
             </div>
          </div>
        </div>
        <div class="row">
           <div class="col-lg-2">
                <a name="next" onclick="myFunction()" id="next" class="next btn font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pl-5 pr-5 pt-3 pb-3 mt-3" href="#"style="color: white !important;">Continue</a>
            </div>
            <div class="col-lg-2">
              <div id="cancel" class="">
                <div class="">
                  <a name="prev" class="spacing-1" style="text-decoration: none;"><button class="btn text-color-white mt-3 font-semibold spacing-1 text-uppercase text-xsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #972121;">CANCEL</button></a>
                </div>
              </div>
            </div>     
        </div>
    </div>
  </div>

   <div class="content" style="padding-bottom:5.8%;">
        <div class="container" style="padding-top: 155px;">
           <div class="row">
              <div class="col-lg-6"></div>
              <div class="col-lg-6 text-right">
                  <p class="text-color-white font-semibold text-uppercase spacing-1">Service Quote for: Armidale 2350</p>
              </div>
              <div class="col-lg-12 text-center">
               
                 <p class="text-white font-semibold text-uppercase spacing-1 mt-3">Select payment method</p>
              </div>
              <div class="col-lg-3"></div>
              <div class="col-lg-2 col-6 text-center mt-3">
                  <div class="styled-radio mt-2 mb-2 ">
                    <input type="radio" id="cod" name="payment" data-toggle="modal" data-target=".bd-example-modal-xl">
                      <label for="cod" class="text-xxsmall font-weight-bold text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/cash on service selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Cash</label>
                    </div>
                  </div>
                  
                  <div class="col-lg-2 col-6 text-center mt-3">
                  <div class="styled-radio mt-2 mb-2 ">
                    <input type="radio" id="visa" name="payment" data-toggle="modal" data-target=".bd-example-modal-xl">
                      <label for="visa" class="text-xxsmall font-weight-bold text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/visa selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Visa</label>
                    </div>
                  </div>
                  <div class="col-lg-2 col-6  text-center mt-3">
                  <div class="styled-radio mt-2 mb-2">
                    <input type="radio" id="mastercard" name="payment" data-toggle="modal" data-target=".bd-example-modal-xl">
                      <label for="mastercard" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"> <img src="{{ asset('frontend/button-icons/mastercard selected.svg') }}" class="img-fluid pl-4 pr-4"> <br>Mastercard</label>
                    </div>
                  </div>

                  <div class="col-lg-3"></div>
                  <div class="col-lg-12 text-center">
                    <p class="text-color-white mt-3 font-semibold text-xsmall spacing-1 text-uppercase">The selected payment method is for information only. Actual price may vary depending on the actual site visit.</p>
                  </div>
                  <div class="col-lg-12 text-center">
                    <div id="continue" class="">
                      <a id="next" href="{{route('frontend.book-submitted')}}" class="spacing-1 text-color-white" style="text-decoration: none;">
                    <button class="btn text-color-white mt-3 font-semibold spacing-1 text-uppercase text-xsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #1F46A3;" id="continues">CONTINUE
                      </button>
                      </a>
                    </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
{{-- </form> --}}
 </div>
</div>

      <div style="  bottom: 0px !important; width: 100%">
        <div style="background-color: rgba(0,0,0,.5);">
        <div class="container p-2">
          <div class="row">
            <div class="col-lg-6 pt-2">
              <div class="d-flex flex-row text-color-white pointer">
            
            <a name="prev" id="prev" class="d-flex pt-2 flex-row text-color-white spacing-1" style="text-decoration: none;">
            <i class="fas fa-caret-left mt-1 mr-2"></i><p class="text-color-white font-semibold">GO BACK</p></a>
          </div>
            </div>
            <div class="col-lg-6">
              <div class="row">
                <div class="col-lg-2 pt-2 mt-2">
                  <p class="text-color-white">Completed </p>
                </div>
                <div class="col-lg-10 pt-2">
              {{-- <div class="progress">
                  <div class="progress-bar bg-info" role="progressbar" style="" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div> --}}
                <div style="padding: 1em;">
                  <div class="progress">
                    <div id="progressbar" class="progress-bar progress-bar-striped active" style="width: 0%"></div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <footer style="background-color: white;">
          <div class="container pt-3">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 pb-1 pt-2">
              <h4 class="  text-xsmall">2019 Big Boss Group. All rights reserved</h4>
            </div>
            <div class="col-lg-6 col-md-6 pb-2 col-sm-12 text-lg-right text-center text-md-right">
              <h4 class="  text-color-gray text-xsmall">Connect with us
                <i class="ml-2 fab fa-facebook" style="font-size: 20px; color: #4C60A2;"></i>
                <i class="fab fa-twitter ml-2" style="font-size: 20px;color: #54CDE1;"></i>
                <i class="fab fa-instagram ml-2" style="font-size: 20px; color: #ED7EAC;"></i>
                <i class="fab fa-linkedin ml-2" style="font-size: 20px; color: #127CC2;"></i>
                <i class="fas fa-phone-square ml-2" style="font-size: 20px; color: #9A171F;"></i>
                <i class="fas fa-envelope-square ml-2 mr-2" style="font-size: 20px; color: #9A171F;"></i>
              </h4>

            </div>
          </div>
      </div>
</footer>
</div>


   {!!  $message_alert   !!}
@stop

@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/cleaning bg.png') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
} 

@media only screen and (max-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}

}

@media only screen and (min-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height:5%; width: 7%;
}

  }
  a:hover{
    color: white !important;
  }
</style>
@stop