@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
        <!-- header starts -->
    
            <!-- Begin page content -->
            <div class="container ">
                <div class="row">
                    <div class="col-lg-12 pt-2">
                        <h6 class="text-xsmall text-color-gray text-uppercase">File Directory </h6> 
                    </div>
                
                  
                     <div class="col-lg-3 mt-2">
                        <div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 100px !important;">
                             <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                             <div style="background-color:rgba(0,161,49, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                            <div class="card-body position-absolute">
                                <div class="media">
                                    <div class="media-body">
                                         <div class="row">
                                                <div class="col-lg-7 pt-2">
                                                    <p class="text-color-white text-xsmall font-semibold">Total Files</p>                                                   
                                                </div>
                                                <div class="col-5 text-right pt-2">
                                                     <h2 class="text-color-white font-semibold round">{{$total_contract}}</h2>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mt-2">
                        <div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 100px !important;">
                             <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                             <div style="background-color:rgba(244,169,28, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                            <div class="card-body position-absolute">
                                <div class="media">
                                    <div class="media-body">
                                         <div class="row">
                                                <div class="col-lg-8 pt-2">
                                                    <p class="text-color-white text-xsmall font-semibold">High Risk Files</p>                                                   
                                                </div>
                                                <div class="col-4 text-right pt-2">
                                                     <h2 class="text-color-white font-semibold round">{{$high_contract}}</h2>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mt-2">
                        <div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 100px !important;">
                             <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                             <div style="background-color:rgba(231,121,14, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                            <div class="card-body position-absolute">
                                <div class="media">
                                    <div class="media-body">
                                         <div class="row">
                                                <div class="col-lg-6 pt-2">
                                                    <p class="text-color-white text-xsmall font-semibold">Expiring in 30 Days</p>                                                   
                                                </div>
                                                <div class="col-6 text-right pt-2">
                                                     <h2 class="text-color-white font-semibold round">{{$expiring_contract}}</h2>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mt-2">
                        <div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 100px !important;">
                             <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                             <div style="background-color:rgba(161,0,10, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                            <div class="card-body position-absolute">
                                <div class="media">
                                    <div class="media-body">
                                         <div class="row">
                                                <div class="col-lg-6 pt-2">
                                                    <p class="text-color-white text-xsmall">Expired Files</p>                                                 
                                                </div>
                                                <div class="col-6 text-right pt-2">
                                                     <h2 class="text-color-white font-semibold round">{{$expired_contract}}</h2>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="http://booking.bigbossgroup.com.au/admin/contract-group/list"> Upload New Files </a>
                    <?php
                    /***************
                    <div class="col-lg-12 mt-3">
                         <h6 class="text-xsmall text-color-gray text-uppercase">FILE Groups </h6>
                    </div>
                   
                    <div class="col-lg-3 mt-3">
                        <a href="{{ route('system.contract-group.index') }}?type=cleaning" class="pointer" style="text-decoration: none;">
                            <div class="card">
                                <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="position: ;" >
                                <img src="{{ asset('frontend/images/user.jpg') }}" class="img-fluid w-50" style="position: absolute; border-radius: 50%; left: 25%; top: 30%;" >
                                <div class="text-center" style="margin-top: 90px; margin-bottom: 40px;">
                                    <p class="text-color-blue font-semibold" style="font-size: 17px; line-height: 30px;">Big Boss Cleaning <br>
                                    <span class="text-color-gray text-xsmall"><i>{{$contract_cleaning}} Files</i></span></p>
                                </div>
                             
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 mt-3">
                        <a href="{{ route('system.contract-group.index') }}?type=accounting" class="pointer" style="text-decoration: none;">
                        <div class="card">
                            <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="position: ;" >
                            <img src="{{ asset('frontend/images/user.jpg') }}" class="img-fluid w-50" style="position: absolute; border-radius: 50%; left: 25%; top: 30%;" >
                            <div class="text-center" style="margin-top: 90px; margin-bottom: 40px;">
                                <p class="text-color-blue font-semibold" style="font-size: 17px; line-height: 30px;">Big Boss Accountants <br>
                                <span class="text-color-gray text-xsmall"><i>0 Files</i></span></p>
                            </div>
                          
                        </div>
                    </a>
                    </div>
                    <div class="col-lg-3 mt-3">
                        <a href="{{ route('system.contract-group.index') }}?type=forex" class="pointer" style="text-decoration: none;">
                        <div class="card">
                            <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="position: ;" >
                            <img src="{{ asset('frontend/images/user.jpg') }}" class="img-fluid w-50" style="position: absolute; border-radius: 50%; left: 25%; top: 30%;" >
                            <div class="text-center" style="margin-top: 90px; margin-bottom: 40px;">
                                <p class="text-color-blue font-semibold" style="font-size: 17px; line-height: 30px;">Big Boss FX <br>
                                <span class="text-color-gray text-xsmall"><i>0 Files</i></span></p>
                            </div>
                          
                        </div>
                    </a>
                    </div>
                    <div class="col-lg-3 mt-3">
                        <a href="{{ route('system.contract-group.index') }}?type=infotech" class="pointer" style="text-decoration: none;">
                        <div class="card">
                            <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="position: ;" >
                            <img src="{{ asset('frontend/images/user.jpg') }}" class="img-fluid w-50" style="position: absolute; border-radius: 50%; left: 25%; top: 30%;" >
                            <div class="text-center" style="margin-top: 90px; margin-bottom: 40px;">
                                <p class="text-color-blue font-semibold" style="font-size: 17px; line-height: 30px;">Big Boss InfoTech <br>
                                <span class="text-color-gray text-xsmall"><i>0 Files</i></span></p>
                            </div>
                          
                        </div>
                    </a>
                    </div>      
                    ********************/
                    ?>
                 </div>
                   
            </div>
        </div>
        <!-- main container ends -->

        
        <!-- sidebar right ends -->

    </div>

@stop

@section('page-styles')
<style type="text/css">
    footer {
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}

</style>
@stop