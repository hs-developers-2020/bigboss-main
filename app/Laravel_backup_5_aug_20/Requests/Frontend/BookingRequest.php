<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth, Log;
use App\Laravel\Requests\RequestManager;

class BookingRequest extends RequestManager{

	public function rules(){
		//if statesless yes , disable refugee and citizen else refugee and citizenship is required.
		$current_progress = $this->session()->get('current_progress');
		// dd($current_progress);
		$rules = [];
		switch($current_progress){
			case 1:
				$rules = [
					'postcode'		=> "required",
				];
			break;
			case 2:
				$rules = [
					'location' => "required",
					
				];
			break;
			case 3:
				$rules = [
					'property' => "required",
					
				];
			break;
			case 4:
				$rules = [
					'type' => "required",
					
				];
			break;
			case 5:
				$rules = [
					'svc' => "required",
					
					
				];
			break;
			case 6:
				$rules = [
					"fname"=>"required",
					"lname"=>"required",
					// "email"=>"required",
					"contact_number"  =>  "required",
				];

				
			break;
			case 7:
				$rules = [
					"booking_date"=>"required",
				];
			break;
	
			case 9:
			$rules = [
				"payment"=>"required",
				
			];
			break;
		}
		return $rules;
	}

	public function response(array $errors)
	{
		
		if ($this->ajax() || $this->wantsJson())
		{
			return new JsonResponse($errors, 422);
		}

		
		// session()->put('current_step',7);
		// dd($this->getRedirectUrl());
		return $this->redirector->to($this->getRedirectUrl())
		->withInput($this->except($this->dontFlash))
		->withErrors($errors, $this->errorBag);
	}

	public function messages(){
		return [
			'required_with_area_code'	=> "Required if Area Code Provided",
			'required'	=> "Field is Required",
			'email' => "Invalid format",
			'phone' => "Invalid Mobile Number Format"
		];
	}
}