<?php


namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Service;
use App\Laravel\Models\BusinessGroup;
use App\Laravel\Models\UserInfo;
use App\Laravel\Models\User;
use App\Laravel\Models\BusinessGroupFile;

use App\Laravel\Events\AuditTrailActivity;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\BusinessGroupFileRequest;
use App\Laravel\Requests\System\EditBusinessGroupFileRequest;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, Auth, ImageUploader,Input,PDF,FileUploader,File,AuditRequest,Event;

class ContractGroupController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['file'] = [ ''=>"All Type",'text_file' => "Text File","pdf_file" => "PDF File","image_file" => "Image File"];
		$this->data['risk_type'] = [ ''=>"Choose Type",'yes' => "Yes","no" => "No"];
	
		
	}
  
   public function index(PageRequest $request ){

   		$type = $request->get('type',false);
   		$this->data['keyword'] = $request->get('keyword',NULL);

   		if (Auth::user()->type =="franchisee") {
   			$this->data['business_group'] = BusinessGroup::keyword($this->data['keyword'])->where('user_id' , Auth::user()->head_id)->orderBy('created_at' , "DESC")->get();
   		}elseif (Auth::user()->type =="head") {
   			$head= User::where('head_id' , Auth::user()->id)->pluck('id');
   			$this->data['business_group'] = BusinessGroup::keyword($this->data['keyword'])->whereIn('user_id', $head)->where('group',$type)->orderBy('created_at' , "DESC")->get();
   		}elseif (Auth::user()->type =="area_head") {
   			$this->data['business_group'] = BusinessGroup::keyword($this->data['keyword'])->where('user_id' , Auth::user()->id)->orderBy('created_at' , "DESC")->get();
   		}else{
   			$this->data['business_group'] = BusinessGroup::keyword($this->data['keyword'])->where('group',$type)->orderBy('created_at' , "DESC")->get();
   		}
   		
		return view ('system.contract.contract-group.index',$this->data);
	}
	public function create(){
		
		return view ('system.contract.contract-group.create',$this->data);
	}

	public function store (BusinessGroupFileRequest $request,$id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$new_business_group_file = new BusinessGroupFile;
			$new_business_group_file->fill($request->all());

			$new_business_group_file->group_id = $id;
			$new_business_group_file->fill($request->all());
			$new_business_group_file->user_id = Auth::user()->id;
			if($request->hasFile('file')) {
				$image = $request->file('file');
				$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$filetype = 'pdf_file';
					$upload_image = FileUploader::upload($image, 'storage/file/documents');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$filetype = 'image_file';
					$upload_image = ImageUploader::upload($image, "uploads/images/business_groups/groups");
				}

				$new_business_group_file->path = $upload_image['path'];
			    $new_business_group_file->directory = $upload_image['directory'];
			    $new_business_group_file->filename = $upload_image['filename'];
			    $new_business_group_file->file_type = $filetype;
			}

			if (in_array(Auth::user()->type, ["super_user","head","area_head"])) {
				$new_business_group_file->is_approved ="yes";
				$new_business_group_file->approved_by = Auth::user()->id;
			}

			if($new_business_group_file->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FILE_UPLOADED", 'remarks' => Auth::user()->full_name." has successfully upload {$new_business_group_file->name} file information. ",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session::flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.contract-group.list',$id);
			}
			session()->flash('notification-status','failed');
			Session::flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}

	public function edit ($id = NULL) {
	//dd("Dasdsd");
		$business_file = BusinessGroupFile::find($id);

		if (!$business_file) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.contract-group.index');
		}

		$this->data['business_file'] = $business_file;
		return view('system.contract.contract-group.edit',$this->data);
	}

	public function update (EditBusinessGroupFileRequest $request, $id = NULL) {

		//dd("dsadsa");
		try {
			$business_file = BusinessGroupFile::find($id);
			$business_file->updated_by = Auth::user()->id;

			if (!$business_file) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.services.index');
			}

			$business_file->fill($request->all());

			if($request->hasFile('file')) {
				$image = $request->file('file');
				$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
				
					$filetype = 'pdf_file';
					$upload_image = FileUploader::upload($image, 'storage/file/documents');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					
					$filetype = 'image_file';
					$upload_image = ImageUploader::upload($image, "uploads/images/business_groups/groups");
				}
			
			    $business_file->path = $upload_image['path'];
			    $business_file->directory = $upload_image['directory'];
			    $business_file->filename = $upload_image['filename'];
			    $business_file->file_type = $filetype;
			}
			

			if($business_file->save()) {
				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.contract-group.list',$business_file->group_id);
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function show($id = NULL){
		$business_file = BusinessGroupFile::find($id);
		if ($business_file->date_expiry <= Carbon::now()->format('Y-m-d')) {
			$this->data['status'] = "expired";
		}else{
			$this->data['status'] = "active";
		}
		
		if (!$business_file) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.contract-group.list');
		}


		$this->data['business_file'] = $business_file;
		return view ('system.contract.contract-group.contract-details',$this->data);
	}

	public function list(){

		return view ('system.contract.contract-group.contract-list');
	}

	public function contactList(PageRequest $request,$id = NULL){

		$user = Auth::user();
		$this->data['keyword'] = $request->get('keyword',false);

		$contract = BusinessGroup::where('user_id' , $id)->first();

		if ($user->type == "franchisee") {
			$this->data['business_group'] = BusinessGroupFile::keyword($this->data['keyword'])->where("user_id" , $user->id)->orderBy('created_at' , "DESC")->get();
		}else if ($user->type == "area_head") {
			$this->data['business_group'] = BusinessGroupFile::keyword($this->data['keyword'])->where("group_id" , $user->id)->orderBy('created_at' , "DESC")->get();
		}else{
			$this->data['business_group'] = BusinessGroupFile::keyword($this->data['keyword'])->where("group_id" , $id)->orderBy('created_at' , "DESC")->get();
		}
		

		if (!$contract) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.contract.index');
		}

		$this->data['contract'] = $contract;

		return view ('system.contract.contract-group.contract-list',$this->data);
	}


    
	public function download($id= NULL){

        $document = BusinessGroupFile::find($id);


        $file_path = $document->directory."/".$document->filename;
       // dd($file_path);
        return response()->download($file_path);
    }

    public function destroy ($id = NULL) {
    	$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$business_file = BusinessGroupFile::find($id);

			if (!$business_file) {
				session()->flash('notification-status',"failed");

				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.contract-group.index');
			}

			if($business_file->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FILE_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$business_file->name} file information. ",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);
				session()->flash('alert-type','failed');
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.contract-group.list',$business_file->group_id);
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function approved_contract ($id = NULL){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$business_file = BusinessGroupFile::find($id);
			
			if (!$business_file) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.contract-group.index');
			}
			$business_file->is_approved =Input::get('remarks');
			$business_file->approved_by = Auth::user()->id;
			if($business_file->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FILE_APPROVED", 'remarks' => Auth::user()->full_name." has successfully approved {$business_file->name} file information. ",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('alert-type','success');
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Contract has been Approved.");
				return redirect()->route('system.contract-group.list',$business_file->group_id);
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}




	

}
