<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use Str, Helper;

class Payment extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "payment";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $timestamps = true;

    public function reference(){
        if($this->reference_type == "personal_certificate"){
            return $this->belongsTo('App\Laravel\Models\ResidentCertificate','reference_id','id');
        }

        if($this->reference_type == "business_permit"){
            return $this->belongsTo('App\Laravel\Models\BusinessClearance','reference_id','id');
        }
    	
        if($this->reference_type == "activity_clearance"){
        //     dd($this->reference_id);
            return $this->belongsTo('App\Laravel\Models\ActivityClearance','reference_id','id');
        }
    }

    public function scopeKeyword($query,$keyword){
        if($keyword){
            $key = Str::lower($keyword);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("receipt_number LIKE '{$key}%'")
                        ->orWhereRaw("amount LIKE '{$key}%'")
                        ->orWhereHas('reference', function($query) use($key){
                            $query->whereRaw("LOWER(certificate_number) LIKE '%{$key}%'");
                          });
            });

        }
    }

    public function scopeDateRange($query,$from,$to){
        return $query->where(function($query) use($from){
                    if($from){
                        $_from = Helper::date_db($from); 
                        return $query->orWhereRaw("DATE(created_at) >= '{$_from}'");
                    }
                })->where(function($query) use ($to){
                    if($to){
                        $_to = Helper::date_db($to); 
                        return $query->orWhereRaw("DATE(created_at) <= '{$_to}'");
                    }
                });
    }

}
