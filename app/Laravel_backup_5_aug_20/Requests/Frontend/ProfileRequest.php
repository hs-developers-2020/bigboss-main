<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ProfileRequest extends RequestManager{

	public function rules(){
		//if statesless yes , disable refugee and citizen else refugee and citizenship is required.
		$id = Auth::user()->id;
		$rules = [
				"firstname" => "required|unique:user,username,{$id}",
				"lastname"  => "required",
				"email"  => "required",
				"contact_number"  =>  "required",
			

		];
		
		return $rules;
	}

	public function messages(){
		return [
			'required_with_area_code'	=> "Required if Area Code Provided",
			'citizenship' => "Required",
			'required'	=> "Required",
			'required_without' => "Required",
			'required_unless' => "Required",
			'required_if'	=> "Required",
			'gender.required' => "Please select a gender",
			'email' => "Invalid format",
			'integer' => "Invalid number",
			'phone' => "Invalid Mobile Number Format"
		];
	}
}