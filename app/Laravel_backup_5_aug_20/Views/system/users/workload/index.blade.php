@extends('system._layouts.main')
<style type="text/css">
	footer {
        
		margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 8% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
		<!-- header starts -->
	
	<form action="" method="POST" enctype="multipart/form-data">
    <div class="container ">
    	<div class="row">
    		<div class="col-lg-8 mt-5 ">
                <h6 class="text-uppercase text-xsmall font-semibold text-color-gray">Set Workload: {{$startDate->format('M d')}} - {{$endDate->format('d , Y')}}</h6>
            </div>
            <div class="col-lg-4 text-left">
                <div class="form-group">

                    <div class="text-color-gray mb-2  {{$errors->first('work_type') ? 'has-error' : NULL}}">
                        <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                        <span class="ml-2 text-xxsmall font-semibold text-uppercase">Type <code>(Save to apply this type in all dates)</code> </span>
                        {!!Form::select('work_type',$types,old('work_type',($type ? $type->work_type: "")),['class' => "form-control text-xsmall input-sm select2", 'id' => "work_type"])!!}
                        @if($errors->first('work_type'))
                            <span class="help-block" style="color: red;">{{$errors->first('work_type')}}</span>
                        @endif
                    </div>
                </div>
            </div>
    	</div>
        <div class="bg-color-white p-4">
         
                <table class="table bg-color-white  mb-0 services">
                    <thead>
                        <tr>
                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" style="width: 20%;">Date</th>
                            <th class="p-3 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs" style="width: 30%;">Codes</th>
                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm" style="width: 18%;">Max Leads</th>
                              <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Time In</th>
                                                                                              
                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Time Out</th>
                              
                        </tr>
                    </thead>
                    <tbody>
                        {!!csrf_field()!!}
                         {{-- @if(is_array($availability) AND count($availability) > 0) --}}
                            @forelse($availability as $index => $value)
                            <tr>
                                <td>
                                    <p class="text-color-blue font-semibold">{{Helper::date_work($value->date)}} - {{ Helper::day_only($value->date)}}</p>
                                    <input type="hidden" class="form-control" name="date[]" value="{{$value->date}}">
                                </td>
                                <td>
                                    {!!Form::select('work_stated[]',$work_code,old("work_stated.{$index}",$value->work_stated),['class' => "form-control  text-color-blue font-semibold pl-3 pr-3", 'id' => "work_stated"])!!}
                                </td>
                                <td>
                                    <input type="number" class="form-control" name="max_leads[]" value="{{old("max_leads.{$index}",$value->max_leads)}}">
                                </td>
                                <td>
                                    <input type="time" class="form-control" name="time_in[]" value="{{old("time_in.{$index}",$value->time_in)}}">
                                </td>
                                <td>
                                    <input type="time" class="form-control" name="time_out[]" value="{{old("time_out.{$index}",$value->time_out)}}">
                                </td>
                                
                            </tr>
                            @empty
                                @foreach(range($first_date,$second_date) as $index => $value)
                                <tr>
                                    <td>
                                        <p class="text-color-blue font-semibold">{{$startDate->toFormattedDateString()}} - {{ $startDate->englishDayOfWeek }}</p>
                                          <input type="hidden" class="form-control" name="date[]" value="{{$startDate}}">
                                    </td>
                                    <td>
                                      {!!Form::select('work_stated[]',$work_code,old('work_stated'),['class' => "form-control  text-color-blue font-semibold pl-3 pr-3", 'id' => "work_stated"])!!}
                                    </td>
                                    <td>
                                      <input type="number" class="form-control" name="max_leads[]">
                                    </td>
                                    <td>
                                      <input type="time" class="form-control" name="time_in[]" value="09:00:00">
                                    </td>
                                    <td>
                                      <input type="time" class="form-control" name="time_out[]" value="17:00:00">
                                    </td>
                                </tr>
                                <?php $startDate->addDay() ?>
                                @endforeach
                            @endforelse
                        

                        

                       

                       

                       
                    </tbody>
                </table>  
                <div class="d-flex justify-content-start mt-3 mb-3">
                    <button type="submit" class="btn bg-color-blue text-white text-xsmall ml-3">Save Changes</button>
                </div>
            
        </div>                 
    </div>
    </form>
</div>
		<!-- main container ends -->

		
		<!-- sidebar right ends -->

	</div>

@section('page-scripts')
    <script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
    <script>

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip(); 

            @if(Session::has('alert-type'))
            var type="{{Session::get('alert-type','info')}}"
            console.log(type)
            switch(type){
                case 'info':
                    toastr.options = {
                      "positionClass": "toast-top-center"
                    };
                    toastr.info("{{ Session::get('notification-msg') }}")
                     break;
                case 'success':
                     toastr.options = {
                      "positionClass": "toast-top-center"
                    };
                    toastr.success("{{ Session::get('notification-msg') }}");
                    break;
                case 'warning':
                    toastr.options = {
                      "positionClass": "toast-top-center"
                    };
                    toastr.warning("{{ Session::get('notification-msg') }}");
                    break;
                case 'failed':
                     toastr.options = {
                      "positionClass": "toast-top-center"
                    };
                    toastr.error("{{ Session::get('notification-msg') }}");
                    break;
            }
            @endif
        });
      </script>
      @stop