<?php

use App\Laravel\Models\Regions;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Regions::truncate();
        
        Regions::create(['name' => "New South Wales" , 'code' => "NSW"]);
        Regions::create(['name' => "Northern Territory", 'code' => "NT"]);
        Regions::create(['name' => "Queensland", 'code' => "QLD"]); 
        Regions::create(['name' => "South Australia",'code' => "SA"]);
        Regions::create(['name' => "Tasmania", 'code' => "TAS"]);
        Regions::create(['name' => "Victoria", 'code' => "VIC"]);
        Regions::create(['name' => "Western Australia", 'code' => "WA"]);

    }
}
