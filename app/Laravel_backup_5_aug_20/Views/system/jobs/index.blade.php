@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
 
<div class="main-container" style="margin-top: 90px;">
	<div class="container-fluid bg-black position-relative pb-4">
		<div class="row">
			<div class="container pt-2 pb-4">
				<div class="row page-title-row">
					<div class="col-lg-1 pt-2">
            <h6 class="text-uppercase text-xsmall text-color-gray">Jobs</h6>
          </div>
            {{-- <div class="col-lg-11 mb-2 d-flex flex-row text-left">
              <i class="fas fa-search pt-2 text-secondary" style="font-size: 18px;"></i><input type="text" class="text-secondary form-control bg-transparent border-0 text-xsmall" placeholder="Search User" name=""><a href="{{ route('system.users.create-user') }}" class="no-decoration"><button class="btn bg-color-blue text-xxxsmall text-color-white text-uppercase"><i class="fas fa-plus text-color-white mr-2"></i>Add New</button></a>
            </div> --}}
        </div>	
			</div>
		</div>
	</div>
</div>
<div class="container">      
	<div class="row upside-summary">
  	<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-4 pb-2">
      <a href="{{route('system.jobs.unserviced')}}">
    		<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 50px !important;">
          <img src="{{ asset('system/img/dashboard/1.jpg') }}" class="img-fluid" >
          <div class="card-body position-absolute w-100">
              <div class="media">
                  <div class="media-body">
                      <div class="row">
                        <div class="col-lg-8 pt-3">
                            <p class="text-color-white text-small font-semibold text-height-1">Unassigned Jobs</p> 
                         </div>
                         <div class="col-4 text-right pt-4">
                             <h1 class="text-color-white font-semibold round">{{$unassigned}}</h1>
                         </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </a>
  	</div>
		<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
			<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
        <img src="{{ asset('system/img/dashboard/2.jpg') }}" class="img-fluid" >
        <div class="card-body position-absolute w-100">
          <div class="media">
            <div class="media-body">
              <div class="row">
                <div class="col-lg-8 pt-3">
                    <p class="text-color-white text-small font-semibold text-height-1">Total Jobs Within 1 Day</p> 
                 </div>
                 <div class="col-4 text-right pt-4">
                     <h1 class="text-color-white font-semibold round">{{$last_day}}</h1>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
			<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
        <img src="{{ asset('system/img/dashboard/3.jpg') }}" class="img-fluid" >
        <div class="card-body position-absolute w-100">
          <div class="media">
            <div class="media-body">
              <div class="row">
                @if(Auth::user()->type =="franchisee")
                  <div class="col-lg-8 pt-3">
                    <p class="text-color-white text-small font-semibold text-height-1">Jobs <br> Ongoing</p> 
                  </div>
                  <div class="col-4 text-right pt-4">
                    <h1 class="text-color-white font-semibold round">{{$pending}}</h1>
                  </div>
               
                @else
                  <div class="col-lg-8 pt-3">
                    <p class="text-color-white text-small font-semibold text-height-1">Jobs <br> Ongoing</p> 
                  </div>
                  <div class="col-4 text-right pt-4">
                    <h1 class="text-color-white font-semibold round">{{$ongoing}}</h1>
                  </div>
              
                @endif
                
              </div>
            </div>
          </div>
        </div>
      </div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
			<div class="card mb-3 border-0 rounded-0 " style="border-radius: 5px !important; height: 110px !important;">
        <img src="{{ asset('system/img/dashboard/4.jpg') }}" class="img-fluid" >
        <div class="card-body position-absolute w-100">
          <div class="media">
            <div class="media-body">
              <div class="row">
                @if(Auth::user()->type =="franchisee")
                   <div class="col-lg-8 pt-3">
                    <p class="text-color-white text-small font-semibold text-height-1">Jobs <br> Completed</p> 
                  </div>
                 <div class="col-4 text-right pt-4">
                     <h1 class="text-color-white font-semibold round">{{$ongoing}}</h1>
                 </div>
                @else
                  <div class="col-lg-8 pt-3">
                    <p class="text-color-white text-small font-semibold text-height-1">Jobs <br> Pending</p> 
                  </div>
                 <div class="col-4 text-right pt-4">
                     <h1 class="text-color-white font-semibold round">{{$pending}}</h1>
                 </div>
                @endif
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>          
	<div class="row">
		<div class="col-lg-4 mt-5">
			<p class="text-xsmall text-color-gray font-semibold text-uppercase">Weekly Percentage</p>
			<div class="bg-color-white p-4 ">
				<div class="row">

<?php 
$datebooking11= str_replace('/', '-',$date_start);
 $newdateb2 = date("d-m-Y", strtotime($datebooking11));
?>

					<div class="col-lg-8">
						<p class="text-xsmall text-color-gray">Total Unconverted leads <br> As of<span class="text-color-gray font-semibold">  </span></p>
					</div>
					<div class="col-lg-4 text-right">
						<h4>{{$pending}}/{{$total_job}}</h4>
					</div>
				</div>
			</div>
			<div class="p-1"  style="background-color: #931111;">
				<div class="container d-flex justify-content-between pt-2">
					<p class="text-color-white text-xsmall ">{{$converted}} Converted Leads</p>
					<p class="text-color-white text-xsmall text-uppercase">{{$percent}}%</p>
				</div>
			</div>
			<div class="bg-color-white p-4 mt-4">
				<div class="row">
					<div class="col-lg-8">
						<p class="text-xsmall text-color-gray">Leads Unserviced <br> As of<span class="text-color-gray font-semibold"> </span></p>
					</div>
					<div class="col-lg-4 text-right">
						<h4>{{$unserviced}}</h4>
					</div>
				</div>
			</div>
			<div class="p-1" style="background-color: #931111;">
					<div class="container d-flex justify-content-between pt-2">
						<p class="text-color-white text-xsmall ">{{$unserviced}} Unserviced Leads</p>
						<p class="text-color-white text-xsmall ">{{$unserviced_percent}}%</p>
					</div>
			</div>
		</div>
		<div class="col-lg-8 mt-5">
			<div class="d-flex justify-content-between">
				<p class="text-xsmall text-color-gray text-uppercase font-semibold">Current Jobs</p>
				<p class="text-xsmall text-uppercase font-semibold"><a class="text-color-red decoration-none"  href="{{route('system.jobs.show')}}">View all</a></p>
			</div>
			<div class="bg-color-white p-4 ">
				<div class="row">
					<div class="col-lg-12">
						<table class="table bg-color-white  mb-0 footable">
              <thead>
                <tr>
                  <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Username</th>
                  <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs">Service Type</th>
                  <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">Service Fee</th>                                                                   
                  <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">Status</th>
                </tr>
              </thead>
              <tbody>
                @forelse($jobs as $index => $value)
                <tr>
                  <td class="userlist p-4" width="50%">
                    <a class=" text-color-white" style="text-decoration: none; cursor: hand;" href="{{ route('system.jobs.details',[$value->id]) }}">
                      <div class="media">
                        <div class="figure avatar40 border rounded-circle align-self-start">
                          <label class="checkbox-user-check">
                            <input type="checkbox">
                            <i class="fa fa-check"></i>
                          </label>
                          <img src="{{ asset('frontend/images/user.jpg') }}" alt="Generic placeholder image" class="">
                        </div>
                        <div class="media-body">
                          <h5 class="text-color-gray">{{Str::title($value->full_name)}}
                            <span class="float-right text-muted"></span>
                          </h5>
                          <p class="mb-0">{{$value->location}}</p>
                        </div>
                      </div>
                    </a>
                  </td>
                  <td>
                    <h5 class="text-color-gray p-0">{{$value->service_type ?: $value->services}}</h5>
                    <p class="mb-0">{{Str::title($value->property_type)}}</p>
                  </td>
                  <td>
                    <h5 class="text-color-gray p-0">$ {{Helper::amount($value->booking_total)}}</h5>
                   
                  </td>
                  <td>
                    <button class="btn btn-warning rounded-3 text-color-white text-xxsmall">{{Str::title($value->status)}}</button>
                  </td>
                </tr>
                @empty
                <tr>
                 <td>No Record Available</td>
                </tr>
                @endforelse
              </tbody>
            </table>        
					</div>
				</div>
		  </div>
	  </div>
  </div>
</div>  
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
        console.log("dsad")
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
    @endif
});
</script>
@stop