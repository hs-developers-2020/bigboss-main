<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class UpdateBookingRequest extends RequestManager{

	public function rules(){

		$type = $this->get('business_group');

		$rules = [
			'unit'	=> "required",
			'number_of_rooms' => "required",
			
		];

		if ($type == "accounting") {
			$rules["unit"] = "nullable";
			$rules["number_of_rooms"] = "nullable";
		}

		foreach(range(1,count($this->get('other_services'))) as $index => $value){
				$rules["other_services.{$index}"] = "required";
				$rules["other_services_price.{$index}"] = "required";

			}
	
			

		return $rules;
	}

	

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
		];
	}
}