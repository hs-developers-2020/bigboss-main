@extends('frontend._layout.main')
@section('style-script')
<style type="text/css">
body {
background-color: #F4F4F4 !important;
}

@media only screen and (max-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}

}

@media only screen and (min-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height:5%; width: 7%;
}

  }
  a:hover{
    color: white !important;
  }
</style>
@stop
@section('content')

@include('frontend._components.topnav-main')

<div class="container mt-4"  style="margin-top: 170px !important;">
  <div class="d-flex justify-content-between">
  
</div>
</div>

<div class="container mt-2" style="background-color: white; ">         
    <div class="content">
      <div class="">
        <div class="card" style="border: 0px solid rgba(0,0,0,.125);">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row">
                  <div class="col-lg-6 pl-5 pr-5 pt-5" style="background-color: white;">
                    <div class="row">
                      <div class="col-lg-12">
                        <p class="text-color-gray font-semibold text-uppercase text-small">Booking Status: <span class="text-color-green">Ongoing</span></p>
                      </div>
                      <div class="col-lg-12">
                        <p class="font-semibold"><i>Cleaning Expert has arrived and is now starting service.</i></p>
                      </div>
                       <div class="col-lg-12 mt-2">
                        <p class="text-color-gray font-semibold text-uppercase"><i class="fas fa-list-alt mr-2 text-color-blue"></i>Service Details</p>
                      </div>
                      <div class="col-lg-6 mt-2">
                        <p class="text-color-gray font-semibold text-uppercase">Service Type</p>
                      </div>
                      <div class="col-lg-6 mt-2">
                        <p class="text-color-blue font-semibold text-uppercase">House of Standard</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-uppercase">Service Package</p>
                      </div>
                      <div class="col-lg-6">
                       <p class="text-color-blue font-semibold text-uppercase">One Off</p>
                      </div>
                      <div class="col-lg-12 mt-3">
                        <p class="text-color-gray font-semibold text-uppercase"><i class="fas fa-calendar-alt text-color-blue mr-2"></i>Booking Time/Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-uppercase">Booking Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold text-uppercase">27/02/2019</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-uppercase">Secondary Booking Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold text-uppercase">27/02/2019</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-uppercase">Service Time</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold text-uppercase">10:00 am - 01:00 pm</p>
                      </div>
                       <div class="col-lg-12 mt-3">
                        <p class="text-color-gray font-semibold text-uppercase"><i class="fas fa-map-marker-alt mr-2 text-color-blue"></i>Service Location</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-uppercase">Address</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold text-uppercase">Level 4/235 Macquarie St., Sydney, NSQ</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold text-uppercase">Post</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold text-uppercase">2000</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 p-5" id="gradient-background">
                    
                        <div class="">
                            <div class="row">
                              
                              <div class="col-lg-6">
                                <p class="text-color-white font-semibold text-uppercase"><i class="fas fa-user text-color-white mr-2"></i>Customer Details</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                <p class="text-color-white font-semibold text-uppercase ">Booking ID : 878968552</p>
                              </div>
                              <div class="col-lg-12 mt-2">
                                <div class="d-flex flex-row">
                                  <img src="{{ asset('frontend/images/user.jpg') }}" class="rounded-circle mr-3 mt-2 picture-size">
                                  <p class="text-xsmall text-color-white mt-3 pt-2 font-weight-bold ml-2 spacing-1">Emmet Brown<br>
                                    <span class="font-semibold text-uppercase text-color-white text-xxsmall">Da Cleaners</span>
                                  </p>
                                </div>
                              </div>
                              <div class="col-lg-6 mt-4">
                                <p class="text-color-white font-semibold text-uppercase">Contact Number</p>
                              </div>
                              <div class="col-lg-6 text-right mt-4">
                                <p class="text-color-white font-semibold text-uppercase">(+61) 0416 001 630</p>
                              </div>
                              <div class="col-lg-6">
                                <p class="text-color-white font-semibold text-uppercase">Email Address</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                <p class="text-color-white font-semibold text-uppercase">john.gallows@gmail.com</p>
                              </div>
                              <div class="col-lg-12 mt-3">
                                <h5 class="text-color-white mb-2 font-semibold text-uppercase text-small mb-2">Payment Summary</h5>
                              </div>
                              <div class="col-lg-12">
                                <p class="text-color-white font-semibold mt-3 text-uppercase mt-2"><i class="fas fa-user text-color-white mr-2"></i>Payment Details</p>
                              </div>
                              <div class="col-lg-6">
                                <p class="text-color-white font-semibold text-uppercase">Service Type</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                <p class="text-color-white text-small font-semibold text-uppercase">$ 5.00</p>
                              </div>
                              <div class="col-lg-6">
                                <p class="text-color-white font-semibold text-uppercase">Service Package</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                <p class="text-color-white text-small font-semibold text-uppercase">$ 30.00</p>
                              </div>
                              <div class="col-lg-12">
                                <hr class="text-color-white" style="height: 2px; background-color: white;">
                              </div>
                              <div class="col-lg-6">
                                <p class="text-color-white font-semibold text-uppercase text-small">Estimated Total</p>
                              </div>
                              <div class="col-lg-6 text-right">
                                <p class="text-color-white font-semibold text-uppercase text-small">$ 35.00</p>
                              </div>
                            </div>
                        </div>
                  </div>
                 
             </div>
          </div>
        </div>
        
   </div>

  </div>

    </div>
    <div class="container ">
      <div class="row">
             <div class="col-lg-12 d-flex flex-row"> 
                  <div class="">
                    <button class="btn text-color-white mt-4 text-xxsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #972121;">
                      <a name="prev" class="spacing-1" style="text-decoration: none;">CANCEL BOOKING</a>
                    </button>
                  </div>
              </div>     
        </div>
      </div>

@include('frontend._components.footer')


@stop