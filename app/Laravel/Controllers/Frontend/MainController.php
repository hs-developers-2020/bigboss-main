<?php

namespace App\Laravel\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Laravel\Models\CleaningService;
use App\Laravel\Models\Service;
use App\Laravel\Models\SubService;
use App\Laravel\Models\OtherService;
use App\Laravel\Services\ResponseManager;
use App\Http\Requests\PageRequest;

use Auth,PDF,Request, Helper, Log;

class MainController extends Controller
{
    public function index(PageRequest $request){
           
    	$request->session()->forget('current_step');
        $request->session()->forget('current_progress');
    	$request->session()->forget('clean');
    	$request->session()->forget('percent');
    	$request->session()->forget('booking_info');
        Log::info("Main Screen");
      
    	return view ('frontend.page.main.index');
	}

	public function faqs(){
		return view ('frontend.page.faqs.index');
	}
    public function privacy(){
        return view ('frontend.page.privacy.index');
    }
     public function terms(){
        return view ('frontend.page.terms_of_use.index');
    }
    public function services(){
        Log::info("Main Screen");
        return view ('frontend.page.services.index');
    }
    public function how(){
        return view ('frontend.page.how_it_works.index');
    }

    public function logout(PageRequest $request){
        Auth::logout();
        return redirect()->route('frontend.main');
    }

    public function print_booking(PageRequest $request,$id=NULL)
    {
            // return "test";
        $total_amount=0;

       $this->data['booking'] = CleaningService::where('booking_id',$id)->first(); 

         $service_standard = explode(",",$this->data['booking']->standard_services); 

        foreach ($service_standard as $key) {
            $service = Service::find($key);

            if($service) {
                $total_amount += $service->price;
            }


        }

        $this->data['services'] = Service::whereIn('id', explode(',', $this->data['booking']->service_id))->get();
        $this->data['subservice'] = SubService::find($this->data['booking']->number_of_rooms);

        
        $this->data['others'] = OtherService::where('booking_id', $this->data['booking']->booking_id)->get();

        $this->data['total_amount'] = $total_amount;
        
        $pdf = PDF::loadView('pdf.booking_receipt', $this->data )->setPaper('A4', 'portrait');
        return $pdf->stream('booking_reciept.pdf');
    }
}
