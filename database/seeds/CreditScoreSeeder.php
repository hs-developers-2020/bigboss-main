<?php

use App\Laravel\Models\CreditScore;
use Illuminate\Database\Seeder;

class CreditScoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CreditScore::create(
            ['field' => "age", 'value' => "<28",'score' => 1]);
        CreditScore::create(
            ['field' => "age", 'value' => "28+",'score' => 7]);
        CreditScore::create(
            ['field' => "age", 'value' => "40+",'score' => 9]);
        CreditScore::create(
            ['field' => "age", 'value' => "50+",'score' => 9]);
        CreditScore::create(
            ['field' => "age", 'value' => "60+",'score' => 3]);

        CreditScore::create(
            ['field' => "marital_status", 'value' => "Single",'score' => 3]);
        CreditScore::create(
            ['field' => "marital_status", 'value' => "Married",'score' => 9]);
        CreditScore::create(
            ['field' => "marital_status", 'value' => "Separated",'score' => 7]);
        CreditScore::create(
            ['field' => "marital_status", 'value' => "Widow",'score' => 5]);

        CreditScore::create(
            ['field' => "gender", 'value' => "Female",'score' => 9]);
        CreditScore::create(
            ['field' => "gender", 'value' => "Male",'score' => 4]);

        CreditScore::create(
            ['field' => "ownership_of_house", 'value' => "Own",'score' => 9]);
        CreditScore::create(
            ['field' => "ownership_of_house", 'value' => "Lease",'score' => 4]);
        CreditScore::create(
            ['field' => "ownership_of_house", 'value' => "Staying with Family",'score' => 6]);

        CreditScore::create(
            ['field' => "length_of_stay", 'value' => "<1",'score' => 1]);
        CreditScore::create(
            ['field' => "length_of_stay", 'value' => "1+",'score' => 3]);
        CreditScore::create(
            ['field' => "length_of_stay", 'value' => "4+",'score' => 7]);
        CreditScore::create(
            ['field' => "length_of_stay", 'value' => "7+",'score' => 9]);


        CreditScore::create(
            ['field' => "number_of_children", 'value' => "<1",'score' => 7]);
        CreditScore::create(
            ['field' => "number_of_children", 'value' => "1 to 3",'score' => 9]);
        CreditScore::create(
            ['field' => "number_of_children", 'value' => "4 to 6",'score' => 3]);
        CreditScore::create(
            ['field' => "number_of_children", 'value' => ">7",'score' => 1]);

        CreditScore::create(
            ['field' => "educational_attainment", 'value' => "None",'score' => 1]);
        CreditScore::create(
            ['field' => "educational_attainment", 'value' => "Elementary",'score' => 3]);
        CreditScore::create(
            ['field' => "educational_attainment", 'value' => "High School",'score' => 5]);
        CreditScore::create(
            ['field' => "educational_attainment", 'value' => "College",'score' => 7]);
        CreditScore::create(
            ['field' => "educational_attainment", 'value' => "Masters",'score' => 9]);

        CreditScore::create(
            ['field' => "has_bank_account", 'value' => "Yes",'score' => 9]);
        CreditScore::create(
            ['field' => "has_bank_account", 'value' => "No",'score' => 6]);

        CreditScore::create(
            ['field' => "years_in_business", 'value' => "<2",'score' => 1]);
        CreditScore::create(
            ['field' => "years_in_business", 'value' => "3+",'score' => 5]);
        CreditScore::create(
            ['field' => "years_in_business", 'value' => "5+",'score' => 8]);
        CreditScore::create(
            ['field' => "years_in_business", 'value' => "7+",'score' => 9]);

        CreditScore::create(
            ['field' => "store_size", 'value' => "5sqm or less",'score' => 1]);
        CreditScore::create(
            ['field' => "store_size", 'value' => "6 to 10sqm",'score' => 3]);
        CreditScore::create(
            ['field' => "store_size", 'value' => "10.1 to 20sqm",'score' => 8]);
        CreditScore::create(
            ['field' => "store_size", 'value' => "above 20sqm",'score' => 9]);

        CreditScore::create(
            ['field' => "ownership_of_store", 'value' => "Own",'score' => 9]);
        CreditScore::create(
            ['field' => "ownership_of_store", 'value' => "Lease",'score' => 7]);

        CreditScore::create(
            ['field' => "average_monthly_sales", 'value' => "<15.9k",'score' => 1]);
        CreditScore::create(
            ['field' => "average_monthly_sales", 'value' => "15k to 29.9k",'score' => 4]);
        CreditScore::create(
            ['field' => "average_monthly_sales", 'value' => "30k to 50k",'score' => 7]);
        CreditScore::create(
            ['field' => "average_monthly_sales", 'value' => "above 50k",'score' => 9]);

        CreditScore::create(
            ['field' => "store_license", 'value' => "none",'score' => 1]);
        CreditScore::create(
            ['field' => "store_license", 'value' => "1",'score' => 5]);
        CreditScore::create(
            ['field' => "store_license", 'value' => "2",'score' => 7]);
        CreditScore::create(
            ['field' => "store_license", 'value' => "3+",'score' => 9]);

        CreditScore::create(
            ['field' => "store_online_partner", 'value' => "none",'score' => 5]);
        CreditScore::create(
            ['field' => "store_online_partner", 'value' => "1",'score' => 7]);
        CreditScore::create(
            ['field' => "store_online_partner", 'value' => "2",'score' => 9]);

        CreditScore::create(
            ['field' => "weekly_payment", 'value' => "none",'score' => 9]);
        CreditScore::create(
            ['field' => "weekly_payment", 'value' => "<2500",'score' => 8]);
        CreditScore::create(
            ['field' => "weekly_payment", 'value' => "<5000",'score' => 5]);
        CreditScore::create(
            ['field' => "weekly_payment", 'value' => "<10000",'score' => 3]);
        CreditScore::create(
            ['field' => "weekly_payment", 'value' => "10000+",'score' => 1]);
    }
}
