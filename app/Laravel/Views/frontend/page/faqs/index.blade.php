@extends('frontend._layout.main')
@section('style-script')
<style type="text/css">
body {
background-color: #F4F4F4 !important;
}

@media only screen and (max-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center; 
    background-size: cover; \
    background-position: center;
}
.picture-size {
height: 15%; width: 30%;
}

}

@media only screen and (min-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center; 
    background-size: cover; 
     background-position: center;
}
.picture-size {
height:5%; width: 7%;
}

  }
  a:hover{
    color: white !important;
  }
</style>
@stop
@section('content')

@include('frontend._components.topnav-main')

<div class="container-fluid mt-4 text-center" id="gradient-background"  style="margin-top: 170px !important; padding:100px;">
    <h3 class="text-white text-uppercase">Frequently Asked Questions</h3>
</div>

<div class="container mt-2" style="background-color: white; ">         
    <div class="contentz">
      <div class="">
        <div class="card p-4" style="border: 0px solid rgba(0,0,0,.125);">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row">
                  <div class="col-lg-12 pl-5 pr-5 pt-5" style="background-color: white;">
                    <div class="row">
                      <!--- <p class="text-color-gray text-xsmall text-justify">
                        <span class="font-semibold text-small"> <strong>Frequently Asked Questions</strong></span> -->
                    <br><br>
                    <span class="font-weight-bold">How do I qualify as a Big Boss Cleaning Business Owner?</span>
                    
                     <!-- <span class="font-semibold">Answer: </span> --> You must be business oriented and must have a dedication and goal in life to succeed in Big
                    Boss Cleaning. You may or may not have an experience in managing a similar business as long as you
                    have the willingness to learn the business is enough. You should also have a positive attitude, an eye for
                    a detail, open minded and ready to take on the challenges of running the business.
                    <br><br>
                     <span class="font-weight-bold">How long will it take me to start working for my business?</span>
                    <br>
                    This will depend on several factors, securing all the legal documents and completing the business
                    training and actual training for at least 5 days after you have signed the business licence agreement.
                    <br><br>
                     <span class="font-weight-bold">Will the Big Boss provide all the things I need to start?</span>
                    <br>
                    Each business owner will have his chosen area or territory. The initial equipments, tools, chemicals, car
                    sticker, uniform, marketing materials are included on the initial business fee. If the business owner wishes to do
                    other jobs like mowing, carpet cleaning or rubbish removal, the equipments will be at the expense of the busienss.
                    <br><br>
                     <span class="font-weight-bold">Will Big Boss provide jobs for the business owner?</span>
                    <br>
                    Big Boss Group has a call centre and a website that generate leads to give to the business owner, apart
                    from that we have contracts with other agencies like National Housing, real estate agents or home care clients where we can get
                    the leads.
                    <br><br>
                    <span class="font-weight-bold"> Is the business owner required to purchase all the supplies from Big Boss? </span>
                    <br>
                    As a business owner you are not required to buy all the supplies from Big Boss. However, we encourage you to use standard chemicals from our accredited suppliers anywhere in
                    Australia.
                    <br><br>
                    <span class="font-weight-bold"> What pre launching services do you provide? </span>
                    <br>
                    Before you start operating as Big Boss Cleaning Business Owner, a representative will assist you in training on how to run the business and how to do
                    the actual job. We will also provide the initial equipments and tools you will be needing to perform the
                    jobs.
                    <br><br>
                    <span class="font-weight-bold"> What about the Training?</span>
                    <br>
                    The training consists of classroom and actual training. We will spend time and effort on all you need to
                    know to be successful in this field.
                    <br><br>
                    <span class="font-weight-bold"> Do you have a comprehensive manual for the operation?</span>
                    <br>
                    Definitely! You will have a hard copy of the confidential operations manual and you can also access it on
                    our online system. Business owners will have their own access to it.
                      </p>
                      
                    </div>
                  </div>         
             </div>
          </div>
        </div>  
   </div>
  </div>
</div>

@include('frontend._components.footer')


@stop