<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use Helper,Str;
class Finance extends Model
{
    use DateFormatterTrait,SoftDeletes;

    protected $table = "finance";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group','amount','details','category',
    ];

    public $timestamps = true;


    public function info() {
        return $this->belongsTo("App\Laravel\Models\UserInfo", "id" ,"user_id");
    }

    public function user() {
        return $this->belongsTo("App\Laravel\Models\User", "user_id" ,"id");
    }

    public function franchisee() {
        return $this->belongsTo("App\Laravel\Models\User", "franchisee_id" ,"id");
    }


    public function scopeDateRange($query,$from,$to){
        return $query->where(function($query) use($from){
                    if($from){
                        $_from = Helper::date_db($from); 
                        return $query->orWhereRaw("DATE(created_at) >= '{$_from}'");
                    }
                })->where(function($query) use ($to){
                    if($to){
                        $_to = Helper::date_db($to); 
                        return $query->orWhereRaw("DATE(created_at) <= '{$_to}'");
                    }
                });
    }

    public function scopeType($query,$type){
        if($type){
            $key = Str::lower($type);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("LOWER(type) LIKE '%{$key}%'");
                                            
            });

     
        }
    }


}
