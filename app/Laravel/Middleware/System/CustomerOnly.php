<?php 

namespace App\Laravel\Middleware\System;

use Closure, Session;
use Auth;
use Illuminate\Contracts\Auth\Guard;

class CustomerOnly {

    /**
    * The Guard implementation.
    *
    * @var Guard
    */
    protected $auth;

    /**
    * Create a new filter instance.
    *
    * @param  Guard  $auth
    * @return void
    */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        if (Auth::check() AND $this->auth->user()->type != 'user') {
            Session::flash('notification-status','failed');
            Session::flash('notification-msg',"You don't have enough access to the requested page.");
            return redirect()->route('system.dashboard');
        }

        return $next($request);
    }

}