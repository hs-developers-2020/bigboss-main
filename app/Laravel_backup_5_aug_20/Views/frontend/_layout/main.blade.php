<!DOCTYPE html>
<html lang="en">
  
<head>
    @include('frontend._components.metas')
    @include('frontend._components.styles')
    @yield('page-styles')
  </head>
  <body>
      
     @yield('content')
	
	<script>
	  function initFreshChat() {
	    window.fcWidget.init({
	      token: "54db15cf-e2b9-4cf1-a7db-2f8e83ae2de6",
	      host: "https://wchat.freshchat.com"
	    });
	  }
	  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
	</script>
    @include('frontend._components.scripts')
    @yield('page-scripts')
  </body>
</html>