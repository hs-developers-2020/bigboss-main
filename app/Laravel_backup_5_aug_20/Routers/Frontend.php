<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Frontend", 
	'as' => "frontend.", 
	// 'prefix'	=> "admin",
	// 'middleware' => "", 

	], function(){

		$this->group(['middleware' => ["web","system.guest"]], function(){

			$this->get('register/{_token?}',['as' => "create",'uses' => "AuthController@create"]);
			$this->post('register/{_token?}',['uses' => "AuthController@store"]);
			$this->get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@index"]);
			$this->post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
			$this->post('forgot/{_token?}',['as' => "forgot",'uses' => "AuthController@forgot_password"]);
			/*$this->get('sample', ['uses' => "AuthController@sample"]);
			$this->post('sample', ['uses' => "AuthController@sample_store"]);*/
			
		});


		$this->group(['middleware' => ["web","system.customer"]], function(){

			$this->get('customer-logout',['as'=>"clogout",'uses'=>"MainController@logout"]);
			
			$this->get('/', ['as' => "main",'uses' => "MainController@index"]);
			$this->get('/faqs', ['as' => "faqs",'uses' => "MainController@faqs"]);
			$this->get('/privacy', ['as' => "privacy",'uses' => "MainController@privacy"]);
			$this->get('/terms-of-use', ['as' => "terms",'uses' => "MainController@terms"]);
			// search booking number

			$this->group(['prefix'=>'tracker','as'=>'tracker.'],function(){

				$this->post('/',['as'=>"index",'uses'=>"TrackController@index"]);
			});


			$this->group(['prefix' => "cleaning", 'as' => "cleaning."], function () {
				$this->get('/',['as' => "index", 'uses' => "SubpageCleaningController@cleaning"]);
				$this->post('/',['uses' => "SubpageCleaningController@create"]);				
				$this->post('back',['as'=>'back','uses' => "SubpageCleaningController@back"]);					
			});
			$this->get('searchpostcode', 'SubpageCleaningController@search');

			// $this->get('cleaning', ['as' => "cleaning",'uses' => "SubpageCleaningController@cleaning"]);

			$this->get('/basic-booking', ['as' => "basic-booking",'uses' => "BasicBookingController@index"]);
		
			$this->get('/book-submitted', ['as' => "book-submitted",'uses' => "BasicBookingController@bookSubmitted"]);
			$this->get('/book-approved', ['as' => "book-approved",'uses' => "BasicBookingController@bookApproved"]);


			$this->get('/booking-invoice', ['as' => "booking-invoice",'uses' => "BasicBookingController@bookingInvoice"]);

			$this->get('/rate-service/{id?}', ['as' => "rate-service",'uses' => "BasicBookingController@rating"])->middleware('system.auth');
			// $this->get('/rate-service/{id?}', ['as' => "rate-service",'uses' => "BasicBookingController@rating"]);
			$this->post('/rate-service/{id?}', ['as' => "store_feedback",'uses' => "BasicBookingController@store_feedback"])->middleware('system.auth');

			// $this->get('/login', ['as' => "login",'uses' => "AuthController@index"]);
			// $this->get('/create', ['as' => "create",'uses' => "AuthController@create"]);
		

			// $this->group(['prefix'=> "accounting",'as' => 'accounting.' ],function(){
			// 	$this->get('/', [ 'as' => "index",'uses' => "SubpageCleaningController@accounting"]);
			// 	});

			$this->group(['prefix'=> "forex",'as' => 'forex.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "SubpageCleaningController@forex"]);
				});

			$this->group(['prefix'=> "how-it-works",'as' => 'how-it-works.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "MainController@how"]);
				});

				$this->group(['prefix'=> "services",'as' => 'services.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "MainController@services"]);
				});


			$this->group(['prefix'=> "it-service",'as' => 'it-service.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "SubpageCleaningController@itService"]);
				});

			/*$this->group(['prefix'=>"booking",'as'=>"booking."],function(){
				$this->get('/',['as'=>"index",'uses'=>"SubpageCleaningController@step_index"]);
				$this->post('location',['as'=>"two",'uses'=>"SubpageCleaningController@step_two"]);
				$this->post('property-type',['as'=>"three",'uses'=>"SubpageCleaningController@step_three"]);
				$this->post('service-type',['as'=>"four",'uses'=>"SubpageCleaningController@step_four"]);
				$this->post('service-package',['as'=>"five",'uses'=>"SubpageCleaningController@step_five"]);
				$this->post('details',['as'=>"six",'uses'=>"SubpageCleaningController@step_six"]);
				$this->post('date',['as'=>"seven",'uses'=>"SubpageCleaningController@step_seven"]);
				$this->post('summary',['as'=>"eight",'uses'=>"SubpageCleaningController@step_eight"]);
				$this->post('payment-type',['as'=>"nine",'uses'=>"SubpageCleaningController@step_nine"]);
				$this->post('completed',['as'=>"finish",'uses'=>"SubpageCleaningController@finish"]);
				$this->post('cancel-booking',['as'=>"cancel-booking",'uses'=>"SubpageCleaningController@cancel"]);
				// $this->post('cancel-booking',['as'=>"cancel",'uses'=>"SubpageCleaningController@cancel"]);
			});*/




			/*$this->group(['prefix'=>"accounting",'as'=>"accounting."],function(){
				$this->get('/',['as'=>"index",'uses'=>"SubpageAccountingController@step_index"]);
				$this->post('location',['as'=>"two",'uses'=>"SubpageAccountingController@step_two"]);
				$this->post('property-type',['as'=>"three",'uses'=>"SubpageAccountingController@step_three"]);
				$this->post('service-type',['as'=>"four",'uses'=>"SubpageAccountingController@step_four"]);
				$this->post('service-package',['as'=>"five",'uses'=>"SubpageAccountingController@step_five"]);
				$this->post('details',['as'=>"six",'uses'=>"SubpageAccountingController@step_six"]);
				$this->post('date',['as'=>"seven",'uses'=>"SubpageAccountingController@step_seven"]);
				$this->post('summary',['as'=>"eight",'uses'=>"SubpageAccountingController@step_eight"]);
				$this->post('payment-type',['as'=>"nine",'uses'=>"SubpageAccountingController@step_nine"]);
				$this->post('completed',['as'=>"finish",'uses'=>"SubpageAccountingController@finish"]);
				$this->post('cancel-booking',['as'=>"cancel-booking",'uses'=>"SubpageAccountingController@cancel"]);
			});*/

			$this->group(['prefix'=>"cleaning",'as'=>"cleaning."],function(){
				$this->get('/',['as'=>"index",'uses'=>"SubpageCleaningController@cleaning_index"]);
				$this->post('/',['uses' => "SubpageCleaningController@cleaning_store"]);
				$this->get('revert',['as'=>"revert",'uses'=>"SubpageCleaningController@revert"]);
				$this->post('cancel',['as'=>"cancel",'uses'=>"SubpageCleaningController@cancel"]);
				$this->get('last_cancel',['as'=>"last_cancel",'uses'=>"SubpageCleaningController@last_cancel"]);
			});


			$this->group(['prefix'=>"accounting",'as'=>"accounting."],function(){
				$this->get('/',['as'=>"index",'uses'=>"SubpageAccountingController@accounting_index"]);
				$this->post('/',['uses' => "SubpageAccountingController@accounting_store"]);
				$this->get('revert',['as'=>"revert",'uses'=>"SubpageAccountingController@revert"]);
				$this->post('cancel',['as'=>"cancel",'uses'=>"SubpageAccountingController@cancel"]);
				$this->get('last_cancel',['as'=>"last_cancel",'uses'=>"SubpageAccountingController@last_cancel"]);
			});

			$this->group(['prefix'=>"infotech",'as'=>"infotech."],function(){
				$this->get('/',['as'=>"index",'uses'=>"SubpageInfotechController@infotech_index"]);
				$this->post('/',['uses' => "SubpageInfotechController@infotech_store"]);
				$this->get('revert',['as'=>"revert",'uses'=>"SubpageInfotechController@revert"]);
				$this->post('cancel',['as'=>"cancel",'uses'=>"SubpageInfotechController@cancel"]);
				$this->get('last_cancel',['as'=>"last_cancel",'uses'=>"SubpageInfotechController@last_cancel"]);
			});
				

			$this->get('print-booking/{id?}',['as'=>"print",'uses'=>"MainController@print_booking"]);
			$this->get('cancel-booking',['as'=>"cancel",'uses'=>"SubpageCleaningController@cancel"]);
			$this->get('cancel-accounting',['as'=>"cancel-accounting",'uses'=>"SubpageAccountingController@cancel"]);
			$this->get('email',['as'=>"email",'uses'=>"SubpageCleaningController@mails"]);
		});

			$this->get('/dashboard', ['as' => "dashboard",'uses' => "DashboardController@index"]);
			$this->get('/profile', ['as' => "profile",'uses' => "ProfileController@index"]);
			$this->post('/profile', ['uses' => "ProfileController@update"]);
});