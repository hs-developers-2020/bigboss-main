<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class LocationRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'name'	=> "required|unique:location,name,{$id}",
			'type'		=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'name.unique'	=> "Location name already used. Please double check your input.",
			'required'	=> "Field is required.",
		];
	}
}